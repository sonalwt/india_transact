
<!DOCTYPE html>
<html>
<head>
  <title>India Transact</title>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- stylesheet --> 
  <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/style.css')}}">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/responsive.css')}}">
  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
  <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.css'>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.css">
  <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">

  <style type="text/css">
        .ty-box{
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            width: 100%;
        }
        
        .go_back_link {
            display: inline-block;
            background: #7fbb30;
            padding: 10px 50px;
            border-radius: 50px;
            color: #fff;
            margin: 40px;
            text-transform: uppercase;
            font-weight: 600;
        }
  </style>
  
  <!-- Global site tag (gtag.js) - Google Ads: 725284602 -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-725284602"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-725284602');
    </script>
    
    <script>
      gtag('event', 'page_view', {
        'send_to': 'AW-725284602',
        'value': 'replace with value',
        'items': [{
          'id': 'replace with value',
          'google_business_vertical': 'retail'
        }, {
          'id': 'replace with value',
          'location_id': 'replace with value',
          'google_business_vertical': 'custom'
        }]
      });
    </script>
    
    <!-- Event snippet for India Transact Aug conversion page -->
    <script>
      gtag('event', 'conversion', {
          'send_to': 'AW-725284602/pnskCNnHuKgBEPrt69kC',
          'value': 1.0,
          'currency': 'INR'
      });
    </script>


</head>
<body id="transcroller-body" class="aos-all">

<section class="">
  <div class="container ty-box text-center">
      <div class="col-md-12 thanku_logo">
        <img src="{{URL::asset($websitedetail->logo)}}" alt="logo">
      </div>

      <div class="col-md-12">
           <h3 class="thankyou_text">Thank you for getting in touch with us. We'll get back to you shortly.</h3>
     </div>

       <div class="col-md-12">
          <a href="{{url('/')}}" class="go_back_link" target="_blank">Visit Website</a>
      </div>

  </div>
</section>


  <!-- scripts -->
  <script src='https://code.jquery.com/jquery-1.11.1.min.js'></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.js'></script>
  <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bxslider/4.2.15/jquery.bxslider.min.js"></script>
  <script src="{{URL::asset('assets/js/main.js')}}"></script>
  
</body>
</html>