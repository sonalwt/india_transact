@extends('layouts.app')
@if(isset($websitedetail->business_vertical_page_title))
@section('title')
   {{$websitedetail->business_vertical_page_title}}
@stop
@endif
@if(isset($websitedetail->business_vertical_page_keyword))
@section('keywords')
   {{$websitedetail->business_vertical_page_keyword}}
@stop
@endif
@if(isset($websitedetail->business_vertical_page_description))
@section('description')
   {{$websitedetail->business_vertical_page_description}}
@stop
@endif
@if(isset($websitedetail->business_vertical_page_url))
@section('url')
   {{url($websitedetail->business_vertical_page_url)}}
@stop
@endif
@if(isset($websitedetail->business_vertical_page_image))
@section('image')
   {{URL::asset($websitedetail->business_vertical_page_image)}}
@stop
@endif
@section('content')
@foreach($businessheadings as $businessheading)
<section class="section" id="bv_section">
	<div class="container text-center title_desc">
			
		</div>
	<!-- Banner-slider-content -->
	<div class="hero_section">
		<div class="hero_content">
			
			<div class="circle_change" data-aos="zoom-out-right" data-aos-delay="1000">
				<div class="border_line">
					<div class="slide_content">
						<div class="slide_inner_content">
							<div class="slide_img">
								@foreach($businessheading->businessverticals as $key=>$businessvertical)
								@php
								$in=$key+1;
								@endphp
								<div class="slide_icons img_icon_{{$in}} @if($in==4){{'active_icon'}}@endif" id="{{$in}}_content">
									<span class="slide_img_border" data-toggle="tooltip" data-placement="left" title="{{$businessvertical->business_title}}">
										<img src="{{$businessvertical->business_icon}}" alt="{{$businessvertical->business_title}}">
										<img src="{{$businessvertical->business_icon_selected}}" alt="{{$businessvertical->business_title}}" class="fix_change_ico">
									</span>
								</div>
								
								@endforeach
								
							</div>
							<div class="slider_inner_center">
								@foreach($businessheading->businessverticals as $key1=>$businessvertical)
								@php
								$in1=$key1+1;
								@endphp
								<div class="slider_center_block @if($in1!=4){{'hidecontent'}}@endif" id="s_c_{{$in1}}_content">									
									<img src="{{$businessvertical->business_image}}" class="base_img4" alt="{{$businessvertical->business_title}}">
								</div>
								@endforeach
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="slide_change_content" data-aos="fade-left" data-aos-delay="500">
					<h3 class="title">{{$businessheading->business_heading}}</h3>
					@foreach($businessheading->businessverticals as $key1 => $businessvertical)
					@php
								$in1=++$key1;
								@endphp
				<div class="slide_content @if($in1!=4){{'hidecontent'}}@endif sc_content" id="sub_{{$in1}}_content">
					<h2 class="title">{{$businessvertical->business_title}}</h2>
					<div class="sc_desc">
						{!! $businessvertical->business_description!!}
					</div>
				</div>
				@endforeach
				
			</div>
		</div>	
		
	</div>
	</section>
	@endforeach	
	<!-- End of Banner-slider-content -->
@endsection