@extends('layouts.app')
@if(isset($websitedetail->privacy_policy_page_title))
@section('title')
   {{$websitedetail->privacy_policy_page_title}}
@stop
@endif
@if(isset($websitedetail->privacy_policy_page_keyword))
@section('keywords')
   {{$websitedetail->privacy_policy_page_keyword}}
@stop
@endif
@if(isset($websitedetail->privacy_policy_page_description))
@section('description')
   {{$websitedetail->privacy_policy_page_description}}
@stop
@endif
@if(isset($websitedetail->privacy_policy_page_url))
@section('url')
   {{url($websitedetail->privacy_policy_page_url)}}
@stop
@endif
@if(isset($websitedetail->privacy_policy_page_image))
@section('image')
   {{URL::asset($websitedetail->privacy_policy_page_image)}}
@stop
@endif
@section('content')
<section class="section">
		<div class="container-fluid">
			<div class="container">
				@if(isset($privacypolicy))
					{!! $privacypolicy->description !!}
				<div class="container">&nbsp;
					<p><a class="wh_policy_btn" href="{{url($privacypolicy->image)}}" target="_blank">Whistle Blower Policy</a></p>
				</div>
				@endif
			</div>
		</div>
		
	</section>
@endsection