<div class="container-fluid">
    <h3>Dear Team,<br><br>
                    We've a new enquiry</h3><br>
   
    <table cellpadding="15px" cellspacing="0px" style="width: auto; color: #333; padding: 20px; font-family: Open Sans, sans-serif; border: 3px dashed #d2ae6d;" border="1" align="center">
                    <tbody>
                            @if(isset($data['name']))
                             <tr><th align="right" bgColor="#f2f2f2">name</th><td bgColor="#f6f6f6">{{$data['name']}}</td></tr>
                             @endif
                             @if(isset($data['email']))
                            <tr><th align="right" bgColor="#f2f2f2">email</th> <td bgColor="#f6f6f6">{{$data['email']}}</td></tr>
                             @endif
                             @if(isset($data['mobile']))
                                <tr><th align="right" bgColor="#f2f2f2">mobile</th>  <td bgColor="#f6f6f6">{{$data['mobile']}}</td></tr>
                                 @endif
                             @if(isset($data['state']))
                                <tr><th align="right" bgColor="#f2f2f2">state</th><td bgColor="#f6f6f6">{{$data['state']}}</td></tr>
                                 @endif
                             @if(isset($data['city']))
                                <tr><th align="right" bgColor="#f2f2f2">city</th> <td bgColor="#f6f6f6">{{$data['city']}}</td></tr>
                                 @endif
                             @if(isset($data['intrested_in']))
                                <tr><th align="right" bgColor="#f2f2f2">intrested_in</th> <td bgColor="#f6f6f6">{{$data['intrested_in']}}</td></tr>
                                 @endif
                             @if(isset($data['message']))
                                <tr><th align="right" bgColor="#f2f2f2">message</th> <td bgColor="#f6f6f6">{{$data['message']}}</td></tr>
                                 @endif
                             @if(session()->has('utm_source'))
                                <tr><th align="right" bgColor="#f2f2f2">utm_source</th><td bgColor="#f6f6f6">{{session()->get('utm_source')}}</td></tr>
                                 @endif
                             @if(session()->has('utm_medium'))
                                <tr><th align="right" bgColor="#f2f2f2">utm_medium</th><td bgColor="#f6f6f6">{{session()->get('utm_medium')}}</td></tr>
                                 @endif
                             @if(session()->has('utm_campaign'))
                                <tr><th align="right" bgColor="#f2f2f2">utm_campaign</th><td bgColor="#f6f6f6">{{session()->get('utm_campaign')}}</td></tr>
                                 @endif
                             @if(isset($data['form_type']))
                                <tr><th align="right" bgColor="#f2f2f2">form_type</th><td bgColor="#f6f6f6">{{$data['form_type']}}</td></tr>
                                 @endif
                        
                        </tbody>
    </table>
</div>