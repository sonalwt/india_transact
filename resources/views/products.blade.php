@extends('layouts.app')
@if(isset($websitedetail->product_page_title))
@section('title')
   {{$websitedetail->product_page_title}}
@stop
@endif
@if(isset($websitedetail->product_page_keyword))
@section('keywords')
   {{$websitedetail->product_page_keyword}}
@stop
@endif
@if(isset($websitedetail->product_page_description))
@section('description')
   {{$websitedetail->product_page_description}}
@stop
@endif
@if(isset($websitedetail->product_page_url))
@section('url')
   {{url($websitedetail->product_page_url)}}
@stop
@endif
@if(isset($websitedetail->product_page_image))
@section('image')
   {{URL::asset($websitedetail->product_page_image)}}
@stop
@endif
@section('content')
@if(isset($pospayment))
 <section id="pos_block" class="section">
    <div class="container-fluid">
      <div class="col-md-6 pos_content" data-aos="fade-right">
        <h1 class="title">
         {{ $pospayment->title}}
        </h1>
        <span class="divider"></span>
       {!! $pospayment->description!!}
      </div>
      <div class="col-md-6 pos_img" data-aos="fade-left">
        <img src="{{URL::asset($pospayment->image)}}" alt="Payments and POS" class="payment-n-pos" style="background: none;">
      </div>
    </div>
  </section>
@endif
  @if(isset($ongobusiness))
  <section id="ongo-pos" class="section">
    <div class="container-fluid">
      <div class="col-md-6 pos_content" data-aos="fade-right">
        <h1 class="title">
        {{$ongobusiness->title}} 
        </h1>
        <span class="divider"></span>

       {!!$ongobusiness->description!!} 

      </div>
      <div class="col-md-6 pos_img" data-aos="fade-left">
        <div class="img-pos hide-circle">
          <img src="{{URL::asset($ongobusiness->image)}}" alt="Ongo Business POS " class="payment-n-pos" style="background: none;">          
        </div>
      </div>
    </div>
  </section>
  @endif
  <div class="click-button">
    <i class="fa fa-arrow-circle-o-down"></i>
  </div>

 
      <section id="services" class="product_slider scroll-section">
        <div class="container-fluid page-wrapper" style="padding:0px;">
          <div class="replaceable" data-id="content-replaceable">
        <main class="about-page">
        <div class="ps-slider-plus">
            <div class="wrapper">
                <!-- <div class="briks-bot" data-height="50%"></div> -->
                <div class="container" style="padding:0px;">
                  <div class="slider-left">
                    <div class="block">
                        <svg height="550" width="550">
                            <circle class="circle" cx="275" cy="275" r="274" transform="rotate(46, 275, 275)"/>
                        </svg>
                         @foreach($solutions as $key=>$solution)
                        <div class="block-min block-min-{{$key+1}}">

                            <svg height="83" width="83" style="transform:rotate(-120deg)">
                                <circle class="circle-first-block" cx="41.5" cy="41.5" r="41" />
                                <circle class="circle-progress-block" data-id="{{$key+1}}" cx="41.5" cy="41.5" r="41" />
                                <circle class="circle-second-block" cx="41.5" cy="41.5" r="35.5" />

                                <g transform="translate(18,18)" >
                                  <image xlink:href="{{URL::asset($solution->icon_image)}}" x="0" y="0" height="50px" width="50px"/ alt="{{$solution->title2}}">
                                </g>
                            </svg>
                        </div>
                       @endforeach
                       
                    </div>
                </div>

                    <div class="slider-right" data-bottom-top="transform: translate3d(0px,100px,0px) rotate(0.0001deg)" data-top-bottom="transform: translate3d(0px,-100px,0px) rotate(0.0001deg)">
                        <div class="swiper-container" data-id="slider-wheel">
                            <div class="swiper-wrapper">
                              @foreach($solutions as $k=>$solution)
                              <div class="swiper-slide">
                                    <div class="ps_block_1">
                                      <img src="{{URL::asset($solution->main_image)}}" alt="{{$solution->title2}}">
                                    </div>
                                    <div class="ps_block_2">
                                      <h3>{{$solution->title1}}</h3>
                                      <h1>{{$solution->title2}}</h1>
                                     {!! $solution->description !!}
                                    </div>
                                </div>
                                @endforeach
                                
                               
                            </div>
                            <div class="slider-controls">
                                <div class="button-prev">
                                    <div class="bg"></div>
                                    <div class="ar"></div>
                                </div>
                                <div class="button-next">
                                    <div class="bg"></div>
                                    <div class="ar"></div>
                                </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
               </main>
            </div>
        </div>
      </section>

      @if(isset($loyaltyservices))
      <section class="section" id="loyalty">
        <div class="container-fluid">
          <div class="col-md-6" data-aos="fade-right">
            <h1 class="title">{{$loyaltyservices->title}}</h1>
            <span class="divider"></span>
            {!!$loyaltyservices->description!!}
          </div>
          <div class="col-md-6 text-center">
            <!-- <img src="images/products/loyalty-cards.png" alt=""> -->
            <img src="{{URL::asset($loyaltyservices->image1)}}" alt="{{$loyaltyservices->title}}" class="card_1" data-aos="fade-left" data-aos-delay="300">
            <img src="{{URL::asset($loyaltyservices-> image2)}}" alt="{{$loyaltyservices->title}}" class="card_2"  data-aos="fade-right" data-aos-delay="500">
          </div>
        </div>
      </section>
      @endif
      @if(isset($ongobilling))
      <section class="section" id="ongo">
        <div class="container-fluid">
          <div class="col-md-6 ongo_content" data-aos="fade-right">
            <h1 class="title">{{$ongobilling->title}}</h1>
            <span class="divider"></span>
            {!!$ongobilling->description!!}
          </div>
          <div class="col-md-6" data-aos="fade-left" data-aos-delay="300">
            <img src="{{URL::asset($ongobilling->image)}}" alt="{{$ongobilling->title}}">
          </div>
        </div>
      </section>
       @endif

  
       @if(isset($merchantservices))
      <section class="section online-presence" id="online-presence">
        <div class="container-fluid">
          <div class="col-md-12">
            <div class="owl-carousel owl-theme" id="online-presence-slider">

             
              @foreach($merchantservices as $key=>$merchantservice)
              <div class="item col-md-12 @if($key==0) {{'Active'}} @endif">
                <div class="col-md-6">
                  <img src="{{URL::asset($merchantservice->main_image)}}" style="width: 90%;" alt="Mini ATM">
                </div>  
                <div class="col-md-6 op_content">
                  <h3>{{$merchantservice->title1}}</h3>
                  <h1 class="title">{{$merchantservice->title2}}</h1>
                  {!! $merchantservice->description  !!}
                </div>              
              </div>
              @endforeach
       

            </div>
          </div>
        </div>
      </section>
      @endif
    
@endsection
@section('scripts')
<script type="text/javascript">
    AOS.init({
      easing: 'ease-in-out-sine'
    });
    $(document).ready(function(){
      $(".slide-toggle").click(function(){
        $('.call_now').toggleClass('open_box');
      });
    });

    $('#online-presence-slider').owlCarousel({
       autoplay: true,
       autoplayHoverPause: true,
       loop: true,
       // margin: 20,
       dots:false,
       responsiveClass: true,
       nav: true,
       navText: ["<i class='fa fa-caret-left' aria-hidden='true'></i>","<i class='fa fa-caret-right'></i>"],
       loop: true,
       items: 1,
       responsive: {
         0: {
           items: 1
         },
         568: {
           items: 1
         },
         600: {
           items: 1
         },
         1000: {
           items: 1
         }
       }
      });
  </script>
  @endsection