@extends('layouts.app')
@if(isset($websitedetail->ewaste_policy_page_title))
@section('title')
   {{$websitedetail->ewaste_policy_page_title}}
@stop
@endif
@if(isset($websitedetail->ewaste_policy_page_keyword))
@section('keywords')
   {{$websitedetail->ewaste_policy_page_keyword}}
@stop
@endif
@if(isset($websitedetail->ewaste_policy_page_description))
@section('description')
   {{$websitedetail->ewaste_policy_page_description}}
@stop
@endif
@if(isset($websitedetail->ewaste_policy_page_url))
@section('url')
   {{url($websitedetail->ewaste_policy_page_url)}}
@stop
@endif
@if(isset($websitedetail->ewaste_policy_page_image))
@section('image')
   {{URL::asset($websitedetail->ewaste_policy_page_image)}}
@stop
@endif
@section('content')
<section class="section">
		<div class="container-fluid">
			<div class="container disclaimer-wrapper">
				@if(isset($ewastepolicy))
					{!!$ewastepolicy->description!!}
				@endif

				<div class="table-responsive e_policy_table">
					<table class="table table-bordered">
                        <thead>

                            <tr>
                                <th>Sr. No</th>
                                <th>State</th>
                                <th>Collection Centre Address</th>
                                <th style="width:15%">Toll Free No</th>
                            </tr>
                        </thead>
                        <tbody class="css-serial">
                        	@foreach($collectioncentreaddresses as $collectioncentreaddress)
                            <tr>
                                <td data-label="Sr. No">{{$collectioncentreaddress->id}}</td>
                                <td data-label="State">{{$collectioncentreaddress->state}}</td>
                                <td data-label="Collection Centre Address">{{$collectioncentreaddress->collection_centre_address}} </td>
                                <td data-label="Toll Free No">{{$collectioncentreaddress->toll_free_no}}</td>
                            </tr>
                           @endforeach
                        </tbody>
</table>
				</div>

			</div>
		</div>
		
	</section>
@endsection