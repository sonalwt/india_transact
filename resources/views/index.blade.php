@extends('layouts.app')

@if(isset($websitedetail->index_page_title))
@section('title')
   {{$websitedetail->index_page_title}}
@stop
@endif
@if(isset($websitedetail->index_page_keyword))
@section('keywords')
   {{$websitedetail->index_page_keyword}}
@stop
@endif
@if(isset($websitedetail->index_page_description))
@section('description')
   {{$websitedetail->index_page_description}}
@stop
@endif
@if(isset($websitedetail->index_page_url))
@section('url')
   {{url($websitedetail->index_page_url)}}
@stop
@endif
@if(isset($websitedetail->index_page_image))
@section('image')
   {{URL::asset($websitedetail->index_page_image)}}
@stop
@endif
@section('content')
<!-- Banner-slider-content -->
    <div class="hero_section">
        <div class="hero_content">
            <div class="circle_change" data-aos="zoom-out-right" data-aos-delay="1000">
                <div class="border_line">
                    <div class="slide_content">
                        <div class="slide_inner_content">
                            <div class="slide_img">
                                @foreach($hometransactionheading->hometransactions as $k=>$hometransaction)
                                
                                <div class="slide_icons img_icon_{{$k+1}} @if($k==3) {{'active_icon'}} @endif" id="{{$k+1}}_content">
                                    <span class="slide_img_border" data-toggle="tooltip" data-placement="right" title="{{$hometransaction->title}}">
                                        <img src="{{URL::asset($hometransaction->icon_image)}}" alt="{{$hometransaction->title}}">
                                        <img src="{{URL::asset($hometransaction->selected_icon_image)}}" class="fix_change_ico" alt="{{$hometransaction->title}}">
                                    </span>
                                </div>
                                @endforeach
                            </div>
                            <div class="slider_inner_center">
                                @foreach($hometransactionheading->hometransactions as $k2=>$hometransaction)
                                <div class="slider_center_block @if($k2!=3) {{'hidecontent'}} @endif" id="s_c_{{$k2+1}}_content">
                                    <img src="{{URL::asset($hometransaction->main_image)}}" class="base_img4" alt="{{$hometransaction->title}}">
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="slide_change_content" data-aos="fade-left" data-aos-delay="500">

                <div class="tr_text">
                    <h1> {{$hometransactionheading->headings}}</h1>
                </div>
                    @foreach($hometransactionheading->hometransactions as $k3=>$hometransaction)
                <div class="slide_content sc_content @if($k3!=3) {{'hidecontent'}} @endif" id="sub_{{$k3+1}}_content">
                    <h2 class="title">{{$hometransaction->title}}</h2>
                    <div class="sc_desc">
                        {!!$hometransaction->description!!}
                    </div>
                    <a href="{{url('/products#online-presence')}}" class="know_btn">Explore More</a>
                </div>
                @endforeach
                
            </div>
            <div class="slide_title">
            </div>
        </div>
    </div>
    <!-- End of Banner-slider-content -->

    <div class="click-button">
      <i class="fa fa-arrow-circle-o-down"></i>
    </div>

    <section class="section scroll-section" id="mission">
        <div class="container-fluid">
            <div class="col-md-6 text-center img_block"  data-aos="zoom-in-down" data-aos-delay="1000">
                <img src="{{URL::asset($homeoverview->image)}}" alt="overview">
            </div>
            <div class="col-md-6 about_content_right text-left"  data-aos="zoom-in-up" data-aos-delay="500">
                <h1 class="title" style="line-height: 1.3">{{$homeoverview->headings}}</h1>
                <span class="divider"></span>
                <p>
                    {!!$homeoverview->description!!}
                    <br><br> <a href="{{url('/about')}}" class="know_btn">Explore More</a>.
                </p>

            </div>
        </div>


        <!-- <div class="hero_gif">
            <img src="images/WebHomeAni.gif">
        </div> -->
    </section>

    <section class="section" id="four-images">
        <div class="container-fluid">

            <div class="why_title" data-aos="fade-down-right" data-aos-delay="500">
                <div class="col-md-3"></div>
                <div class="col-md-6 text-center">
                    <h1 style="line-height: 1.3" class="title">Our Footprints</h1>
                    <span style="margin:0 auto;width: 30%;" class="divider"></span>
                </div>
                <div class="col-md-3"></div>
                <span class="clearfix"></span>
            </div>
        </div>

        <div class="clearfix"></div>
        <div class="container-fluid" id="t_numbers" data-aos="zoom-in-up" data-aos-delay="500">
            <div class="container-fluid">

                @foreach($footprints as $key1=>$footprint)
                <div class="col-md-2 sub-img">
                    <div class="img">
                        <img src="{{URL::asset($footprint->counter_image)}}" alt="
                        {{$footprint->counter_caption}}">
                    </div>
                    <!-- <h1>100+</h1> -->
                    <div id="counter_number"><h1 class="count">{{$footprint->counter_number}}</h1><h1>+</h1></div>
                    <h1 style="margin:0px;font-size: 22px;min-height: 25px;">{{$footprint->counter_unit}} </h1>
                    <h3 class="counter_caption">{{$footprint->counter_caption}}</h3>
                </div>
                @php 
            $c=$key1+1;
            @endphp
            @if($c%6==0)
            <div class="clearfix"></div>
            @endif
                @endforeach
                

                

            </div>
        </div>
    </section>

    <section class="section" id="why_section">
        <div class="container-fluid">
            <div class="col-md-3 awards-title" data-aos-delay="500" data-aos="fade-right">
                <h1 class="title">Key Strengths</h1>
                <span class="divider"></span>
            </div>
        </div>
        <div class="container-fluid">
                @foreach($keystrengths as $key=>$keystrength)
            <div class="col-md-3 key_block text-center" data-aos="flip-right" data-aos-delay="700">
                <img src="{{URL::asset($keystrength->image)}}" class="key_img_1" alt="{{$keystrength->caption}}">
                <img src="{{URL::asset($keystrength->after_hover_image)}}" class="key_img_2" alt="{{$keystrength->caption}}">
                <h3>{{$keystrength->caption}}</h3>
            </div>
            @php 
            $b=$key+1;
            @endphp
            @if($b%4==0)
            <div class="clearfix"></div>
            @endif
            @endforeach
            
        
            
            
            
            
        </div>
    </section>


    <section class="section" id="news">
        <div class="container-fluid">
            <div class="col-md-3 awards-title" data-aos-delay="500" data-aos="fade-right">
                <h1 class="title">News</h1>
                <span class="divider"></span>
            </div>
        </div>
        <div class="container-fluid">
            <div class="col-md-6" data-aos-delay="700" data-aos="fade-right">
                    @foreach($homenews as $key1=>$news1)
                    @php
                    $in=$key1;
                    $out=++$in;
                    @endphp
                <div class="news_img" id="news_img_{{$out}}_news" @if($key1==0) @else style="display: none;" @endif>
                    <img src="{{URL::asset($news1->main_image)}}" alt="{{$news1->title}}">
                </div>
                    @endforeach
<!--
                <div class="news_img" id="news_img_2_news" style="display: none;">
                    <img src="images/news/large/news-main2.png" alt="News image">
                </div>
-->
    
            </div>
            <div class="col-md-6" data-aos-delay="900" data-aos="fade-left">
                @foreach($homenews as $key2=>$news2)
                @php
                    $in1=$key2;
                    $out1=++$in1;
                    @endphp
                <div class="news_title_content" id="news_{{$out1}}_news" @if($key2==0) @else style="display: none;" @endif>
                    <h3 class="title">{{$news2->title}}</h3>
                    @if(isset($news2->date))
                         @php
                            $date=date("M d, Y", strtotime($news2-> date));
                         @endphp
                           
                     @else 
                        $date='';
                    @endif
                    <h4 class="subtitle">{{$news2->location}} | {{$date}}</h4>
                    {!!$news2->description!!}
                    <a href="{{url( $news2->news_file)}}" target="_blank" class="know_btn">Read More <!-- <img src="images/right-arrow.png"> --></a>

                </div>
                @endforeach

                <hr style="border-top: 2px solid #7fbb30;">
                <div class="news_items_block">
                    @foreach($homenews as $key3=>$news3)
                    @php
                    $in2=$key3;
                    $out2=++$in2;
                    @endphp
                    <div class="col-md-4 news_item text-center" id="{{$out2}}_news">
                        <img src="{{URL::asset($news3->icon_image)}}" alt="{{$news3->title}}">
                        <span class="news_caption">{{$news3->title}}</span>
                    </div>
                    @endforeach
                    <div class="text-center">
                        <a href="{{url('/news')}}" class="know_btn">Explore More <!-- <img src="images/right-arrow.png"> --></a>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <section class="section" id="awards">
        <div class="container-fluid">
            <div class="col-md-3 awards-title" data-aos-delay="500" data-aos="fade-right">
                <h1 class="title">Awards & Recognition</h1>
                <span class="divider"></span>
            </div>
        </div>

        <div class="container-fluid awards-listing">
            <div class="col-md-10 col-md-offset-1">
                @foreach($homeawards as $k=>$homeaward)
                
                <div class="col-md-4 list" data-aos-delay="{{$k+7}}00" data-aos="flip-right">
                    <img src="{{URL::asset($homeaward->image)}}" alt="Awards">
                    <p>{{$homeaward->title}}</p>
                </div>
                @php 
            $a=$k+1;
            @endphp
            @if($a%3==0)
            <div class="clearfix"></div>
            @endif
                @endforeach
                
            </div>
        </div>

        <div class="container-fluid">
            <div class="col-md-3 float-right">
                <a href="{{url('/award')}}" class="know_btn">Explore More <!-- <img src="images/right-arrow.png"> --></a>
            </div>
        </div>
    </section>

    <section class="section" id="awards">
        <div class="container-fluid">
            <div class="col-md-3 awards-title" data-aos-delay="500" data-aos="fade-right">
                <h1 class="title">Clients</h1>
                <span class="divider"></span>
            </div>
        </div>

        <div class="container-fluid awards-listing">
            <div class="col-md-10 col-md-offset-1">
                <div class="client_slider owl-carousel owl-theme">
                    @foreach($clients as $client)
                    <div class="item">
                        <img src="{{URL::asset($client->client_logo)}}" alt="{{$client->client_name}}">
                        <p></p>
                    </div>
                    @endforeach
                </div>

            </div>
        </div>

        <div class="container-fluid">
            <div class="col-md-3 float-right">
                <a href="{{url('/client')}}" class="know_btn">Explore More <!-- <img src="images/right-arrow.png"> --></a>
            </div>
        </div>
    </section>


    <section class="section" id="videos">
        <div class="container-fluid">
            <div class="col-md-3 awards-title" data-aos-delay="500" data-aos="fade-right">
                <h1 class="title">Videos</h1>
                <span class="divider"></span>
            </div>
        </div>
        <div class="container-fluid">
            <div class="col-md-6" data-aos-delay="700" data-aos="fade-right">
                @foreach($homevideos as $k=>$homevideo)
                @php
                    $in=$k;
                    $out=++$in;
                    @endphp
                <div class="videos_img" id="videos_img_{{$out}}_news">
                    <iframe width="" height="315" src="{{$homevideo->youtube_link}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="align-center"></iframe>
                </div>
                @endforeach
            </div>
            <div class="col-md-6 video-thumbnails" data-aos-delay="900" data-aos="fade-left">
                <div class="news_items_block">
                    @foreach($homevideos as $k=>$homevideo)
                @php
                    $in=$k;
                    $out=++$in;
                    @endphp
                    <div class="col-md-4 videos_item text-center" id="{{$out}}_news">
                        <img src="{{URL::asset($homevideo->image)}}" alt="Vaishya Sahaka">
                        <span class="news_caption">{{$homevideo->title}}</span>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>



    <section class="section">
        <div class="container">
            <div class="col-md-3"></div>
            <div class="col-md-6 team_title client_title aos-init aos-animate" data-aos="zoom-in">
                <h1 class="title">Associates & Certifications</h1>
                <span class="divider" style="margin:0 auto;"></span>
            </div>
            <div class="col-md-3"></div>
        </div>
        <div class="container-fluid" id="client-logos">
            <ul class="client-logo">
                @foreach($homeassociates as $homeassociate)
                <li>
                    <img src="{{URL::asset($homeassociate->image)}}" alt="{{$homeassociate->title}}">
                </li>
                @endforeach
            </ul>
        </div>
    </section>


<!--    <section class="quick-contact">-->
<!--        <div class="container">-->
<!--            <h1 class="title text-center">Contact Us</h1>-->
<!--            <div class="col-md-8 col-md-offset-2">-->
<!--                <div class="contact_form cont_form_home">-->
<!--                    <form action="javascript:void(0)" id="abcd" accept-charset="utf-8">-->
<!--                        <div class="col-md-6">-->
<!--                            <input type="text"  class="form-control" placeholder="Name" name="name" id="name">-->
<!--                        </div>-->
<!--                        <div class="col-md-6">-->
<!--                            <input type="email"  class="form-control" placeholder="Email" name="email" id="email">-->
<!--                        </div>-->
<!--                        <div class="col-md-6">-->
<!--                            <input type="text"  class="form-control" placeholder="Mobile Number" name="contact" id="contact">-->
<!--                        </div>-->
<!--                        <div class="col-md-6">-->
<!--                            <input type="text" id="state"  class="form-control" placeholder="Enter your State">-->
<!--                        </div>-->
<!--                        <div class="col-md-6">-->
<!--                            <input type="text" id="city"  class="form-control" placeholder="Enter your City">-->
<!--                        </div>-->
<!--                        <div class="col-md-6">-->
<!--                            <select id="interest"  class="form-control">-->
<!--                            <option value="">Interested In</option>-->
<!--                            <option>Swipe Machine</option>-->
<!--                            <option>Loyalty Cards</option>-->
<!--                            <option>Payment Gateway</option>-->
<!--                            <option>Billing Software</option>-->
<!--                            <option>Digital Wallet</option>-->
<!--                            <option>Mini ATM</option>-->
<!--                            <option>Integrated Billing</option>-->
<!--                            <option>Others</option>-->
<!--                            </select>-->
<!--                        </div>-->
<!--                        <div class="col-md-12">-->
<!--                            <textarea id="message" placeholder="Write a message"></textarea>-->
<!--                        </div>-->
<!--                        <div class="col-md-12 text-center">-->
<!--                            <input id="enquiry-submit1" type="submit" value="submit">-->
<!--                        </div>-->
<!--                        <div class="col-md-12 text-center">-->
<!--                            <span id="enq_msg"></span>-->
<!--                        </div>-->
<!--                    </form>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </section>-->





    <div class="call_now">
        <button type="button" class="slide-toggle"><i class="fa fa-envelope"></i></button>
        <div class="box">
            <div class="box-inner">
                <h4>Enquiry Form</h4>
                <hr>
                <div class="modal-body mx-3">
                    <div class="md-form mb-5">
                        <input type="text" id="index_name" class="form-control validate" placeholder="Enter your Name *">
                    </div>
                    <div class="md-form mb-5">
                        <input type="email" id="index_email" class="form-control validate" placeholder="Enter your Email *">
                    </div>
                    <div class="md-form mb-4">
                        <input type="tel" id="index_contact" class="form-control validate" placeholder="Enter Contact No *">
                    </div>
                    <div class="md-form mb-4">
                        <input type="text" id="index_state" class="form-control validate" placeholder="Enter your State *">
                   </div>
                   <div class="md-form mb-4">
                       <input type="text" id="index_city" required class="form-control validate" placeholder="Enter your City">
                   </div>
                    <input type="hidden" id="index_form_type" name="index_form_type" value="index-form">
                   <div class="md-form mb-4">
                       <select id="index_interest" required class="form-control validate">
                           <option value="">Interested In</option>
                           @foreach($products as $product)
                           <option value="{{$product->id}}">{{$product->title}}</option>
                           @endforeach
                          
                       </select>
                   </div>
                    <div class="md-form mb-4">
                        <textarea placeholder="Write Message" id="index_message" class="form-control"></textarea>
                    </div>
                </div>
                <div class="modal-footer d-flex justify-content-center">
                    <button class="btn btn-deep-orange" id="enquiry-submit">Submit</button>
                </div>
                <div class="md-form mb-4 msg-pd">
                    <span id="enq_msg"></span>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="enq_form">
      <div class="modal-dialog">
        <div class="modal-content">

          <!-- Modal Header -->
          <div class="modal-header text-center">
            <h4 class="modal-title">Fill in your details and we will get in touch with you.</h4>
          </div>

          <!-- Modal body -->
          <div class="modal-body">
                <form  autocomplete="off" class="inq-form lead_form">
                    {{ csrf_field() }}
                    <div class="col-12">
                        <input  autocomplete="off" data-req="y" data-type="text" type="text" id="modal_name" class="form-control capture" placeholder="Name*" name="name">
                    </div>
                    <div class="col-12">
                        <input  autocomplete="off" data-req="y" data-type="email" type="email" class="form-control capture" placeholder="Email*" id="modal_email" name="email">
                    </div>
                    <div class="col-12">
                        <input  autocomplete="off" data-req="y" data-type="mobile" type="tel" class="form-control capture" placeholder="Mobile No*" id="modal_mobile" name="mobile">
                 
                    </div>
                    <div class="col-12">
                        <input  autocomplete="off" data-req="y" data-type="state" type="text" class="form-control capture" placeholder="State*" id="modal_state" name="state">
                    </div>
                      <input type="hidden" id="form_type" name="form_type" value="pop-up-form">
                    <input type="hidden" id="modal_utm_source" name="modal_utm_source" value="{{$utm_source}}">
                          <input type="hidden" id="modal_utm_campaign" name="modal_utm_campaign" value="{{$utm_medium}}">
                          <input type="hidden" id="modal_utm_medium" name="modal_utm_medium" value="{{$utm_campaign}}">
                    <div class="col-12 text-center">
                        <button type="submit" class="btn btn-custom hvr-sweep-to-right" id="modal_frm_submit">SUBMT</button>
                    </div>
                    <div id="modal_frm_msg"></div>
                </form>
          </div>

          <!-- Modal footer -->

<!--            <button type="button" class="close" data-dismiss="modal">CLOSE</button>-->
            <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>


        </div>
      </div>
    </div>
    <div class="modal fade" id="popup-image">
      <div class="modal-dialog">
        <div class="modal-content">
          <!-- Modal Header -->        

          <!-- Modal body -->
        <div class="modal-body">
             <a href="http://onelink.to/h26dkk" target="_blank">
                <img src="{{URL::asset('/assets/images/AarogyaSetuITSL.jpeg')}}" style="max-width: 100%">
             </a>
        </div>
    
        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>


        </div>
      </div>
    </div>
@endsection
@section('scripts')
<script>
    $(window).load(function() {
      $('body').addClass('home_grid_slider');
});
    $(window).on('load',function(){
                    setTimeout(function(){
                    $('#popup-image').modal('show');
                     }, 500);
                });
    $(document).ready(function(){
        
        $('#modal_frm_submit').on('click', function(e){
            //alert('test');
                            e.preventDefault();

                            var name = $.trim($('#modal_name').val());
                            var email = $.trim($('#modal_email').val());
                            var mobile = $.trim($('#modal_mobile').val());
                            var state = $.trim($('#modal_state').val());
                            var utm_source = $.trim($('#modal_utm_source').val());
                            var utm_campaign = $.trim($('#modal_utm_campaign').val());
                            var utm_medium = $.trim($('#modal_utm_medium').val());
                            var form_type=$.trim($('#form_type').val());
                            var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
                            var mobilePattern = /^\d{10}$/;
                            var flag=0;

                            if(name=='' || name == null)
                            {
                                    $('#modal_name').addClass('error');
                                    flag++;
                            }
                            else
                            {
                                    $('#modal_name').removeClass('error');
                            }


                            if(state=='' || state == null)
                            {
                                    $('#modal_state').addClass('error');
                                    flag++;
                            }
                            else
                            {
                                    $('#modal_state').removeClass('error');
                            }

                            if (!emailPattern.test(email) || email == '' || email == null)
                            {
                                    $('#modal_email').addClass('error');
                                    flag++;
                            }
                            else
                            {
                                    $('#modal_email').removeClass('error');
                            }

                            if (!mobilePattern.test(mobile) || mobile == '' || mobile == null)
                            {
                                    $('#modal_mobile').addClass('error');
                                    flag++;
                            }
                            else
                            {
                                    $('#modal_mobile').removeClass('error');
                            }

                            if(flag==0)
                            {
                                $('#modal_frm_submit').attr('disabled',true);
                                $('#modal_frm_msg').val('Please wait...');
                                    var data = {
                                                            email : email,
                                                            name:name,
                                                            mobile:mobile,
                                                            state:state,
                                                            utm_source:utm_source,
                                                            utm_medium:utm_medium,
                                                            utm_campaign:utm_campaign,
                                                            form_type:form_type,
                                                    }

                                                    $.ajax({
                                                    type : "POST",
                                                    url : '/modalenquiryform',
                                                    data : data,
                                                    success : function(result) {
                                                            if(result==1)
                                                            {
                                                                $('#modal_frm_submit').attr('disabled',false);
                                                                    $('#modal_frm_msg').html('Thank you for contacting us');
                                                                    window.setTimeout(function(){window.location.href = '/thankyou'}, 3000);
                                                        }
                                                            else if(result == 0)
                                                            {
                                                                    $('#modal_frm_submit').attr('disabled',false);
                                                                    $('#modal_frm_msg').val('Something went wrong');
                                                            }
                                                    },
                                                    error: function() {
                                                            $('#modal_frm_submit').attr('disabled',false);
                                                            $('#modal_frm_msg').val('Something went wrong');
                                                    }
                                            });
                            }

                    });
        

        });
</script>

@endsection