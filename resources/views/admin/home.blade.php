@extends('admin.layouts.master')
@section('content')
    <section class="">
        <div class="container-fluid">
            <div class="block-header">
                <h2>DASHBOARD</h2>
            </div>
            <div class="row clearfix">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect">
                        <div class="icon">
                            <a href="{{url('/admin/enquiryformleads')}}"> <i class="material-icons">playlist_add_check</i></a>
                        </div>
                      
                        <div class="content">
                             <div class="text">Enquiry  Form Leads</div> 
                            <div class="number count-to" data-from="0" data-to="{{$count_enq}}" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                       
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect">
                        <div class="icon">
                            <a href="{{url('/admin/careerformleads')}}"><i class="material-icons">help</i></a>
                        </div>
                       
                        <div class="content">
                             <div class="text">Career Form Leads</div> 
                            <div class="number count-to" data-from="0" data-to="{{$career_enq}}" data-speed="15" data-fresh-interval="20"></div>
                        </div>
                   
                    </div>
                </div>
                
            </div>
        </div>
    </section>
@endsection

