@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Career Form Leads
                                        </h2>
                                    </div>
                     <br>
                     <!-- <a href="{{ url('/admin/enquiryformproducts/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Enquiryformproduct" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a> -->
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/careerformleads', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Resume</th>
                                        <th>Message</th>
                                        <th>Form Type</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($enquiryforms as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->mobile }}</td>
                                        @if(isset($item->file))
                                        <td><a href="{{URL::asset($item->file)}}" target="_blank" download><i class="material-icons">file_copy</i></a></td>
                                        @else
                                        <td></td>
                                        @endif
                                         <td>{{ $item->message }}</td>
                                        <td>{{ $item->form_type }}</td>
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $enquiryforms->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
