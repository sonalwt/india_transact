@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Merchant Service  {{ $merchantservice->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/merchantservices') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_merchantservices', 'delete_merchantservices')
                         <a href="{{ url('/admin/merchantservices/' . $merchantservice->id . '/edit') }}" title="Edit Merchantservice"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/merchantservices' . '/' . $merchantservice->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Merchantservice" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $merchantservice->id }}</td>
                                    </tr>
                                    <tr><th> Title1 </th><td> {{ $merchantservice->title1 }} </td></tr><tr><th> Title2 </th><td> {{ $merchantservice->title2 }} </td></tr><tr><th> Main Image </th><td><img src="{{URL::asset($merchantservice->main_image)}}" height="100" width="100"> </td></tr><tr><th> Description </th><td> {!! $merchantservice->description !!} </td></tr><tr><th> Status </th><td>@if($merchantservice->status==1) {{ 'Active' }}@else{{'Deactive'}}@endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
