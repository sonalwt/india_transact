 <label for="title1" style="font-size:18px;">{{ 'Title1' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title1" type="text" id="title1" value="{{ isset($merchantservice->title1) ? $merchantservice->title1 : ''}}" required>
     {!! $errors->first('title1', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="title2" style="font-size:18px;">{{ 'Title2' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title2" type="text" id="title2" value="{{ isset($merchantservice->title2) ? $merchantservice->title2 : ''}}" required>
     {!! $errors->first('title2', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="main_image" style="font-size:18px;">{{ 'Main Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="main_image" type="file" id="main_image" value="{{ isset($merchantservice->main_image) ? $merchantservice->main_image : ''}}">
     {!! $errors->first('main_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($merchantservice->main_image))
      <img src="{{URL::asset($merchantservice->main_image)}}" height="100" width="100">
      @endif
 </div>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" required>{{ isset($merchantservice->description) ? $merchantservice->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($merchantservice->status) && $merchantservice->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
