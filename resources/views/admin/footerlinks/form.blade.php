 <label for="facebook" style="font-size:18px;">{{ 'Facebook' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="facebook" type="text" id="facebook" value="{{ isset($footerlink->facebook) ? $footerlink->facebook : ''}}" required>
     {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="twitter" style="font-size:18px;">{{ 'Twitter' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="twitter" type="text" id="twitter" value="{{ isset($footerlink->twitter) ? $footerlink->twitter : ''}}" required>
     {!! $errors->first('twitter', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="instagram" style="font-size:18px;">{{ 'Instagram' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="instagram" type="text" id="instagram" value="{{ isset($footerlink->instagram) ? $footerlink->instagram : ''}}" required>
     {!! $errors->first('instagram', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="linkedin" style="font-size:18px;">{{ 'Linkedin' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="linkedin" type="text" id="linkedin" value="{{ isset($footerlink->linkedin) ? $footerlink->linkedin : ''}}" required>
     {!! $errors->first('linkedin', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="youtube" style="font-size:18px;">{{ 'Youtube' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="youtube" type="text" id="youtube" value="{{ isset($footerlink->youtube) ? $footerlink->youtube : ''}}" required>
     {!! $errors->first('youtube', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="ongomerchant" style="font-size:18px;">{{ 'Ongo Merchant++' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="ongomerchant" type="text" id="ongomerchant" value="{{ isset($footerlink->ongomerchant) ? $footerlink->ongomerchant : ''}}" >
     {!! $errors->first('ongomerchant', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="ongopaytrack" style="font-size:18px;">{{ 'Ongo PayTrack++' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="ongopaytrack" type="text" id="ongopaytrack" value="{{ isset($footerlink->ongopaytrack) ? $footerlink->ongopaytrack : ''}}" required>
     {!! $errors->first('ongopaytrack', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="ongobilling" style="font-size:18px;">{{ 'Ongo Billing++' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="ongobilling" type="text" id="ongobilling" value="{{ isset($footerlink->ongobilling) ? $footerlink->ongobilling : ''}}" required>
     {!! $errors->first('ongobilling', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="ongoqr" style="font-size:18px;">{{ 'Ongo QR++' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="ongoqr" type="text" id="ongoqr" value="{{ isset($footerlink->ongoqr) ? $footerlink->ongoqr : ''}}" required>
     {!! $errors->first('ongoqr', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
