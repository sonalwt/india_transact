 <label for="heading" style="font-size:18px;">{{ 'Heading' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="heading" type="text" id="heading" value="{{ isset($aboutusdetail->heading) ? $aboutusdetail->heading : ''}}" required>
     {!! $errors->first('heading', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" >{{ isset($aboutusdetail->description) ? $aboutusdetail->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="image" style="font-size:18px;">{{ 'Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($aboutusdetail->image) ? $aboutusdetail->image : ''}}">
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($aboutusdetail->image))
       <img src="{{ URL::asset($aboutusdetail->image) }}" height="100" width="100">
      @endif
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
