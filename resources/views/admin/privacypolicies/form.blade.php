 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($privacypolicy->title) ? $privacypolicy->title : ''}}" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" required>{{ isset($privacypolicy->description) ? $privacypolicy->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
<br>
 <label for="image" style="font-size:18px;">{{ 'Upload File' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($privacypolicy->image) ? $privacypolicy->image : ''}}">
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($privacypolicy->image))<a href="{{url($privacypolicy->image)}}" target="_blank"><i class="material-icons">file_copy</i></a></td> @endif
 </div>
<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
