@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Privacypolicy  {{ $privacypolicy->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/privacypolicies') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_privacypolicies', 'delete_privacypolicies')
                         <a href="{{ url('/admin/privacypolicies/' . $privacypolicy->id . '/edit') }}" title="Edit Privacypolicy"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/privacypolicies' . '/' . $privacypolicy->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Privacypolicy" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $privacypolicy->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $privacypolicy->title }} </td></tr><tr><th> Description </th><td> {{ $privacypolicy->description }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
