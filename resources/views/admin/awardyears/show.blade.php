@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Awardyear  {{ $awardyear->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/awardyears') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_awardyears', 'delete_awardyears')
                         <a href="{{ url('/admin/awardyears/' . $awardyear->id . '/edit') }}" title="Edit Awardyear"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/awardyears' . '/' . $awardyear->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Awardyear" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $awardyear->id }}</td>
                                    </tr>
                                    <tr><th> Year </th><td> {{ $awardyear->year }} </td></tr><tr><th> Year Status </th><td>@if($awardyear->year_status==1){{ 'Active'}}@else{{'Deactive'}}@endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
