 <label for="year" style="font-size:18px;">{{ 'Year' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="year" type="text" id="year" value="{{ isset($awardyear->year) ? $awardyear->year : ''}}" required>
     {!! $errors->first('year', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="year_status" style="font-size:18px;">{{ 'Year Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="year_status" class="form-control" id="year_status" >
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($awardyear->year_status) && $awardyear->year_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('year_status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
