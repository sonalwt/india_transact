 <label for="business_heading" style="font-size:18px;">{{ 'Business Heading' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="business_heading" type="text" id="business_heading" value="{{ isset($businessheading->business_heading) ? $businessheading->business_heading : ''}}" required>
     {!! $errors->first('business_heading', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="business_heading_sort_order" style="font-size:18px;">{{ 'Business Heading Sort Order' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="business_heading_sort_order" type="text" id="business_heading_sort_order" value="{{ isset($businessheading->business_heading_sort_order) ? $businessheading->business_heading_sort_order : ''}}" required>
     {!! $errors->first('business_heading_sort_order', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="business_heading_status" style="font-size:18px;">{{ 'Business Heading Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="business_heading_status" class="form-control" id="business_heading_status" required>
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($businessheading->business_heading_status) && $businessheading->business_heading_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('business_heading_status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
