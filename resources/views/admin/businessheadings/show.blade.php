@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Businessheading  {{ $businessheading->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/businessheadings') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_businessheadings', 'delete_businessheadings')
                         <a href="{{ url('/admin/businessheadings/' . $businessheading->id . '/edit') }}" title="Edit Businessheading"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/businessheadings' . '/' . $businessheading->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Businessheading" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $businessheading->id }}</td>
                                    </tr>
                                    <tr><th> Business Heading </th><td> {{ $businessheading->business_heading }} </td></tr>
                                    <tr><th> Business Heading Sort Order </th><td> {{ $businessheading->business_heading_sort_order }} </td></tr>
                                    <tr><th> Business Heading Status </th><td> @if($businessheading->business_heading_status==1) {{ 'Active' }}@else {{'Deactive'}}@endif</td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
