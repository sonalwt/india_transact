 <label for="headings" style="font-size:18px;">{{ 'Headings' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="headings" type="text" id="headings" value="{{ isset($homeoverview->headings) ? $homeoverview->headings : ''}}" required>
     {!! $errors->first('headings', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" required>{{ isset($homeoverview->description) ? $homeoverview->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="image" style="font-size:18px;">{{ 'Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($homeoverview->image) ? $homeoverview->image : ''}}">
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($homeoverview->image))
      <img src="{{URL::asset($homeoverview->image)}}" height="200" width="200">
      @endif
 </div>
  <br>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($homeoverview->status) && $homeoverview->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
