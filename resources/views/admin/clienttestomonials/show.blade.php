@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Client Testomonial  {{ $clienttestomonial->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/clienttestomonials') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_clienttestomonials', 'delete_clienttestomonials')
                         <a href="{{ url('/admin/clienttestomonials/' . $clienttestomonial->id . '/edit') }}" title="Edit Clienttestomonial"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/clienttestomonials' . '/' . $clienttestomonial->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Clienttestomonial" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $clienttestomonial->id }}</td>
                                    </tr>
                                    <tr><th> Client Message </th><td> {!! $clienttestomonial->client_message !!} </td></tr><tr><th> Client Name </th><td> {{ $clienttestomonial->client_name }} </td></tr><tr><th> Clienttestomonials Sort Order </th><td> {{ $clienttestomonial->clienttestomonials_sort_order }} </td></tr><tr><th> Clienttestomonials Status </th><td>@if($clienttestomonial->clienttestomonials_status==1){{'Active'}}@else {{'Deactive'}} @endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
