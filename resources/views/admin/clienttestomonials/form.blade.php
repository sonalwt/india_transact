 <label for="client_message" style="font-size:18px;">{{ 'Client Message' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="client_message" type="textarea" id="client_message" required>{{ isset($clienttestomonial->client_message) ? $clienttestomonial->client_message : ''}}</textarea>
     {!! $errors->first('client_message', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="client_name" style="font-size:18px;">{{ 'Client Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="client_name" type="text" id="client_name" value="{{ isset($clienttestomonial->client_name) ? $clienttestomonial->client_name : ''}}" >
     {!! $errors->first('client_name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="clienttestomonials_sort_order" style="font-size:18px;">{{ 'Clienttestomonials Sort Order' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="clienttestomonials_sort_order" type="text" id="clienttestomonials_sort_order" value="{{ isset($clienttestomonial->clienttestomonials_sort_order) ? $clienttestomonial->clienttestomonials_sort_order : ''}}" required>
     {!! $errors->first('clienttestomonials_sort_order', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="clienttestomonials_status" style="font-size:18px;">{{ 'Clienttestomonials Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="clienttestomonials_status" class="form-control" id="clienttestomonials_status" required>
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($clienttestomonial->clienttestomonials_status) && $clienttestomonial->clienttestomonials_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('clienttestomonials_status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
