@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Client Testomonial
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/clienttestomonials/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Clienttestomonial" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/clienttestomonials', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Client Message</th>
                                        <th>Client Name</th>
                                        <th>Clienttestomonials Sort Order</th>
                                        <th>Clienttestomonials Status</th>
                                        @can('view_clienttestomonials','edit_clienttestomonials', 'delete_clienttestomonials')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($clienttestomonials as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td width="300">{!! $item->client_message !!}</td>
                                        <td width="300">{{ $item->client_name }}</td>
                                        <td>{{ $item->clienttestomonials_sort_order }}</td>
                                        <td>@if($item->clienttestomonials_status==1){{'Active'}}@else {{'Deactive'}} @endif</td>
                                        @can('view_clienttestomonials')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/clienttestomonials/' . $item->id) }}" title="View Clienttestomonial"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'clienttestomonials',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $clienttestomonials->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
