 <label for="client_name" style="font-size:18px;">{{ 'Client Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="client_name" type="text" id="client_name" value="{{ isset($client->client_name) ? $client->client_name : ''}}" required>
     {!! $errors->first('client_name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="client_logo" style="font-size:18px;">{{ 'Client Logo' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="client_logo" type="file" id="client_logo" value="{{ isset($client->client_logo) ? $client->client_logo : ''}}" >
     {!! $errors->first('client_logo', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($client->client_logo))
      <img src="{{URL::asset($client->client_logo)}}" alt="{{$client->client_name}}" height="200" width="200">
      @endif
 </div>
  <br>
 <label for="client_sort_order" style="font-size:18px;">{{ 'Client Sort Order' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="client_sort_order" type="text" id="client_sort_order" value="{{ isset($client->client_sort_order) ? $client->client_sort_order : ''}}" required>
     {!! $errors->first('client_sort_order', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="client_status" style="font-size:18px;">{{ 'Client Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="client_status" class="form-control" id="client_status" required>
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($client->client_status) && $client->client_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('client_status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
