@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Client  {{ $client->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/clients') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_clients', 'delete_clients')
                         <a href="{{ url('/admin/clients/' . $client->id . '/edit') }}" title="Edit Client"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/clients' . '/' . $client->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Client" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $client->id }}</td>
                                    </tr>
                                    <tr><th> Client Name </th><td> {{ $client->client_name }} </td></tr><tr><th> Client Logo </th><td> @if(isset($client->client_logo))<img src="{{URL::asset($client->client_logo)}}" alt="{{ $client->client_name }}" height="100" width="100">@endif</td></tr><tr><th> Client Sort Order </th><td> {{ $client->client_sort_order }} </td></tr><tr><th> Client Status </th><td>@if($client->client_status==1) {{ 'Active' }} @else {{ 'Deactive'}}@endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
