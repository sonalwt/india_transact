@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Clients
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/clients/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Client" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/clients', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Client Name</th><th>Client Logo</th><th>Client Sort Order</th><th>Client Status</th>
                                        @can('view_clients','edit_clients', 'delete_clients')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($clients as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->client_name }}</td>
                                        <td>@if(isset($item->client_logo))<img src="{{URL::asset($item->client_logo)}}" alt="{{ $item->client_name }}" height="100" width="100">@endif</td>
                                        <td>{{ $item->client_sort_order }}</td>
                                        <td>@if( $item->client_status==1) {{ 'Active'}}@else {{'Deactive'}} @endif</td>
                                        @can('view_clients')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/clients/' . $item->id) }}" title="View Client"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'clients',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $clients->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
