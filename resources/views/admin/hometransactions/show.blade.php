@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Hometransaction  {{ $hometransaction->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/hometransactions') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_hometransactions', 'delete_hometransactions')
                         <a href="{{ url('/admin/hometransactions/' . $hometransaction->id . '/edit') }}" title="Edit Hometransaction"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/hometransactions' . '/' . $hometransaction->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Hometransaction" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $hometransaction->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $hometransaction->title }} </td></tr>
                                    <tr><th> Heading </th><td> {{ $hometransaction->hometransactionheading->headings }} </td></tr>
                                    <tr><th> Description </th><td> {!! $hometransaction->description !!} </td></tr>
                                    <tr><th> Main Image </th><td><img src="{{ URL::asset($hometransaction->main_image) }}" height="50" width="50"></td></tr>
                                    <tr><th> Icon Image </th><td> <img src="{{ URL::asset($hometransaction->icon_image) }}" height="50" width="50"> </td></tr>
                                    <tr><th> Selected Icon Image </th><td> <img src="{{URL::asset($hometransaction->selected_icon_image)}}" height="50" width="50" style="background-color: #2b972b"> </td></tr>
                                    <tr><th> Status </th><td>@if($hometransaction->status==1) {{ 'Active' }}@else{{'Deactive'}}@endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
