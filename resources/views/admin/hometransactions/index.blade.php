@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Hometransactions
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/hometransactions/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Hometransaction" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/hometransactions', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Title</th><th>Main Image</th><th>Icon Image</th><th>Selected Icon Image</th>
                                        @can('view_hometransactions','edit_hometransactions', 'delete_hometransactions')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($hometransactions as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->title }}</td><td><img src="{{ URL::asset($item->main_image) }}" height="50" width="50"></td><td><img src="{{ URL::asset($item->icon_image) }}" height="50" width="50"></td><td><img src="{{URL::asset($item->selected_icon_image)}}" height="50" width="50" style="background-color: #2b972b"></td>
                                        @can('view_hometransactions')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/hometransactions/' . $item->id) }}" title="View Hometransaction"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'hometransactions',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $hometransactions->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
