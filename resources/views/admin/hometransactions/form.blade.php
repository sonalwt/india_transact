 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($hometransaction->title) ? $hometransaction->title : ''}}" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="heading_id" style="font-size:18px;">{{ 'Heading' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="heading_id" type="heading_id" id="heading_id" placeholder="{{$headings->headings}}" value="{{ isset($headings->id) ? $headings->id : ''}}" required>
     {!! $errors->first('heading_id', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" required>{{ isset($hometransaction->description) ? $hometransaction->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="main_image" style="font-size:18px;">{{ 'Main Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="main_image" type="file" id="main_image" value="{{ isset($hometransaction->main_image) ? $hometransaction->main_image : ''}}" >
     {!! $errors->first('main_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($hometransaction->main_image)) <img src="{{URL::asset($hometransaction->main_image)}}" height="100" width="100">@endif
 </div>
  <br>
 <label for="icon_image" style="font-size:18px;">{{ 'Icon Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="icon_image" type="file" id="icon_image" value="{{ isset($hometransaction->icon_image) ? $hometransaction->icon_image : ''}}" >
     {!! $errors->first('icon_image', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($hometransaction->icon_image)) <img src="{{URL::asset($hometransaction->icon_image)}}" height="100" width="100">@endif
 </div>
  <br>
 <label for="selected_icon_image" style="font-size:18px;">{{ 'Selected Icon Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="selected_icon_image" type="file" id="selected_icon_image" value="{{ isset($hometransaction->selected_icon_image) ? $hometransaction->selected_icon_image : ''}}" >
     {!! $errors->first('selected_icon_image', '<p class="help-block">:message</p>') !!}
      </div>
        @if(isset($hometransaction->selected_icon_image)) <img src="{{URL::asset($hometransaction->selected_icon_image)}}" height="100" width="100" style="background-color: #2b972b">@endif
 </div>
  <br>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($hometransaction->status) && $hometransaction->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
