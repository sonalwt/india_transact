@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            websitedetail  {{ $websitedetail->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/websitedetails') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_websitedetails', 'delete_websitedetails')
                         <a href="{{ url('/admin/websitedetails/' . $websitedetail->id . '/edit') }}" title="Edit websitedetail"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/websitedetails' . '/' . $websitedetail->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete websitedetail" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $websitedetail->id }}</td>
                                    </tr>
                                    <tr><th> Logo </th><td> {{ $websitedetail->logo }} </td></tr><tr><th> Favicon </th><td> {{ $websitedetail->favicon }} </td></tr><tr><th> Index Page Title </th><td> {{ $websitedetail->index_page_title }} </td></tr><tr><th> Index Page Keyword </th><td> {{ $websitedetail->index_page_keyword }} </td></tr><tr><th> Index Page Description </th><td> {{ $websitedetail->index_page_description }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
