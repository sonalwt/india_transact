 <label for="logo" style="font-size:18px;">{{ 'Logo' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="logo" type="file" id="logo" value="{{ isset($websitedetail->logo) ? $websitedetail->logo : ''}}" >
     {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($websitedetail->logo))
       <img src="{{URL::asset($websitedetail->logo)}}" height="50" width="50">
       @endif
 </div>

 <br>
 <label for="favicon" style="font-size:18px;">{{ 'Favicon' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="favicon" type="file" id="favicon" value="{{ isset($websitedetail->favicon) ? $websitedetail->favicon : ''}}" >
     {!! $errors->first('favicon', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($websitedetail->favicon))
       <img src="{{URL::asset($websitedetail->favicon)}}" height="50" width="50">
       @endif
 </div>
 <br>
 <label for="index_page_title" style="font-size:18px;">{{ 'Index Page Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="index_page_title" type="text" id="index_page_title" value="{{ isset($websitedetail->index_page_title) ? $websitedetail->index_page_title : ''}}" >
     {!! $errors->first('index_page_title', '<p class="help-block">:message</p>') !!}
      </div>

 </div>
 <br>
 <label for="index_page_keyword" style="font-size:18px;">{{ 'Index Page Keyword' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="index_page_keyword" type="text" id="index_page_keyword" value="{{ isset($websitedetail->index_page_keyword) ? $websitedetail->index_page_keyword : ''}}" >
     {!! $errors->first('index_page_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="index_page_description" style="font-size:18px;">{{ 'Index Page Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="index_page_description" type="text" id="index_page_description" value="{{ isset($websitedetail->index_page_description) ? $websitedetail->index_page_description : ''}}" required>
     {!! $errors->first('index_page_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="index_page_url" style="font-size:18px;">{{ 'Index Page Url' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="index_page_url" type="text" id="index_page_url" value="{{ isset($websitedetail->index_page_url) ? $websitedetail->index_page_url : ''}}" >
     {!! $errors->first('index_page_url', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="index_page_image" style="font-size:18px;">{{ 'Index Page Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="index_page_image" type="file" id="index_page_image" value="{{ isset($websitedetail->index_page_image) ? $websitedetail->index_page_image : ''}}" >
     {!! $errors->first('index_page_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($websitedetail->index_page_image))
      <img src="{{URL::asset($websitedetail->index_page_image)}}" height="50" width="50">
      @endif
 </div>
 <br>
 <label for="about_page_title" style="font-size:18px;">{{ 'About Page Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="about_page_title" type="text" id="about_page_title" value="{{ isset($websitedetail->about_page_title) ? $websitedetail->about_page_title : ''}}" >
     {!! $errors->first('about_page_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="about_page_keyword" style="font-size:18px;">{{ 'About Page Keyword' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="about_page_keyword" type="text" id="about_page_keyword" value="{{ isset($websitedetail->about_page_keyword) ? $websitedetail->about_page_keyword : ''}}" >
     {!! $errors->first('about_page_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="about_page_description" style="font-size:18px;">{{ 'About Page Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="about_page_description" type="text" id="about_page_description" value="{{ isset($websitedetail->about_page_description) ? $websitedetail->about_page_description : ''}}" >
     {!! $errors->first('about_page_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="about_page_url" style="font-size:18px;">{{ 'About Page Url' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="about_page_url" type="text" id="about_page_url" value="{{ isset($websitedetail->about_page_url) ? $websitedetail->about_page_url : ''}}" >
     {!! $errors->first('about_page_url', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="about_page_image" style="font-size:18px;">{{ 'About Page Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="about_page_image" type="file" id="about_page_image" value="{{ isset($websitedetail->about_page_image) ? $websitedetail->about_page_image : ''}}" >
     {!! $errors->first('about_page_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($websitedetail->about_page_image))
      <img src="{{URL::asset($websitedetail->about_page_image)}}" height="50" width="50">
      @endif
 </div>
 <br>
 <label for="product_page_title" style="font-size:18px;">{{ 'Product Page Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="product_page_title" type="text" id="product_page_title" value="{{ isset($websitedetail->product_page_title) ? $websitedetail->product_page_title : ''}}" >
     {!! $errors->first('product_page_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="product_page_keyword" style="font-size:18px;">{{ 'Product Page Keyword' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="product_page_keyword" type="text" id="product_page_keyword" value="{{ isset($websitedetail->product_page_keyword) ? $websitedetail->product_page_keyword : ''}}" >
     {!! $errors->first('product_page_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="product_page_description" style="font-size:18px;">{{ 'Product Page Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="product_page_description" type="text" id="product_page_description" value="{{ isset($websitedetail->product_page_description) ? $websitedetail->product_page_description : ''}}" >
     {!! $errors->first('product_page_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="product_page_url" style="font-size:18px;">{{ 'Product Page Url' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="product_page_url" type="text" id="product_page_url" value="{{ isset($websitedetail->product_page_url) ? $websitedetail->product_page_url : ''}}" >
     {!! $errors->first('product_page_url', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="product_page_image" style="font-size:18px;">{{ 'Product Page Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="product_page_image" type="file" id="product_page_image" value="{{ isset($websitedetail->product_page_image) ? $websitedetail->product_page_image : ''}}" >
     {!! $errors->first('product_page_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($websitedetail->product_page_image))
      <img src="{{URL::asset($websitedetail->product_page_image)}}" height="50" width="50">
      @endif
 </div>
 <br>
 <label for="service_page_title" style="font-size:18px;">{{ 'Service Page Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="service_page_title" type="text" id="service_page_title" value="{{ isset($websitedetail->service_page_title) ? $websitedetail->service_page_title : ''}}" >
     {!! $errors->first('service_page_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="service_page_keyword" style="font-size:18px;">{{ 'Service Page Keyword' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="service_page_keyword" type="text" id="service_page_keyword" value="{{ isset($websitedetail->service_page_keyword) ? $websitedetail->service_page_keyword : ''}}" >
     {!! $errors->first('service_page_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="service_page_description" style="font-size:18px;">{{ 'Service Page Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="service_page_description" type="text" id="service_page_description" value="{{ isset($websitedetail->service_page_description) ? $websitedetail->service_page_description : ''}}" >
     {!! $errors->first('service_page_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="service_page_url" style="font-size:18px;">{{ 'Service Page Url' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="service_page_url" type="text" id="service_page_url" value="{{ isset($websitedetail->service_page_url) ? $websitedetail->service_page_url : ''}}" >
     {!! $errors->first('service_page_url', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="service_page_image" style="font-size:18px;">{{ 'Service Page Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="service_page_image" type="file" id="service_page_image" value="{{ isset($websitedetail->service_page_image) ? $websitedetail->service_page_image : ''}}" >
     {!! $errors->first('service_page_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($websitedetail->service_page_image))
      <img src="{{URL::asset($websitedetail->service_page_image)}}" height="50" width="50">
      @endif
 </div>
 <br>
 <label for="business_vertical_page_title" style="font-size:18px;">{{ 'Business Vertical Page Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="business_vertical_page_title" type="text" id="business_vertical_page_title" value="{{ isset($websitedetail->business_vertical_page_title) ? $websitedetail->business_vertical_page_title : ''}}" >
     {!! $errors->first('business_vertical_page_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="business_vertical_page_keyword" style="font-size:18px;">{{ 'Business Vertical Page Keyword' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="business_vertical_page_keyword" type="text" id="business_vertical_page_keyword" value="{{ isset($websitedetail->business_vertical_page_keyword) ? $websitedetail->business_vertical_page_keyword : ''}}" >
     {!! $errors->first('business_vertical_page_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="business_vertical_page_description" style="font-size:18px;">{{ 'Business Vertical Page Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="business_vertical_page_description" type="text" id="business_vertical_page_description" value="{{ isset($websitedetail->business_vertical_page_description) ? $websitedetail->business_vertical_page_description : ''}}" >
     {!! $errors->first('business_vertical_page_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="business_vertical_page_url" style="font-size:18px;">{{ 'Business Vertical Page Url' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="business_vertical_page_url" type="text" id="business_vertical_page_url" value="{{ isset($websitedetail->business_vertical_page_url) ? $websitedetail->business_vertical_page_url : ''}}" >
     {!! $errors->first('business_vertical_page_url', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="business_vertical_page_image" style="font-size:18px;">{{ 'Business Vertical Page Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="business_vertical_page_image" type="file" id="business_vertical_page_image" value="{{ isset($websitedetail->business_vertical_page_image) ? $websitedetail->business_vertical_page_image : ''}}" >
     {!! $errors->first('business_vertical_page_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($websitedetail->business_vertical_page_image))
      <img src="{{URL::asset($websitedetail->business_vertical_page_image)}}" height="50" width="50">
      @endif
 </div>
 <br>
 <label for="client_page_title" style="font-size:18px;">{{ 'Client Page Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="client_page_title" type="text" id="client_page_title" value="{{ isset($websitedetail->client_page_title) ? $websitedetail->client_page_title : ''}}" >
     {!! $errors->first('client_page_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="client_page_keyword" style="font-size:18px;">{{ 'Client Page Keyword' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="client_page_keyword" type="text" id="client_page_keyword" value="{{ isset($websitedetail->client_page_keyword) ? $websitedetail->client_page_keyword : ''}}" >
     {!! $errors->first('client_page_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="client_page_description" style="font-size:18px;">{{ 'Client Page Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="client_page_description" type="text" id="client_page_description" value="{{ isset($websitedetail->client_page_description) ? $websitedetail->client_page_description : ''}}" >
     {!! $errors->first('client_page_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="client_page_url" style="font-size:18px;">{{ 'Client Page Url' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="client_page_url" type="text" id="client_page_url" value="{{ isset($websitedetail->client_page_url) ? $websitedetail->client_page_url : ''}}" >
     {!! $errors->first('client_page_url', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="client_page_image" style="font-size:18px;">{{ 'Client Page Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="client_page_image" type="file" id="client_page_image" value="{{ isset($websitedetail->client_page_image) ? $websitedetail->client_page_image : ''}}" >
     {!! $errors->first('client_page_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($websitedetail->client_page_image))
      <img src="{{URL::asset($websitedetail->client_page_image)}}" height="50" width="50">
      @endif
 </div>
 <br>
 <label for="awards_page_title" style="font-size:18px;">{{ 'Awards Page Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="awards_page_title" type="text" id="awards_page_title" value="{{ isset($websitedetail->awards_page_title) ? $websitedetail->awards_page_title : ''}}" >
     {!! $errors->first('awards_page_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="awards_page_keyword" style="font-size:18px;">{{ 'Awards Page Keyword' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="awards_page_keyword" type="text" id="awards_page_keyword" value="{{ isset($websitedetail->awards_page_keyword) ? $websitedetail->awards_page_keyword : ''}}" >
     {!! $errors->first('awards_page_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="awards_page_description" style="font-size:18px;">{{ 'Awards Page Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="awards_page_description" type="text" id="awards_page_description" value="{{ isset($websitedetail->awards_page_description) ? $websitedetail->awards_page_description : ''}}" >
     {!! $errors->first('awards_page_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="awards_page_url" style="font-size:18px;">{{ 'Awards Page Url' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="awards_page_url" type="text" id="awards_page_url" value="{{ isset($websitedetail->awards_page_url) ? $websitedetail->awards_page_url : ''}}" >
     {!! $errors->first('awards_page_url', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="awards_page_image" style="font-size:18px;">{{ 'Awards Page Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="awards_page_image" type="file" id="awards_page_image" value="{{ isset($websitedetail->awards_page_image) ? $websitedetail->awards_page_image : ''}}" >
     {!! $errors->first('awards_page_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($websitedetail->awards_page_image))
      <img src="{{URL::asset($websitedetail->awards_page_image)}}" height="50" width="50">
      @endif
 </div>
 <br>
 <label for="news_page_title" style="font-size:18px;">{{ 'News Page Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="news_page_title" type="text" id="news_page_title" value="{{ isset($websitedetail->news_page_title) ? $websitedetail->news_page_title : ''}}" >
     {!! $errors->first('news_page_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="news_page_keyword" style="font-size:18px;">{{ 'News Page Keyword' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="news_page_keyword" type="text" id="news_page_keyword" value="{{ isset($websitedetail->news_page_keyword) ? $websitedetail->news_page_keyword : ''}}" >
     {!! $errors->first('news_page_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="news_page_description" style="font-size:18px;">{{ 'News Page Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="news_page_description" type="text" id="news_page_description" value="{{ isset($websitedetail->news_page_description) ? $websitedetail->news_page_description : ''}}" >
     {!! $errors->first('news_page_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="news_page_url" style="font-size:18px;">{{ 'News Page Url' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="news_page_url" type="text" id="news_page_url" value="{{ isset($websitedetail->news_page_url) ? $websitedetail->news_page_url : ''}}" >
     {!! $errors->first('news_page_url', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="news_page_image" style="font-size:18px;">{{ 'News Page Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="news_page_image" type="file" id="news_page_image" value="{{ isset($websitedetail->news_page_image) ? $websitedetail->news_page_image : ''}}" >
     {!! $errors->first('news_page_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($websitedetail->news_page_image))
      <img src="{{URL::asset($websitedetail->news_page_image)}}" height="50" width="50">
      @endif
 </div>
 <br>
 <label for="contact_page_title" style="font-size:18px;">{{ 'Contact Page Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="contact_page_title" type="text" id="contact_page_title" value="{{ isset($websitedetail->contact_page_title) ? $websitedetail->contact_page_title : ''}}" >
     {!! $errors->first('contact_page_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="contact_page_keyword" style="font-size:18px;">{{ 'Contact Page Keyword' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="contact_page_keyword" type="text" id="contact_page_keyword" value="{{ isset($websitedetail->contact_page_keyword) ? $websitedetail->contact_page_keyword : ''}}" >
     {!! $errors->first('contact_page_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="contact_page_description" style="font-size:18px;">{{ 'Contact Page Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="contact_page_description" type="text" id="contact_page_description" value="{{ isset($websitedetail->contact_page_description) ? $websitedetail->contact_page_description : ''}}" >
     {!! $errors->first('contact_page_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="contact_page_url" style="font-size:18px;">{{ 'Contact Page Url' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="contact_page_url" type="text" id="contact_page_url" value="{{ isset($websitedetail->contact_page_url) ? $websitedetail->contact_page_url : ''}}" >
     {!! $errors->first('contact_page_url', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="contact_page_image" style="font-size:18px;">{{ 'Contact Page Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="contact_page_image" type="file" id="contact_page_image" value="{{ isset($websitedetail->contact_page_image) ? $websitedetail->contact_page_image : ''}}" >
     {!! $errors->first('contact_page_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($websitedetail->contact_page_image))
      <img src="{{URL::asset($websitedetail->contact_page_image)}}" height="50" width="50">
      @endif
 </div>
<br>

<label for="privacy_policy_page_title" style="font-size:18px;">{{ 'Privacy Policy Page Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="privacy_policy_page_title" type="text" id="privacy_policy_page_title" value="{{ isset($websitedetail->privacy_policy_page_title) ? $websitedetail->privacy_policy_page_title : ''}}" >
     {!! $errors->first('privacy_policy_page_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="privacy_policy_page_keyword" style="font-size:18px;">{{ 'Privacy Policy Page Keyword' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="privacy_policy_page_keyword" type="text" id="privacy_policy_page_keyword" value="{{ isset($websitedetail->privacy_policy_page_keyword) ? $websitedetail->privacy_policy_page_keyword : ''}}" >
     {!! $errors->first('privacy_policy_page_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="privacy_policy_page_description" style="font-size:18px;">{{ 'Privacy Policy Page Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="privacy_policy_page_description" type="text" id="privacy_policy_page_description" value="{{ isset($websitedetail->privacy_policy_page_description) ? $websitedetail->privacy_policy_page_description : ''}}" >
     {!! $errors->first('privacy_policy_page_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="privacy_policy_page_url" style="font-size:18px;">{{ 'Privacy Policy Page Url' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="privacy_policy_page_url" type="text" id="privacy_policy_page_url" value="{{ isset($websitedetail->privacy_policy_page_url) ? $websitedetail->privacy_policy_page_url : ''}}" >
     {!! $errors->first('privacy_policy_page_url', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="privacy_policy_page_image" style="font-size:18px;">{{ 'Privacy Policy Page Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="privacy_policy_page_image" type="file" id="privacy_policy_page_image" value="{{ isset($websitedetail->privacy_policy_page_image) ? $websitedetail->privacy_policy_page_image : ''}}" >
     {!! $errors->first('privacy_policy_page_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($websitedetail->privacy_policy_page_image))
      <img src="{{URL::asset($websitedetail->privacy_policy_page_image)}}" height="50" width="50">
      @endif
 </div>
<br>


<label for="disclaimer_policy_page_title" style="font-size:18px;">{{ 'Disclaimer Policy Page Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="disclaimer_policy_page_title" type="text" id="disclaimer_policy_page_title" value="{{ isset($websitedetail->disclaimer_policy_page_title) ? $websitedetail->disclaimer_policy_page_title : ''}}" >
     {!! $errors->first('disclaimer_policy_page_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="disclaimer_policy_page_keyword" style="font-size:18px;">{{ 'Disclaimer Policy Page Keyword' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="disclaimer_policy_page_keyword" type="text" id="disclaimer_policy_page_keyword" value="{{ isset($websitedetail->disclaimer_policy_page_keyword) ? $websitedetail->disclaimer_policy_page_keyword : ''}}" >
     {!! $errors->first('disclaimer_policy_page_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="disclaimer_policy_page_description" style="font-size:18px;">{{ 'Disclaimer Policy Page Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="disclaimer_policy_page_description" type="text" id="disclaimer_policy_page_description" value="{{ isset($websitedetail->disclaimer_policy_page_description) ? $websitedetail->disclaimer_policy_page_description : ''}}" >
     {!! $errors->first('disclaimer_policy_page_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="disclaimer_policy_page_url" style="font-size:18px;">{{ 'Disclaimer Policy Page Url' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="disclaimer_policy_page_url" type="text" id="disclaimer_policy_page_url" value="{{ isset($websitedetail->disclaimer_policy_page_url) ? $websitedetail->disclaimer_policy_page_url : ''}}" >
     {!! $errors->first('disclaimer_policy_page_url', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="disclaimer_policy_page_image" style="font-size:18px;">{{ 'Disclaimer Policy Page Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="disclaimer_policy_page_image" type="file" id="disclaimer_policy_page_image" value="{{ isset($websitedetail->disclaimer_policy_page_image) ? $websitedetail->disclaimer_policy_page_image : ''}}" >
     {!! $errors->first('disclaimer_policy_page_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($websitedetail->disclaimer_policy_page_image))
      <img src="{{URL::asset($websitedetail->disclaimer_policy_page_image)}}" height="50" width="50">
      @endif
 </div>

 <br>


<label for="ewaste_policy_page_title" style="font-size:18px;">{{ 'E-waste Policy Page Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="ewaste_policy_page_title" type="text" id="ewaste_policy_page_title" value="{{ isset($websitedetail->ewaste_policy_page_title) ? $websitedetail->ewaste_policy_page_title : ''}}" >
     {!! $errors->first('ewaste_policy_page_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="ewaste_policy_page_keyword" style="font-size:18px;">{{ 'E-waste Policy Page Keyword' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="ewaste_policy_page_keyword" type="text" id="ewaste_policy_page_keyword" value="{{ isset($websitedetail->ewaste_policy_page_keyword) ? $websitedetail->ewaste_policy_page_keyword : ''}}" >
     {!! $errors->first('ewaste_policy_page_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="ewaste_policy_page_description" style="font-size:18px;">{{ 'E-waste Policy Page Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="ewaste_policy_page_description" type="text" id="ewaste_policy_page_description" value="{{ isset($websitedetail->ewaste_policy_page_description) ? $websitedetail->ewaste_policy_page_description : ''}}" >
     {!! $errors->first('ewaste_policy_page_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="ewaste_policy_page_url" style="font-size:18px;">{{ 'E-waste Policy Page Url' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="ewaste_policy_page_url" type="text" id="ewaste_policy_page_url" value="{{ isset($websitedetail->ewaste_policy_page_url) ? $websitedetail->ewaste_policy_page_url : ''}}" >
     {!! $errors->first('ewaste_policy_page_url', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="ewaste_policy_page_image" style="font-size:18px;">{{ 'E-waste Policy Page Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="ewaste_policy_page_image" type="file" id="ewaste_policy_page_image" value="{{ isset($websitedetail->ewaste_policy_page_image) ? $websitedetail->ewaste_policy_page_image : ''}}" >
     {!! $errors->first('ewaste_policy_page_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($websitedetail->ewaste_policy_page_image))
      <img src="{{URL::asset($websitedetail->ewaste_policy_page_image)}}" height="50" width="50">
      @endif
 </div>
<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
