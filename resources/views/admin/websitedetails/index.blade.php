@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Websitedetails
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/websitedetails/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New websitedetail" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/websitedetails', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Logo</th><th>Favicon</th><th>Index Page Title</th><th>Index Page Keyword</th><th>Index Page Description</th>
                                        @can('view_websitedetails','edit_websitedetails', 'delete_websitedetails')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($websitedetails as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->logo }}</td><td>{{ $item->favicon }}</td><td>{{ $item->index_page_title }}</td><td>{{ $item->index_page_keyword }}</td><td>{{ $item->index_page_description }}</td>
                                        @can('view_websitedetails')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/websitedetails/' . $item->id) }}" title="View websitedetail"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'websitedetails',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $websitedetails->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
