 <label for="award_details" style="font-size:18px;">{{ 'Award Details' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="award_details" type="textarea" id="award_details" required>{{ isset($award->award_details) ? $award->award_details : ''}}</textarea>
     {!! $errors->first('award_details', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="award_year" style="font-size:18px;">{{ 'Award Year' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="award_year" class="form-control" id="award_year" >
    @foreach ($awardyears as  $awardyear)
        <option value="{{$awardyear->id}}" {{ (isset($award->award_year) && $award->award_year == $awardyear->id) ? 'selected' : ''}}>{{ $awardyear->year }}</option>
    @endforeach
</select>
     {!! $errors->first('award_year', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="award_sort_order" style="font-size:18px;">{{ 'Award Sort Order' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="award_sort_order" type="text" id="award_sort_order" value="{{ isset($award->award_sort_order) ? $award->award_sort_order : ''}}" >
     {!! $errors->first('award_sort_order', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="year_status" style="font-size:18px;">{{ 'Year Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="year_status" class="form-control" id="year_status" >
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($award->year_status) && $award->year_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('year_status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
