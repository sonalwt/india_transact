@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Award  {{ $award->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/awards') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_awards', 'delete_awards')
                         <a href="{{ url('/admin/awards/' . $award->id . '/edit') }}" title="Edit Award"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/awards' . '/' . $award->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Award" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $award->id }}</td>
                                    </tr>
                                    <tr><th> Award Details </th><td> {!! $award->award_details !!} </td></tr><tr><th> Award Year </th><td> {{ $award->awardyear->year }} </td></tr><tr><th> Award Sort Order </th><td> {{ $award->award_sort_order }} </td></tr><tr><th> Year Status </th><td>@if($award->year_status==1){{'Active'}}@else{{'Deactive'}}@endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
