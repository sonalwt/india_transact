 <label for="headings" style="font-size:18px;">{{ 'Headings' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="headings" type="text" id="headings" value="{{ isset($hometransactionheading->headings) ? $hometransactionheading->headings : ''}}" required>
     {!! $errors->first('headings', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($hometransactionheading->status) && $hometransactionheading->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
