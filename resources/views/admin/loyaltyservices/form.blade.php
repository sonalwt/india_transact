 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($loyaltyservice->title) ? $loyaltyservice->title : ''}}" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="image1" style="font-size:18px;">{{ 'Image1' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="image1" type="file" id="image1" value="{{ isset($loyaltyservice->image1) ? $loyaltyservice->image1 : ''}}" >
     {!! $errors->first('image1', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($loyaltyservice->image1))
      <img src="{{URL::asset($loyaltyservice->image1)}}" height="100" width="100">
      @endif
 </div>
 <br>
 <label for="image2" style="font-size:18px;">{{ 'Image2' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="image2" type="file" id="image2" value="{{ isset($loyaltyservice->image2) ? $loyaltyservice->image2 : ''}}" >
     {!! $errors->first('image2', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($loyaltyservice->image2))
      <img src="{{URL::asset($loyaltyservice->image2)}}" height="100" width="100">
      @endif
 </div>
 <br>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" >{{ isset($loyaltyservice->description) ? $loyaltyservice->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
