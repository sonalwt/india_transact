 <label for="doc_file" style="font-size:18px;">{{ 'Doc File' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="doc_file" type="file" id="doc_file" value="{{ isset($investorrelation->doc_file) ? $investorrelation->doc_file : ''}}" required>
     {!! $errors->first('doc_file', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($investorrelation->doc_file))<a href="{{url($investorrelation->doc_file)}}" target="_blank"><i class="material-icons">file_copy</i></a></td> @endif
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
