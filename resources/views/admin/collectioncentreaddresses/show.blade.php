@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Collection Centre Address  {{ $collectioncentreaddress->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/collectioncentreaddresses') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_collectioncentreaddresses', 'delete_collectioncentreaddresses')
                         <a href="{{ url('/admin/collectioncentreaddresses/' . $collectioncentreaddress->id . '/edit') }}" title="Edit Collectioncentreaddress"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/collectioncentreaddresses' . '/' . $collectioncentreaddress->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Collectioncentreaddress" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $collectioncentreaddress->id }}</td>
                                    </tr>
                                    <tr><th> State </th><td> {{ $collectioncentreaddress->state }} </td></tr><tr><th> Collection Centre Address </th><td> {{ $collectioncentreaddress->collection_centre_address }} </td></tr><tr><th> Toll Free No </th><td> {{ $collectioncentreaddress->toll_free_no }} </td></tr><tr><th> Status </th><td> @if($collectioncentreaddress->status==1){{'Active' }}@else{{'Deactive'}}@endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
