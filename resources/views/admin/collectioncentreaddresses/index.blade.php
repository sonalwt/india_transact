@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Collection Centre Address
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/collectioncentreaddresses/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Collectioncentreaddress" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/collectioncentreaddresses', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>State</th><th>Collection Centre Address</th><th>Toll Free No</th><th>Status</th>
                                        @can('view_collectioncentreaddresses','edit_collectioncentreaddresses', 'delete_collectioncentreaddresses')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($collectioncentreaddresses as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->state }}</td><td width="400">{{ $item->collection_centre_address }}</td><td>{{ $item->toll_free_no }}</td><td>@if($item->status==1){{'Active' }}@else{{'Deactive'}}@endif</td>
                                        @can('view_collectioncentreaddresses')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/collectioncentreaddresses/' . $item->id) }}" title="View Collectioncentreaddress"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'collectioncentreaddresses',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $collectioncentreaddresses->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
