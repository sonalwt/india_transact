 <label for="state" style="font-size:18px;">{{ 'State' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="state" type="text" id="state" value="{{ isset($collectioncentreaddress->state) ? $collectioncentreaddress->state : ''}}" required>
     {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="collection_centre_address" style="font-size:18px;">{{ 'Collection Centre Address' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="collection_centre_address" type="text" id="collection_centre_address" value="{{ isset($collectioncentreaddress->collection_centre_address) ? $collectioncentreaddress->collection_centre_address : ''}}" required>
     {!! $errors->first('collection_centre_address', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="toll_free_no" style="font-size:18px;">{{ 'Toll Free No' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="toll_free_no" type="text" id="toll_free_no" value="{{ isset($collectioncentreaddress->toll_free_no) ? $collectioncentreaddress->toll_free_no : ''}}" >
     {!! $errors->first('toll_free_no', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($collectioncentreaddress->status) && $collectioncentreaddress->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
