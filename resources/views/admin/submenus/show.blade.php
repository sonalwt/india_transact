@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Submenu  {{ $submenu->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/submenus') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_submenus', 'delete_submenus')
                         <a href="{{ url('/admin/submenus/' . $submenu->id . '/edit') }}" title="Edit Submenu"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/submenus' . '/' . $submenu->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Submenu" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $submenu->id }}</td>
                                    </tr>
                                    
                                    <tr><th> Sub Menu </th><td> {{ $submenu->sub_menu }} </td></tr><tr><th> Menu Id </th><td> {{ $submenu->menus->menu }} </td></tr><tr><th> Submenu Link </th><td> {{ $submenu->submenu_link }} </td></tr><tr><th> Submenu Sort Order </th><td> {{ $submenu->submenu_sort_order }} </td></tr><tr><th> Submenu Status </th><td> @if($submenu->submenu_status=='1'){{  'Active'}}@else{{'Deactive'}}@endif</td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
