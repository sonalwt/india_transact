 <label for="sub_menu" style="font-size:18px;">{{ 'Sub Menu' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="sub_menu" type="text" id="sub_menu" value="{{ isset($submenu->sub_menu) ? $submenu->sub_menu : ''}}" required>
     {!! $errors->first('sub_menu', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="menu_id" style="font-size:18px;">{{ 'Select Main Menu' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="menu_id" class="form-control" id="menu_id" >
          @foreach($menus as $menu)
   
        <option value="{{ $menu->id }}" @if(isset($submenu->menu_id) && ($submenu->menu_id)== $menu->id){{'selected'}}@endif>{{$menu->menu}}</option>
        @endforeach
</select>
     {!! $errors->first('menu_id', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="submenu_link" style="font-size:18px;">{{ 'Submenu Link' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="submenu_link" type="text" id="submenu_link" value="{{ isset($submenu->submenu_link) ? $submenu->submenu_link : ''}}" >
     {!! $errors->first('submenu_link', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="submenu_sort_order" style="font-size:18px;">{{ 'Submenu Sort Order' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="submenu_sort_order" type="text" id="submenu_sort_order" value="{{ isset($submenu->submenu_sort_order) ? $submenu->submenu_sort_order : ''}}" required>
     {!! $errors->first('submenu_sort_order', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="submenu_status" style="font-size:18px;">{{ 'Submenu Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="submenu_status" class="form-control" id="submenu_status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($submenu->submenu_status) && $submenu->submenu_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('submenu_status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
