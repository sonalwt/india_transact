<aside id="leftsidebar" class="sidebar">
    <!-- User Info -->
    <div class="user-info">
        <div class="image">
            @if(Auth::guard('admin')->user()->image)
                <img src="{{asset('images/profile_image/'.Auth::guard('admin')->user()->image)}}" width="48" height="48" alt="User" />
                @else
            <img src="{{asset('images/user.png')}}" width="48" height="48" alt="User" />
                @endif
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::guard('admin')->user()->name}}</div>
            <div class="email">{{Auth::guard('admin')->user()->email}}</div>
            <div class="btn-group user-helper-dropdown">
                <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                <ul class="dropdown-menu pull-right">

                    <li><a href="{{url('/admin/profile')}}"><i class="material-icons">person</i>Profile</a></li>
                    <li role="seperator" class="divider"></li>
                    <li><a href="{{url('/admin/logout')}}"><i class="material-icons">input</i>Sign Out</a></li>
                    <li role="seperator" class="divider"></li>
                </ul>
            </div>
        </div>
    </div>
    <!-- #User Info -->
    <!-- Menu -->
    <div class="menu">
        <ul class="list">
            <li class="header">MAIN NAVIGATION</li>
            <li class="active">
                <a href="{{url('admin/home')}}">
                    <i class="material-icons">home</i>
                    <span>Home</span>
                </a>
            </li>
           <!--  @can('view_users')
            <li><a href="{{'/admin/users'}}"><i class="material-icons">person_add</i><span>Users</span></a>
            @endcan
             @can('view_roles')
            <li><a href="{{'/admin/roles'}}"><i class="material-icons">book</i><span>Role</span></a></li>
            @endcan -->
            
            
             <li>
                <a href="javascript:void(0);" class="menu-toggle {{(request()->is('admin/hometransactions/*')  || request()->is('admin/homeoverviews/*') || request()->is('admin/footprints') || request()->is('admin/footprints/*') || request()->is('admin/keystrengths/*')  || request()->is('admin/keystrengths')  || request()->is('admin/homenews/*') || request()->is('admin/homenews') || request()->is('admin/homeawards') || request()->is('admin/homeawards/*') || request()->is('admin/homevideos/*') || request()->is('admin/homevideos') || request()->is('admin/homeassociates/*') || request()->is('admin/homeassociates'))? 'toggled' : '' }}">
                    
                    <span>Home Page</span>
                </a>
                <ul class="ml-menu">
                    <!-- <li>
                        <a href="{{url('admin/hometransactionheadings/1/edit')}}"><span>Edit Transaction Heading</span></a>
                    </li> -->
                    @can('view_hometransactions')
                     <li>
                        <a href="javascript:void(0);" class="menu-toggle {{(request()->is('admin/hometransactions/*') ) ? 'toggled' : '' }}"><span>Transaction Made Simple</span></a>

                        <ul class="ml-menu">
                             <li class="{{ (request()->is('admin/hometransactions/1/edit'))   ? 'active' : '' }}">
                                <a href="{{url('admin/hometransactions/1/edit')}}"><span>Edit Payment & Business Services</span></a>
                            </li>
                             <li class="{{ (request()->is('admin/hometransactions/2/edit'))   ? 'active' : '' }}">
                                <a href="{{url('admin/hometransactions/2/edit')}}"><span>Edit Merchant Services</span></a>
                            </li>
                             <li class="{{ (request()->is('admin/hometransactions/3/edit'))   ? 'active' : '' }}">
                                <a href="{{url('admin/hometransactions/3/edit')}}"><span>Edit Loyalty & VAS</span></a>
                            </li>
                             <li class="{{ (request()->is('admin/hometransactions/4/edit'))   ? 'active' : '' }}">
                                <a href="{{url('admin/hometransactions/4/edit')}}"><span>Edit Switching and issuance processing</span></a>
                            </li>
                        </ul>
                    </li>
                     @endcan
                     @can('view_homeoverviews')
                    <li class="{{ (request()->is('admin/homeoverviews/1/edit'))   ? 'active' : '' }}">
                        <a href="{{url('admin/homeoverviews/1/edit')}}"><span>Edit Overview</span></a>
                    </li>
                    @endcan
                     @can('view_footprints')
                    <li class="{{ (request()->is('admin/footprints/*') || request()->is('admin/footprints'))   ? 'active' : '' }}">
                        <a href="{{url('admin/footprints')}}"><span> Add Footprints</span></a>
                    </li>
                    @endcan
                    @can('view_keystrengths')
                     <li class="{{ (request()->is('admin/keystrengths/*') || request()->is('admin/keystrengths'))   ? 'active' : '' }}">
                        <a href="{{url('admin/keystrengths')}}"><span>Add Key Strength</span></a>
                    </li>
                    @endcan
                     @can('view_homenews')
                    <li class="{{ (request()->is('admin/homenews/*') || request()->is('admin/homenews'))   ? 'active' : '' }}">
                        <a href="{{url('admin/homenews')}}"><span>Add Home News</span></a>
                    </li>
                    @endcan
                     @can('view_homeawards')
                     <li class="{{ (request()->is('admin/homeawards/*') || request()->is('admin/homeawards'))   ? 'active' : '' }}">
                        <a href="{{url('admin/homeawards')}}"><span>Add Awads And Recognition</span></a>
                    </li>
                     @endcan
                     @can('view_homevideos')
                    <li class="{{ (request()->is('admin/homevideos/*') || request()->is('admin/homevideos'))   ? 'active' : '' }}">
                        <a href="{{url('admin/homevideos')}}"><span>Add Videos</span></a>
                    </li>
                     @endcan
                      @can('view_homeassociates')
                     <li class="{{ (request()->is('admin/homeassociates/*') || request()->is('admin/homeassociates'))   ? 'active' : '' }}">
                        <a href="{{url('admin/homeassociates')}}"><span>Add Associates & Certifications</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
              
              @can('view_aboutusdetails')
             <li>
                <a href="javascript:void(0);" class="menu-toggle {{(request()->is('admin/aboutusdetails/1/edit') || request()->is('admin/aboutusvisions/1/edit') || request()->is('admin/boardofdirectors') || request()->is('admin/boardofdirectors/*') || request()->is('admin/managementteams') || request()->is('admin/managementteams/*') ) ? 'toggled' : '' }}">
                    
                    <span>About Us</span>
                </a>
                <ul class="ml-menu">
                    
                    <li class="{{ (request()->is('admin/aboutusdetails/*') || request()->is('admin/aboutusdetails'))   ? 'active' : '' }}">
                        <a href="{{url('admin/aboutusdetails/1/edit')}}"><span> Edit About Us</span></a>
                    </li>
                     <li class="{{ (request()->is('admin/aboutusvisions/*') || request()->is('admin/aboutusvisions'))   ? 'active' : '' }}">
                        <a href="{{url('admin/aboutusvisions/1/edit')}}"><span>Edit Vision</span></a>
                    </li>
                    <li class="{{ (request()->is('admin/boardofdirectors/*') || request()->is('admin/boardofdirectors'))   ? 'active' : '' }}">
                        <a href="{{url('admin/boardofdirectors')}}"><span>Add Board Of Directors</span></a>
                    </li>
                     <li class="{{ (request()->is('admin/managementteams/*') || request()->is('admin/managementteams'))   ? 'active' : '' }}">
                        <a href="{{url('admin/managementteams')}}"><span>Add Management Team member</span></a>
                    </li>
                </ul>
            </li>
              @endcan
                 <li>
                <a href="javascript:void(0);" class="menu-toggle {{(request()->is('admin/pospayments/1/edit') || request()->is('admin/loyaltyservices/1/edit') || request()->is('admin/paymentsolutions') || request()->is('admin/paymentsolutions/*') || request()->is('admin/ongobusinesses/1/edit') || request()->is('admin/ongobillings/1/edit') || request()->is('admin/merchantservices') || request()->is('admin/merchantservices/*') ) ? 'toggled' : '' }}">
                    
                    <span>Products</span>
                </a>
                <ul class="ml-menu">
                    @can('view_pospayments')
                    <li class="{{ (request()->is('admin/pospayments/*') || request()->is('admin/pospayments'))   ? 'active' : '' }}">
                        <a href="{{url('admin/pospayments/1/edit')}}"><span> Edit Payments And POS</span></a>
                    </li>
                     @endcan
                     @can('view_paymentsolutions')
                     <li class="{{ (request()->is('admin/paymentsolutions/*') || request()->is('admin/paymentsolutions'))   ? 'active' : '' }}">
                        <a href="{{url('admin/paymentsolutions')}}"><span>Payments And POS Solutions</span></a>
                    </li>
                     @endcan
                      @can('view_loyaltyservices')
                    <li class="{{ (request()->is('admin/loyaltyservices/*') || request()->is('admin/loyaltyservices'))   ? 'active' : '' }}">
                        <a href="{{url('admin/loyaltyservices/1/edit')}}"><span>Edit Loyalty & Value Added Services</span></a>
                    </li>
                     @endcan
                      @can('view_ongobusinesses')
                         <li class="{{ (request()->is('admin/ongobusinesses/*') || request()->is('admin/ongobusinesses'))   ? 'active' : '' }}">
                        <a href="{{url('admin/ongobusinesses/1/edit')}}"><span>Edit Ongo Business POS</span></a>
                        </li>
                        @endcan
                         @can('view_ongobillings')
                     <li class="{{ (request()->is('admin/ongobillings/*') || request()->is('admin/ongobillings'))   ? 'active' : '' }}">
                        <a href="{{url('admin/ongobillings/1/edit')}}"><span>Edit Ongo Billing ++</span></a>
                    </li>
                      @endcan
                      @can('view_merchantservices')
                    <li class="{{ (request()->is('admin/merchantservices/*') || request()->is('admin/merchantservices'))   ? 'active' : '' }}">
                        <a href="{{url('admin/merchantservices')}}"><span>Add Merchant Services</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
              @can('view_services')
            <li  class="{{ (request()->is('admin/services/*') || request()->is('admin/services'))   ? 'active' : '' }}" ><a href="{{'/admin/services'}}"><span>services</span></a></li>
            @endcan
            @can('view_businessverticals')
             <li>
                <a href="javascript:void(0);" class="menu-toggle {{(request()->is('admin/businessverticals') || request()->is('admin/businessverticals/*') ) ? 'toggled' : '' }}">
                    
                    <span>Business Verticals</span>
                </a>
                <ul class="ml-menu">
                    
                    <!-- <li>
                        <a href="{{url('admin/businessheadings')}}"><span> Add Business Heading</span></a>
                    </li>
                     <li>
                        <a href="{{url('admin/businessverticals')}}"><span>Add Business Heading Data</span></a>
                    </li> -->
                    <li class="{{ (request()->is('admin/businessverticals/1/edit'))   ? 'active' : '' }}">
                        <a href="{{url('admin/businessverticals/1/edit')}}"><span> Edit Hospitality & Entertainment</span></a>
                    </li>
                    <li class="{{ (request()->is('admin/businessverticals/2/edit'))   ? 'active' : '' }}">
                        <a href="{{url('admin/businessverticals/2/edit')}}"><span> Edit Educational Institutes</span></a>
                    </li>
                     <li class="{{ (request()->is('admin/businessverticals/3/edit'))   ? 'active' : '' }}">
                        <a href="{{url('admin/businessverticals/3/edit')}}"><span> Edit Consumer Durables</span></a>
                    </li>
                    <li class="{{ (request()->is('admin/businessverticals/4/edit'))   ? 'active' : '' }}">
                        <a href="{{url('admin/businessverticals/4/edit')}}"><span> Edit Retail Stores</span></a>
                    </li>
                    <li class="{{ (request()->is('admin/businessverticals/5/edit'))   ? 'active' : '' }}">
                        <a href="{{url('admin/businessverticals/5/edit')}}"><span> Edit E-commerce</span></a>
                    </li>
                     <li class="{{ (request()->is('admin/businessverticals/6/edit'))   ? 'active' : '' }}">
                        <a href="{{url('admin/businessverticals/6/edit')}}"><span> Edit Health & Beauty</span></a>
                    </li>
                    <li class="{{ (request()->is('admin/businessverticals/7/edit'))   ? 'active' : '' }}">
                        <a href="{{url('admin/businessverticals/7/edit')}}"><span> Edit Lifestyle & Fashion</span></a>
                    </li>
                </ul>
            </li>
              @endcan
              
             <li>
                <a href="javascript:void(0);" class="menu-toggle {{(request()->is('admin/clienttestomonials') || request()->is('admin/clienttestomonials/*') || request()->is('admin/clients') || request()->is('admin/clients/*') ) ? 'toggled' : '' }}">
                    
                    <span>Clientele</span>
                </a>
                <ul class="ml-menu">
                     @can('view_clienttestomonials')
                    <li class="{{ (request()->is('admin/clienttestomonials/*') || request()->is('admin/clienttestomonials') )   ? 'active' : '' }}">
                        <a href="{{url('admin/clienttestomonials')}}"><span> Client Testomonials</span></a>
                    </li>
                    @endcan
                      @can('view_clients')
                     <li class="{{ (request()->is('admin/clients/*') || request()->is('admin/clients') )   ? 'active' : '' }}">
                        <a href="{{url('admin/clients')}}"><span>Clients</span></a>
                    </li>
                    @endcan
                </ul>
            </li>
              
               @can('view_awards')
             <li>
                <a href="javascript:void(0);" class="menu-toggle {{(request()->is('admin/awardyears') || request()->is('admin/awardyears/*') || request()->is('admin/awards') || request()->is('admin/awards/*') ) ? 'toggled' : '' }}">
                    
                    <span>Awards & Recognition</span>
                </a>
                <ul class="ml-menu">
                    
                    <li class="{{ (request()->is('admin/awardyears/*') || request()->is('admin/awardyears') )   ? 'active' : '' }}">
                        <a href="{{url('admin/awardyears')}}"><span> Award Year</span></a>
                    </li>
                     <li class="{{ (request()->is('admin/awards/*') || request()->is('admin/awards') )   ? 'active' : '' }}">
                        <a href="{{url('admin/awards')}}"><span>Awards</span></a>
                    </li>
                </ul>
            </li>
              @endcan
             @can('view_news')
            <li class="{{ (request()->is('admin/news/*') || request()->is('admin/news') )   ? 'active' : '' }}"><a href="{{'/admin/news'}}"><span>Media & News</span></a></li>
            @endcan
             @can('view_reachuses')
            <li class="{{ (request()->is('admin/reachuses/*') || request()->is('admin/reachuses') )   ? 'active' : '' }}"><a href="{{'/admin/reachuses/1/edit'}}"><span>Edit Contact Us</span></a></li>
            @endcan
            @can('view_privacypolicies')
            <li class="{{ (request()->is('admin/privacypolicies/*') || request()->is('admin/privacypolicies') )   ? 'active' : '' }}"><a href="{{'/admin/privacypolicies/1/edit'}}"><span>Edit Privacy Policy</span></a></li>
            @endcan
             @can('view_investorrelations')
            <li class="{{ (request()->is('admin/investorrelations/*') || request()->is('admin/investorrelations') )   ? 'active' : '' }}"><a href="{{'/admin/investorrelations/1/edit'}}"><span>Edit Investor Relation</span></a></li>
            @endcan
            @can('view_disclaimerpolicies')
            <li class="{{ (request()->is('admin/disclaimerpolicies/*') || request()->is('admin/disclaimerpolicies') )   ? 'active' : '' }}"><a href="{{'/admin/disclaimerpolicies/1/edit'}}"><span>Edit Disclaimer Policy</span></a></li>
            @endcan
           
             <li>
                <a href="javascript:void(0);" class="menu-toggle {{(request()->is('admin/collectioncentreaddresses') || request()->is('admin/collectioncentreaddresses/*') || request()->is('/admin/ewastepolicies/1/edit') ) ? 'toggled' : '' }}">
                    
                    <span>E-waste Management Policy</span>
                </a>
                <ul class="ml-menu">
                      @can('view_collectioncentreaddresses')
                    <li class="{{ (request()->is('admin/collectioncentreaddresses/*') || request()->is('admin/collectioncentreaddresses') )   ? 'active' : '' }}">
                        <a href="{{url('admin/collectioncentreaddresses')}}"><span>Collection Centre Address</span></a>
                    </li>
                     @endcan
                   @can('view_ewastepolicies')
                     <li class="{{ (request()->is('admin/ewastepolicies/1/edit') )   ? 'active' : '' }}"><a href="{{'/admin/ewastepolicies/1/edit'}}"><span>Edit E-waste Management Policy</span></a></li>
                    @endcan
                     
                </ul>
            </li>
             @can('view_financials')
            <li class="{{ (request()->is('admin/financials/*') || request()->is('admin/financials') )   ? 'active' : '' }}"><a href="{{'/admin/financials'}}"><span>Add Financials</span></a></li>
            @endcan
            
              @can('view_footerlinks')
            <li  class="{{ (request()->is('admin/footerlinks/1/edit') )   ? 'active' : '' }}"><a href="{{'/admin/footerlinks/1/edit'}}"><span>Edit Footer links</span></a></li>
            @endcan
            @can('view_websitedetails')
            <li class="{{ (request()->is('admin/websitedetails/1/edit') )   ? 'active' : '' }}"><a href="{{'/admin/websitedetails/1/edit'}}"><span>Edit Website Details</span></a></li>
            @endcan
              <li>
                <a href="javascript:void(0);" class="menu-toggle {{(request()->is('admin/enquiryformproducts') || request()->is('admin/enquiryformproducts/*')  || request()->is('/admin/enquiryformleads')  || request()->is('/admin/careerformleads')) ? 'toggled' : '' }}">
                    
                    <span>Enquiry And Carrer Form</span>
                </a>
                <ul class="ml-menu">
                     @can('view_enquiryformproducts')
                    <li class="{{ (request()->is('admin/enquiryformproducts/*') || request()->is('admin/enquiryformproducts') )   ? 'active' : '' }}">
                        <a href="{{url('admin/enquiryformproducts')}}"><span> Add Enquiry Form products</span></a>
                    </li>
                     @endcan
                     <li class="{{ (request()->is('admin/enquiryformleads/*') || request()->is('admin/enquiryformleads') )   ? 'active' : '' }}">
                        <a href="{{url('admin/enquiryformleads')}}"><span>Enquiry Form Leads</span></a>
                    </li>
                    <li class="{{ (request()->is('admin/careerformleads/*') || request()->is('admin/careerformleads') )   ? 'active' : '' }}">
                        <a href="{{url('admin/careerformleads')}}"><span>Career Form Leads</span></a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <!-- #Menu -->
    <!-- Footer -->
    <div class="legal">
        <div class="copyright">
            &copy; {{date('Y')}} <a href="javascript:void(0);">First Economy</a>
        </div>
    </div>
    <!-- #Footer -->
</aside>