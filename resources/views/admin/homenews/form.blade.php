 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($homenews->title) ? $homenews->title : ''}}" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" required>{{ isset($homenews->description) ? $homenews->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="main_image" style="font-size:18px;">{{ 'Main Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="main_image" type="file" id="main_image" value="{{ isset($homenews->main_image) ? $homenews->main_image : ''}}" >
     {!! $errors->first('main_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($homenews->main_image))
      <img src="{{URL::asset($homenews->main_image)}}" height="100" width="100">
      @endif
 </div>
  <br>
 <label for="icon_image" style="font-size:18px;">{{ 'Icon Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="icon_image" type="file" id="icon_image" value="{{ isset($homenews->icon_image) ? $homenews->icon_image : ''}}" >
     {!! $errors->first('icon_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($homenews->icon_image))
      <img src="{{URL::asset($homenews->icon_image)}}" height="100" width="100">
      @endif
 </div>
  <br>
 <label for="news_file" style="font-size:18px;">{{ 'News File' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="news_file" type="file" id="news_file" value="{{ isset($homenews->news_file) ? $homenews->news_file : ''}}" >
     {!! $errors->first('news_file', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($homenews->news_file))
      <a href="{{ url($homenews->news_file) }}" target="_blank"><i class="material-icons">file_copy</i></a>
      @endif
 </div>
  <br>
 <label for="location" style="font-size:18px;">{{ 'Location' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="location" type="text" id="location" value="{{ isset($homenews->location) ? $homenews->location : ''}}" required>
     {!! $errors->first('location', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="date" style="font-size:18px;">{{ 'Date' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="date" type="date" id="date" value="{{ isset($homenews->date) ? $homenews->date : ''}}" >
     {!! $errors->first('date', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($homenews->status) && $homenews->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
