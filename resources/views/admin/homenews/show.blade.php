@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Home Page News  {{ $homenews->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/homenews') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_homenews', 'delete_homenews')
                         <a href="{{ url('/admin/homenews/' . $homenews->id . '/edit') }}" title="Edit Homenews"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/homenews' . '/' . $homenews->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Homenews" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $homenews->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $homenews->title }} </td></tr>
                                    <tr><th> Description </th><td> {!! $homenews->description !!} </td></tr>
                                    <tr><th> Main Image </th><td><img src="{{ URL::asset($homenews->main_image) }}" height="200" width="200">  </td></tr>
                                    <tr><th> Icon Image </th><td><img src="{{ URL::asset($homenews->icon_image) }}" height="200" width="200"> </td></tr>
                                    <tr><th> News File </th><td> <a href="{{ url($homenews->news_file) }}" target="_blank"><i class="material-icons">file_copy</i></a></td></tr>
                                    <tr><th> Status </th><td>@if($homenews->status==1) {{ 'Active' }}@else{{'Deactive'}}@endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
