@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Financial  {{ $financial->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/financials') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_financials', 'delete_financials')
                         <a href="{{ url('/admin/financials/' . $financial->id . '/edit') }}" title="Edit Financial"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/financials' . '/' . $financial->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Financial" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $financial->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $financial->title }} </td></tr><tr><th> File </th><td>   <a href="{{URL::asset($financial->file)}}" target="_blank" download><i class="material-icons">file_copy</i></a> </td></tr><tr><th> Status </th><td> @if( $financial->status==1){{ 'Active'}}@else {{'Deactive'}}@endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
