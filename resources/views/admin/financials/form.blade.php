 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($financial->title) ? $financial->title : ''}}" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="file" style="font-size:18px;">{{ 'File' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="file" type="file" id="file" value="{{ isset($financial->file) ? $financial->file : ''}}" >
     {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($financial->file))
      <a href="{{URL::asset($financial->file)}}" target="_blank" download><i class="material-icons">file_copy</i></a>
      @endif
 </div>
  <br>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($financial->status) && $financial->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
