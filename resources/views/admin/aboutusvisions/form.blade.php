 <label for="title1" style="font-size:18px;">{{ 'Title1' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title1" type="text" id="title1" value="{{ isset($aboutusvision->title1) ? $aboutusvision->title1 : ''}}" required>
     {!! $errors->first('title1', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="description1" style="font-size:18px;">{{ 'Description1' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description1" type="textarea" id="description1" >{{ isset($aboutusvision->description1) ? $aboutusvision->description1 : ''}}</textarea>
     {!! $errors->first('description1', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="title2" style="font-size:18px;">{{ 'Title2' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title2" type="text" id="title2" value="{{ isset($aboutusvision->title2) ? $aboutusvision->title2 : ''}}" required>
     {!! $errors->first('title2', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="description2" style="font-size:18px;">{{ 'Description2' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description2" type="textarea" id="description2" >{{ isset($aboutusvision->description2) ? $aboutusvision->description2 : ''}}</textarea>
     {!! $errors->first('description2', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="image" style="font-size:18px;">{{ 'Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($aboutusvision->image) ? $aboutusvision->image : ''}}">
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($aboutusvision->image)) <img src="{{URL::asset($aboutusvision->image)}}" height="100" width="100">@endif
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
