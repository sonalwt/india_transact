@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            About  Us Vision
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/aboutusvisions/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Aboutusvision" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/aboutusvisions', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Title1</th><th>Description1</th><th>Title2</th><th>Description2</th><th>Image</th>
                                        @can('view_aboutusvisions','edit_aboutusvisions', 'delete_aboutusvisions')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($aboutusvisions as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->title1 }}</td><td>{!! $item->description1 !!}</td><td>{{ $item->title2 }}</td><td>{!! $item->description2 !!}</td><td><img src="{{URL::asset($item->image)}}" height="100" width="100"></td>
                                        @can('view_aboutusvisions')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/aboutusvisions/' . $item->id) }}" title="View Aboutusvision"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'aboutusvisions',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $aboutusvisions->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
