@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            About  Us Vision  {{ $aboutusvision->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/aboutusvisions') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_aboutusvisions', 'delete_aboutusvisions')
                         <a href="{{ url('/admin/aboutusvisions/' . $aboutusvision->id . '/edit') }}" title="Edit Aboutusvision"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/aboutusvisions' . '/' . $aboutusvision->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Aboutusvision" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $aboutusvision->id }}</td>
                                    </tr>
                                    <tr><th> Title1 </th><td> {{ $aboutusvision->title1 }} </td></tr><tr><th> Description1 </th><td> {{ $aboutusvision->description1 }} </td></tr><tr><th> Title2 </th><td> {{ $aboutusvision->title2 }} </td></tr><tr><th> Description2 </th><td> {{ $aboutusvision->description2 }} </td></tr><tr><th> Image </th><td> <img src="{{URL::asset($aboutusvision->image)}}" height="100" width="100"> </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
