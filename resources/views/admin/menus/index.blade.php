@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Menus
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/menus/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Menu" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/menus', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Menu</th>
                                        <th>Menu Link</th>
                                        <th>Meta Title</th>
                                        <th>Meta Keyword</th>
                                        <th>Meta Description</th>
                                        <th>Menu Sort Order</th>
                                        <th>Menu Status</th>
                                        @can('view_menus','edit_menus', 'delete_menus')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($menus as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->menu }}</td>
                                        <td>{{ $item->menu_link }}</td>
                                        <td>{{ $item->meta_title }}</td>
                                        <td>{{ $item->meta_keyword }}</td>
                                        <td>{{ $item->meta_description }}</td>
                                        <td>{{ $item->menu_sort_order }}</td>
                                        <td>@if($item->menu_status=='1'){{  'Active'}}@else{{'Deactive'}}@endif</td>
                                        @can('view_menus')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/menus/' . $item->id) }}" title="View Menu"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'menus',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $menus->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
