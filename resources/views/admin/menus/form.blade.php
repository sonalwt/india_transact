 <label for="menu" style="font-size:18px;">{{ 'Menu' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="menu" type="text" id="menu" value="{{ isset($menu->menu) ? $menu->menu : ''}}" required>
     {!! $errors->first('menu', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="meta_title" style="font-size:18px;">{{ 'Meta Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="meta_title" type="text" id="meta_title" value="{{ isset($menu->meta_title) ? $menu->meta_title : ''}}" >
     {!! $errors->first('meta_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="menu_link" style="font-size:18px;">{{ 'Menu Link' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="menu_link" type="text" id="menu_link" value="{{ isset($menu->menu_link) ? $menu->menu_link : ''}}" >
     {!! $errors->first('menu_link', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="meta_keyword" style="font-size:18px;">{{ 'Meta Keyword' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="meta_keyword" type="text" id="meta_keyword" value="{{ isset($menu->meta_keyword) ? $menu->meta_keyword : ''}}" >
     {!! $errors->first('meta_keyword', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="meta_description" style="font-size:18px;">{{ 'Meta Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="meta_description" type="textarea" id="meta_description" >{{ isset($menu->meta_description) ? $menu->meta_description : ''}}</textarea>
     {!! $errors->first('meta_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="menu_sort_order" style="font-size:18px;">{{ 'Menu Sort Order' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="menu_sort_order" type="text" id="menu_sort_order" value="{{ isset($menu->menu_sort_order) ? $menu->menu_sort_order : ''}}" required>
     {!! $errors->first('menu_sort_order', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="menu_status" style="font-size:18px;">{{ 'Menu Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="menu_status" class="form-control" id="menu_status" >
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($menu->menu_status) && $menu->menu_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('menu_status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
