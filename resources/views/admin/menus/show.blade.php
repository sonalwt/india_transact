@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Menu  {{ $menu->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/menus') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_menus', 'delete_menus')
                         <a href="{{ url('/admin/menus/' . $menu->id . '/edit') }}" title="Edit Menu"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/menus' . '/' . $menu->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Menu" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $menu->id }}</td>
                                    </tr>
                                    <tr><th> Menu </th><td> {{ $menu->menu }} </td></tr>
                                    <tr><th> Meta Title </th><td> {{ $menu->meta_title }} </td></tr><tr><th> Menu Link </th><td> {{ $menu->menu_link }} </td></tr>
                                    <tr><th> Meta Keyword </th><td> {{ $menu->meta_keyword }} </td></tr>
                                    <tr><th> Meta Description </th><td> {{ $menu->meta_description }} </td></tr>
                                    <tr><th> Menu Sort Order </th><td> {{ $menu->menu_sort_order }} </td></tr>
                                    <tr><th> Menu Status </th><td> @if($menu->menu_status=='1'){{  'Active'}}@else{{'Deactive'}}@endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
