@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Home Page Video  {{ $homevideo->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/homevideos') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_homevideos', 'delete_homevideos')
                         <a href="{{ url('/admin/homevideos/' . $homevideo->id . '/edit') }}" title="Edit Homevideo"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/homevideos' . '/' . $homevideo->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Homevideo" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $homevideo->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $homevideo->title }} </td></tr><tr><th> Youtube Link </th><td> <iframe width="" height="200" src="{{ $homevideo->youtube_link }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen class="align-center"></iframe></td></tr><tr><th> Image </th><td> <img src="{{ URL::asset($homevideo->image) }}" height="200" width="200"> </td></tr><tr><th> Status </th><td> @if($homevideo->status==1){{  'Active'}} @else {{'Deactive'}}@endif</td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
