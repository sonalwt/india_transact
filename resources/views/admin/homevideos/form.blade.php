 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($homevideo->title) ? $homevideo->title : ''}}" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="youtube_link" style="font-size:18px;">{{ 'Youtube Link' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="youtube_link" type="text" id="youtube_link" value="{{ isset($homevideo->youtube_link) ? $homevideo->youtube_link : ''}}" >
     {!! $errors->first('youtube_link', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="image" style="font-size:18px;">{{ 'Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($homevideo->image) ? $homevideo->image : ''}}">
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($homevideo->image))<ing src="{{URL::asset($homevideo->image)}}" height="200" width="200">@endif
 </div>
 <br>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($homevideo->status) && $homevideo->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
