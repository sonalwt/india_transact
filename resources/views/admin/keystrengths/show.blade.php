@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Key Strength  {{ $keystrength->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/keystrengths') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_keystrengths', 'delete_keystrengths')
                         <a href="{{ url('/admin/keystrengths/' . $keystrength->id . '/edit') }}" title="Edit Keystrength"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/keystrengths' . '/' . $keystrength->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Keystrength" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $keystrength->id }}</td>
                                    </tr>
                                    <tr><th> Caption </th><td> {{ $keystrength->caption }} </td></tr><tr><th> Image </th><td> <img src="{{ $keystrength->image }}" height="50" width="50"> </td></tr><tr><th> After Hover Image </th><td> <img src="{{ $keystrength->after_hover_image }}" height="50" width="50"> </td></tr><tr><th> Status </th><td> @if($keystrength->status==1){{'Active'}}@else{{'Deactive'}}@endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
