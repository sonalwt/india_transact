@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Key Strengths
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/keystrengths/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Keystrength" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/keystrengths', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Caption</th><th>Image</th><th>After Hover Image</th><th>Status</th>
                                        @can('view_keystrengths','edit_keystrengths', 'delete_keystrengths')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($keystrengths as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->caption }}</td><td><img src="{{ $item->image }}" height="50" width="50"></td><td><img src="{{ $item->after_hover_image }}" height="50" width="50"></td><td>@if($item->status==1){{'Active'}}@else{{'Deactive'}}@endif</td>
                                        @can('view_keystrengths')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/keystrengths/' . $item->id) }}" title="View Keystrength"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'keystrengths',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $keystrengths->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
