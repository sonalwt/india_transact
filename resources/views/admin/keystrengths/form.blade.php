 <label for="caption" style="font-size:18px;">{{ 'Caption' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="caption" type="text" id="caption" value="{{ isset($keystrength->caption) ? $keystrength->caption : ''}}" required>
     {!! $errors->first('caption', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="image" style="font-size:18px;">{{ 'Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($keystrength->image) ? $keystrength->image : ''}}" >
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($keystrength->image))<img src="{{ $keystrength->image }}" height="50" width="50">@endif
 </div>
 <br>
 <label for="after_hover_image" style="font-size:18px;">{{ 'After Hover Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="after_hover_image" type="file" id="after_hover_image" value="{{ isset($keystrength->after_hover_image) ? $keystrength->after_hover_image : ''}}" >
     {!! $errors->first('after_hover_image', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($keystrength->after_hover_image))<img src="{{ $keystrength->after_hover_image }}" height="50" width="50">@endif
 </div>
 <br>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($keystrength->status) && $keystrength->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
