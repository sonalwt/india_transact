@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Business Vertical  {{ $businessvertical->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/businessverticals') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_businessverticals', 'delete_businessverticals')
                         <a href="{{ url('/admin/businessverticals/' . $businessvertical->id . '/edit') }}" title="Edit Businessvertical"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/businessverticals' . '/' . $businessvertical->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Businessvertical" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $businessvertical->id }}</td>
                                    </tr>
                                    <tr><th> Business Title </th><td> {{ $businessvertical->business_title }} </td></tr>
                                    <tr><th> Business Heading Id </th><td> {{ $businessvertical->businessheadings->business_heading }} </td></tr>
                                    <tr><th> Business Description </th><td> {!! $businessvertical->business_description !!} </td></tr>
                                    <tr><th> Business Icon </th><td> <img src="{{ URL::asset($businessvertical->business_icon) }}" alt="{{ $businessvertical->business_title }}" height="100" width="100"> </td></tr>
                                    <tr><th> Business Icon Selected </th><td><img src="{{ URL::asset( $businessvertical->business_icon_selected ) }}" alt="{{ $businessvertical->business_title }}" height="100" width="100" style="background-color: #7fbb30;"> </td></tr>
                                    <tr><th> Business Image </th><td><img src="{{ URL::asset($businessvertical->business_image) }}" alt="{{ $businessvertical->business_title }}" height="100" width="100"></td></tr>
                                    <tr><th> Business Sort Order </th><td> {{ $businessvertical->business_sort_order }} </td></tr>
                                    <tr><th> Business Status </th><td> @if($businessvertical->business_status==1){{'Active'}}@else {{'Deactive'}}@endif</td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
