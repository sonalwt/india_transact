@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Business Vertical
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/businessverticals/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Businessvertical" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/businessverticals', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Business Title</th>
                                        <th>Business Heading Id</th>
                                        <th>Business Description</th>
                                        <th>Business Icon</th>
                                        <th>Business Icon Selected</th>
                                        <th>Business Image</th>
                                         <th>Business Sort Order</th>
                                          <th>Business Status</th>
                                        @can('view_businessverticals','edit_businessverticals', 'delete_businessverticals')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($businessverticals as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->business_title }}</td>
                                        <td>{{ $item->businessheadings->business_heading}}</td>
                                        <td>{!! $item->business_description !!}</td>
                                        <td><img src="{{ URL::asset($item->business_icon) }}" alt="{{ $item->business_title }}" height="100" width="100"></td>
                                        <td><img src="{{ URL::asset($item->business_icon_selected) }}" alt="{{ $item->business_title }}" height="100" width="100" style="background-color: #7fbb30;"></td>
                                        <td><img src="{{ URL::asset($item->business_image) }}" alt="{{ $item->business_title }}" height="100" width="100"></td>
                                        <td>{{ $item->business_sort_order }}</td>
                                        <td>@if($item->business_status==1) {{'Active'}}@else {{'Deactive'}}@endif</td>
                                        @can('view_businessverticals')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/businessverticals/' . $item->id) }}" title="View Businessvertical"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'businessverticals',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $businessverticals->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
