 <label for="business_title" style="font-size:18px;">{{ 'Business Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="business_title" type="text" id="business_title" value="{{ isset($businessvertical->business_title) ? $businessvertical->business_title : ''}}" required>
     {!! $errors->first('business_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="business_heading_id" style="font-size:18px;">{{ 'Select Business Heading' }}</label>
 <div class="form-group">
      <div class="form-line">
       <select name="business_heading_id" class="form-control" id="business_heading_id" required>
    @foreach ($businessheadings as $headings)
        <option value="{{ $headings->id }}" {{ (isset($businessvertical->business_heading_id) && $businessvertical->business_heading_id == $headings->id) ? 'selected' : ''}}>{{ $headings->business_heading }}</option>
    @endforeach
</select>
     {!! $errors->first('business_heading_id', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="business_description" style="font-size:18px;">{{ 'Business Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="business_description" type="textarea" id="business_description" required>{{ isset($businessvertical->business_description) ? $businessvertical->business_description : ''}}</textarea>
     {!! $errors->first('business_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="business_icon" style="font-size:18px;">{{ 'Business Icon' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="business_icon" type="file" id="business_icon" value="{{ isset($businessvertical->business_icon) ? $businessvertical->business_icon : ''}}">
     {!! $errors->first('business_icon', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($businessvertical->business_icon))
      <img src="{{ URL::asset($businessvertical->business_icon) }}" alt="{{ $businessvertical->business_title }}" height="100" width="100">
      @endif
 </div>
  <br>
 <label for="business_icon_selected" style="font-size:18px;">{{ 'Business Icon Selected' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="business_icon_selected" type="file" id="business_icon_selected" value="{{ isset($businessvertical->business_icon_selected) ? $businessvertical->business_icon_selected : ''}}">
     {!! $errors->first('business_icon_selected', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($businessvertical->business_icon_selected))
      <img src="{{ URL::asset($businessvertical->business_icon_selected) }}" alt="{{ $businessvertical->business_title }}" height="100" width="100" style="background-color: #7fbb30;">
      @endif
 </div>
  <br>
 <label for="business_image" style="font-size:18px;">{{ 'Business Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="business_image" type="file" id="business_image" value="{{ isset($businessvertical->business_image) ? $businessvertical->business_image : ''}}" >
     {!! $errors->first('business_image', '<p class="help-block">:message</p>') !!}
      </div>
       @if(isset($businessvertical->business_image))
      <img src="{{ URL::asset($businessvertical->business_image) }}" alt="{{ $businessvertical->business_title }}" height="100" width="100">
      @endif
 </div>
  <br>
 <label for="business_sort_order" style="font-size:18px;">{{ 'Business Sort Order' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="business_sort_order" type="text" id="business_sort_order" value="{{ isset($businessvertical->business_sort_order) ? $businessvertical->business_sort_order : ''}}" required>
     {!! $errors->first('business_sort_order', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="business_status" style="font-size:18px;">{{ 'Business Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="business_status" class="form-control" id="business_status" required>
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($businessvertical->business_status) && $businessvertical->business_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('business_status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
