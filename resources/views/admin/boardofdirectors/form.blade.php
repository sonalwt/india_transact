 <label for="name" style="font-size:18px;">{{ 'Name' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="name" type="text" id="name" value="{{ isset($boardofdirector->name) ? $boardofdirector->name : ''}}" required>
     {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="designation" style="font-size:18px;">{{ 'Designation' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="designation" type="text" id="designation" value="{{ isset($boardofdirector->designation) ? $boardofdirector->designation : ''}}" required>
     {!! $errors->first('designation', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="image" style="font-size:18px;">{{ 'Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($boardofdirector->image) ? $boardofdirector->image : ''}}" >
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($boardofdirector->image))
      <img src="{{URL::asset($boardofdirector->image)}}" height="100" width="100">
      @endif
 </div>
  <br>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" >{{ isset($boardofdirector->description) ? $boardofdirector->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" >
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($boardofdirector->status) && $boardofdirector->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
