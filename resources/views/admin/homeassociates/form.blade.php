 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title" type="text" id="title" value="{{ isset($homeassociate->title) ? $homeassociate->title : ''}}" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="image" style="font-size:18px;">{{ 'Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="image" type="file" id="image" value="{{ isset($homeassociate->image) ? $homeassociate->image : ''}}">
     {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($homeassociate->image)) <img src="{{URL::asset($homeassociate->image)}}" height="200" width="200">@endif
 </div>
 <br>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($homeassociate->status) && $homeassociate->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
