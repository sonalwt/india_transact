 <label for="map" style="font-size:18px;">{{ 'Map' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="map" type="text" id="map" value="{{ isset($reachus->map) ? $reachus->map : ''}}" required>
     {!! $errors->first('map', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="registered_office" style="font-size:18px;">{{ 'Registered Office' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="registered_office" type="textarea" id="registered_office" required>{{ isset($reachus->registered_office) ? $reachus->registered_office : ''}}</textarea>
     {!! $errors->first('registered_office', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="office_address" style="font-size:18px;">{{ 'Office Address' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="office_address" type="textarea" id="office_address" required>{{ isset($reachus->office_address) ? $reachus->office_address : ''}}</textarea>
     {!! $errors->first('office_address', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="for_business_related_queries" style="font-size:18px;">{{ 'For Business Related Queries' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="for_business_related_queries" type="text" id="for_business_related_queries" value="{{ isset($reachus->for_business_related_queries) ? $reachus->for_business_related_queries : ''}}" required>
     {!! $errors->first('for_business_related_queries', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="for_media_related_queries" style="font-size:18px;">{{ 'For Media Related Queries' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="for_media_related_queries" type="text" id="for_media_related_queries" value="{{ isset($reachus->for_media_related_queries) ? $reachus->for_media_related_queries : ''}}" required>
     {!! $errors->first('for_media_related_queries', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="health_and_support" style="font-size:18px;">{{ 'Health And Support' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="health_and_support" type="text" id="health_and_support" value="{{ isset($reachus->health_and_support) ? $reachus->health_and_support : ''}}" required>
     {!! $errors->first('health_and_support', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <label for="work_with_us" style="font-size:18px;">{{ 'Work With Us' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="work_with_us" type="text" id="work_with_us" value="{{ isset($reachus->work_with_us) ? $reachus->work_with_us : ''}}" required>
     {!! $errors->first('work_with_us', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
