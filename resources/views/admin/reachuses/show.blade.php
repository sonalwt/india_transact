@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Reachus  {{ $reachus->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/reachuses') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_reachuses', 'delete_reachuses')
                         <a href="{{ url('/admin/reachuses/' . $reachus->id . '/edit') }}" title="Edit Reachus"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/reachuses' . '/' . $reachus->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Reachus" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $reachus->id }}</td>
                                    </tr>
                                    <tr><th> Map </th><td> {{ $reachus->map }} </td></tr><tr><th> Registered Office </th><td> {{ $reachus->registered_office }} </td></tr><tr><th> Office Address </th><td> {{ $reachus->office_address }} </td></tr><tr><th> For Business Related Queries </th><td> {{ $reachus->for_business_related_queries }} </td></tr><tr><th> For Media Related Queries </th><td> {{ $reachus->for_media_related_queries }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
