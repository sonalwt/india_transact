@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Reachuses
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/reachuses/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Reachus" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/reachuses', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Map</th><th>Registered Office</th><th>Office Address</th><th>For Business Related Queries</th><th>For Media Related Queries</th>
                                        @can('view_reachuses','edit_reachuses', 'delete_reachuses')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($reachuses as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{!! $item->map !!}</td><td>{!! $item->registered_office !!}</td><td>{!! $item->office_address !!}</td><td>{{ $item->for_business_related_queries }}</td><td>{{ $item->for_media_related_queries }}</td>
                                        @can('view_reachuses')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/reachuses/' . $item->id) }}" title="View Reachus"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'reachuses',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $reachuses->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
