 <label for="title" style="font-size:18px;">{{ 'Title' }}</label>

        <input class="form-control" name="title" type="text" id="title" value="{{ isset($ewastepolicy->title) ? $ewastepolicy->title : ''}}" required>
     {!! $errors->first('title', '<p class="help-block">:message</p>') !!}

 <br>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>

        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" required>{{ isset($ewastepolicy->description) ? $ewastepolicy->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
 

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
