@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            payment solution  {{ $paymentsolution->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/paymentsolutions') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_paymentsolutions', 'delete_paymentsolutions')
                         <a href="{{ url('/admin/paymentsolutions/' . $paymentsolution->id . '/edit') }}" title="Edit paymentsolution"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/paymentsolutions' . '/' . $paymentsolution->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete paymentsolution" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $paymentsolution->id }}</td>
                                    </tr>
                                    <tr><th> Title1 </th><td> {{ $paymentsolution->title1 }} </td></tr>
                                    <tr><th> Title2 </th><td> {{ $paymentsolution->title2 }} </td></tr>
                                    <tr><th> Icon Image </th><td> <img src="{{ URL::asset($paymentsolution->icon_image) }}" height="100" width="100" alt=""> </td></tr>
                                    <tr><th> Main Image </th><td> <img src="{{ URL::asset($paymentsolution->main_image) }}" height="100" width="100" alt=""> </td></tr>
                                    <tr><th> Description </th><td> {!! $paymentsolution->description !!} </td></tr>
                                    <tr><th> Status </th><td> @if($paymentsolution->status==1){{'Active'}}@else{{'Deactive'}} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
