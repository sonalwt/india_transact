@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Payment solutions
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/paymentsolutions/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New paymentsolution" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/paymentsolutions', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Title1</th><th>Title2</th><th>Icon Image</th><th>Main Image</th><th>Description</th>
                                        @can('view_paymentsolutions','edit_paymentsolutions', 'delete_paymentsolutions')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($paymentsolutions as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->title1 }}</td>
                                        <td>{{ $item->title2 }}</td>
                                        <td><img src="{{ URL::asset($item->icon_image) }}" height="100" width="100" alt=""></td>
                                        <td><img src="{{ URL::asset($item->main_image) }}" height="100" width="100" alt=""></td><td width="400">{!! $item->description !!}</td>
                                        @can('view_paymentsolutions')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/paymentsolutions/' . $item->id) }}" title="View paymentsolution"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'paymentsolutions',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $paymentsolutions->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
