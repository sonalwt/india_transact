 <label for="title1" style="font-size:18px;">{{ 'Title1' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title1" type="text" id="title1" value="{{ isset($paymentsolution->title1) ? $paymentsolution->title1 : ''}}" required>
     {!! $errors->first('title1', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="title2" style="font-size:18px;">{{ 'Title2' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="title2" type="text" id="title2" value="{{ isset($paymentsolution->title2) ? $paymentsolution->title2 : ''}}" required>
     {!! $errors->first('title2', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="icon_image" style="font-size:18px;">{{ 'Icon Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="icon_image" type="file" id="icon_image" value="{{ isset($paymentsolution->icon_image) ? $paymentsolution->icon_image : ''}}" >
     {!! $errors->first('icon_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($paymentsolution->icon_image))
      <img src="{{URL::asset($paymentsolution->icon_image)}}" height="100" width="100" >
      @endif
 </div>
 <br>
 <label for="main_image" style="font-size:18px;">{{ 'Main Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="main_image" type="file" id="main_image" value="{{ isset($paymentsolution->main_image) ? $paymentsolution->main_image : ''}}" >
     {!! $errors->first('main_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($paymentsolution->main_image))
      <img src="{{URL::asset($paymentsolution->main_image)}}" height="100" width="100" >
      @endif
 </div>
 <br>
 <label for="description" style="font-size:18px;">{{ 'Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="description" type="textarea" id="description" required>{{ isset($paymentsolution->description) ? $paymentsolution->description : ''}}</textarea>
     {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="status" style="font-size:18px;">{{ 'Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="status" class="form-control" id="status" required>
    @foreach (json_decode('{"1": "Active", "0": "Disabled"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($paymentsolution->status) && $paymentsolution->status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
