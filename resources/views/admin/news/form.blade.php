 <label for="news_title" style="font-size:18px;">{{ 'News Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="news_title" type="text" id="news_title" value="{{ isset($news->news_title) ? $news->news_title : ''}}" required>
     {!! $errors->first('news_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="news_date" style="font-size:18px;">{{ 'News Date' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="news_date" type="date" id="news_date" value="{{ isset($news->news_date) ? $news->news_date : ''}}" >
     {!! $errors->first('news_date', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="news_file" style="font-size:18px;">{{ 'News File' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="news_file" type="file" id="news_file" value="{{ isset($news->news_file) ? $news->news_file : ''}}" >
     {!! $errors->first('news_file', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="news_status" style="font-size:18px;">{{ 'News Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="news_status" class="form-control" id="news_status" >
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($news->news_status) && $news->news_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('news_status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
