@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            News
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/news/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New News" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/news', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>News Title</th><th>News Date</th><th>News File</th><th>News Status</th>
                                        @can('view_news','edit_news', 'delete_news')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($news as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td width="400">{{ $item->news_title }}</td><td>@if(isset($item->news_date))
                                            @php
                                            $date=date("d-M-Y", strtotime($item->news_date));
                                            @endphp
                                             {{$date}}
                                            @else {{'-'}} 
                                             @endif</td>
                                        <td><a href="{{url($item->news_file)}}" target="_blank"><i class="material-icons">file_copy</i></a></td>
                                        <td>@if($item->news_status==1){{'Active' }}@else {{'Deactive'}} @endif</td>
                                        @can('view_news')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/news/' . $item->id) }}" title="View News"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'news',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $news->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
