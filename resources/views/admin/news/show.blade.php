@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            News  {{ $news->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/news') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_news', 'delete_news')
                         <a href="{{ url('/admin/news/' . $news->id . '/edit') }}" title="Edit News"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/news' . '/' . $news->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete News" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $news->id }}</td>
                                    </tr>
                                    <tr><th> News Title </th><td> {{ $news->news_title }} </td></tr><tr><th> News Date </th><td> @if(isset($news->news_date))
                            @php
                            $date=date("d-M-Y", strtotime($news->news_date));
                            @endphp
                             {{$date}}
                            @else {{'-'}} 
                             @endif </td></tr><tr><th> News File </th><td> <a href="{{url($news->news_file)}}" target="_blank"><i class="material-icons">file_copy</i></a> </td></tr><tr><th> News Status </th><td>@if($news->news_status==1){{'Active' }}@else {{'Deactive'}} @endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
