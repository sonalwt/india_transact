@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Footprints
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/footprints/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Footprint" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/footprints', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Counter Caption</th><th>Counter Number</th><th>Counter Unit</th><th>Counter Image</th><th>Counter Status</th>
                                        @can('view_footprints','edit_footprints', 'delete_footprints')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($footprints as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->counter_caption }}</td>
                                        <td>{{ $item->counter_number }}</td>
                                        <td>{{ $item->counter_unit }}</td>
                                        <td><img src="{{$item->counter_image}}" height="50" width="50"></td>
                                        <td>@if($item->counter_status==1){{'Active'}}@else{{'Deactive'}}@endif</td>
                                        @can('view_footprints')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/footprints/' . $item->id) }}" title="View Footprint"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'footprints',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $footprints->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
