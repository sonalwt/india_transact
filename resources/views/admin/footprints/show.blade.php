@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Footprint  {{ $footprint->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/footprints') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_footprints', 'delete_footprints')
                         <a href="{{ url('/admin/footprints/' . $footprint->id . '/edit') }}" title="Edit Footprint"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/footprints' . '/' . $footprint->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Footprint" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $footprint->id }}</td>
                                    </tr>
                                    <tr><th> Counter Caption </th><td> {{ $footprint->counter_caption }} </td></tr>
                                    <tr><th> Counter Number </th><td> {{ $footprint->counter_number }} </td></tr>
                                    <tr><th> Counter Unit </th><td> {{ $footprint->counter_unit }} </td></tr>
                                    <tr><th> Counter Image </th><td>@if(isset($footprint->counter_image)) <img src="{{$footprint->counter_image}}" height="50" width="50">@endif</td></tr>
                                    <tr><th> Counter Status </th><td> @if($footprint->counter_status ==1){{'Active' }}@else{{'Deactive'}}@endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
