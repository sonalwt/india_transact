 <label for="counter_caption" style="font-size:18px;">{{ 'Counter Caption' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="counter_caption" type="text" id="counter_caption" value="{{ isset($footprint->counter_caption) ? $footprint->counter_caption : ''}}" required>
     {!! $errors->first('counter_caption', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="counter_number" style="font-size:18px;">{{ 'Counter Number' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="counter_number" type="text" id="counter_number" value="{{ isset($footprint->counter_number) ? $footprint->counter_number : ''}}" required>
     {!! $errors->first('counter_number', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="counter_unit" style="font-size:18px;">{{ 'Counter Unit' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="counter_unit" type="text" id="counter_unit" value="{{ isset($footprint->counter_unit) ? $footprint->counter_unit : ''}}" >
     {!! $errors->first('counter_unit', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="counter_image" style="font-size:18px;">{{ 'Counter Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="counter_image" type="file" id="counter_image" value="{{ isset($footprint->counter_image) ? $footprint->counter_image : ''}}">
     {!! $errors->first('counter_image', '<p class="help-block">:message</p>') !!}
      </div>
      @if(isset($footprint->counter_image)) <img src="{{$footprint->counter_image}}" height="50" width="50">@endif
 </div>
  <br>
 <label for="counter_status" style="font-size:18px;">{{ 'Counter Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="counter_status" class="form-control" id="counter_status" required>
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($footprint->counter_status) && $footprint->counter_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('counter_status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
