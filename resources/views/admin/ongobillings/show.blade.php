@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Ongo Billing  {{ $ongobilling->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/ongobillings') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_ongobillings', 'delete_ongobillings')
                         <a href="{{ url('/admin/ongobillings/' . $ongobilling->id . '/edit') }}" title="Edit Ongobilling"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/ongobillings' . '/' . $ongobilling->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Ongobilling" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $ongobilling->id }}</td>
                                    </tr>
                                    <tr><th> Title </th><td> {{ $ongobilling->title }} </td></tr><tr><th> Image </th><td> {{ $ongobilling->image }} </td></tr><tr><th> Description </th><td> {{ $ongobilling->description }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
