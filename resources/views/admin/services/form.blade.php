 <label for="service_title" style="font-size:18px;">{{ 'Service Title' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="service_title" type="text" id="service_title" value="{{ isset($service->service_title) ? $service->service_title : ''}}" required>
     {!! $errors->first('service_title', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
 <br>
 <label for="service_description" style="font-size:18px;">{{ 'Service Description' }}</label>
 <div class="form-group">
      <div class="form-line">
        <textarea class="form-control ckeditor" rows="5" name="service_description" type="textarea" id="service_description" required>{{ isset($service->service_description) ? $service->service_description : ''}}</textarea>
     {!! $errors->first('service_description', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="service_image" style="font-size:18px;">{{ 'Service Image' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="service_image" type="file" id="service_image" value="{{ isset($service->service_image) ? $service->service_image : ''}}">
        @if(isset($service->service_image)) <img src="{{URL::asset($service->service_image)}}" alt="{{$service->service_title}}" height="200" width="200">@endif
     {!! $errors->first('service_image', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="service_sort_order" style="font-size:18px;">{{ 'Service Sort Order' }}</label>
 <div class="form-group">
      <div class="form-line">
        <input class="form-control" name="service_sort_order" type="text" id="service_sort_order" value="{{ isset($service->service_sort_order) ? $service->service_sort_order : ''}}" required>
     {!! $errors->first('service_sort_order', '<p class="help-block">:message</p>') !!}
      </div>
 </div>
  <br>
 <label for="service_status" style="font-size:18px;">{{ 'Service Status' }}</label>
 <div class="form-group">
      <div class="form-line">
        <select name="service_status" class="form-control" id="service_status" required>
    @foreach (json_decode('{"1": "Active", "0": "Deactive"}', true) as $optionKey => $optionValue)
        <option value="{{ $optionKey }}" {{ (isset($service->service_status) && $service->service_status == $optionKey) ? 'selected' : ''}}>{{ $optionValue }}</option>
    @endforeach
</select>
     {!! $errors->first('service_status', '<p class="help-block">:message</p>') !!}
      </div>
 </div>

<button type="submit" class="btn btn-success btn-lg m-t-15 waves-effect col-md-offset-5" style="font-size: 18px;border: double"><i class="material-icons">create</i>{{ $formMode === 'edit' ? 'Update' : 'Create' }}</button>
