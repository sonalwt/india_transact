@extends('admin.layouts.master')
@section('content')
    <div class="container">
        <div class="row">
                 <div class="col-md-10">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Service  {{ $service->id }}
                                        </h2>
                                    </div>
                    <div class="body">
                        <a href="{{ url('/admin/services') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="material-icons">arrow_back</i> Back</button></a>
                        @can('edit_services', 'delete_services')
                         <a href="{{ url('/admin/services/' . $service->id . '/edit') }}" title="Edit Service"><button class="btn btn-primary btn-sm"><i class="material-icons">mode_edit</i> Edit</button></a>
                         <form method="POST" action="{{ url('admin/services' . '/' . $service->id) }}" accept-charset="UTF-8" style="display:inline">
                                    {{ method_field('DELETE') }}
                                    {{ csrf_field() }}
                                    <button type="submit" class="btn btn-danger btn-sm" title="Delete Service" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="material-icons">delete</i> Delete</button>
                         </form>
                        @endcan
                        <br/>
                        <br/>
                        <div class="table-responsive">
                         <table class="table table-bordered table-striped table-hover">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $service->id }}</td>
                                    </tr>
                                    <tr><th> Service Title </th><td> {{ $service->service_title }} </td></tr>
                                    <tr><th> Service Description </th><td> {!! $service->service_description !!} </td></tr>
                                    <tr><th> Service Image </th><td>@if(isset($service->service_image)) <img src="{{URL::asset($service->service_image)}}" alt="{{$service->service_title}}" height="200" width="200"> @endif</td></tr>
                                    <tr><th> Service Sort Order </th><td> {{ $service->service_sort_order }} </td></tr>
                                    <tr><th> Service Status </th><td>@if($service->service_status==1) {{ 'Active' }}@else {{'Deactive'}}@endif </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
