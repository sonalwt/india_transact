@extends('admin.layouts.master')
<style>
    .searchBar{
        margin-right:22px !important;
    }
</style>
@section('content')
    <div class="container-fluid">
        <div class="row">
                     <div class="col-md-12">
                <div class="card">
                 <div class="header" style="background: #e2d1d1">
                                        <h2>
                                            Services
                                        </h2>
                                    </div>
                     <br>
                     <a href="{{ url('/admin/services/create') }}" class="btn btn-success btn-sm waves-effect" title="Add New Service" style="margin-left: 22px;">
                        <i class="material-icons">add_circle</i> Add New
                     </a>
                     {!! Form::open(['method' => 'GET', 'url' => '/admin/services', 'class' => 'navbar-form navbar-right searchBar', 'role' => 'search'])  !!}
                             <div class="input-group">
                             <input type="text" class="form-control" name="search" placeholder="Search..." style="border: ridge">
                             <span class="input-group-btn">
                             <button class="" type="submit">
                             <i class="material-icons" style="height: 27px !important;">search</i>
                              </button>
                              </span>
                              </div>
                     {!! Form::close() !!}
                     <div class="body">
                     <br>
                      <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Service Title</th>
                                        <th>Service Description</th>
                                        <th>Service Image</th>
                                        <th>Service Sort Order</th>
                                        <th>Service Status</th>
                                        @can('view_services','edit_services', 'delete_services')
                                        <th class="text-center">Actions</th>
                                        @endcan
                                    </tr>
                                </thead>
                                <tbody>
                                @foreach($services as $key => $item)
                                    <tr>
                                        <td>{{ ++$key }}</td>
                                        <td>{{ $item->service_title }}</td>
                                        <td width="400px">{!! $item->service_description !!}</td>
                                        <td><img src="{{ URL::asset($item->service_image) }}" alt="{{ $item->service_title }}" height="200" width="200"></td>
                                        <td>{{ $item->service_sort_order }}</td>
                                        <td>@if($item->service_status==1){{ 'Active' }}@else{{'Deactive'}}@endif</td>
                                        @can('view_services')
                                        <td class="text-center">
                                        <a href="{{ url('/admin/services/' . $item->id) }}" title="View Service"><button class="btn btn-info btn-xs"><i class="material-icons">remove_red_eye</i> View</button></a>
                                        @include('shared._actions', ['entity' => 'services',
                                        'id' => $item->id
                                        ])
                                         </td>
                                        @endcan
                                      </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $services->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
