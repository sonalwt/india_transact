@extends('layouts.app')
@if(isset($websitedetail->awards_page_title))
@section('title')
   {{$websitedetail->awards_page_title}}
@stop
@endif
@if(isset($websitedetail->awards_page_keyword))
@section('keywords')
   {{$websitedetail->awards_page_keyword}}
@stop
@endif
@if(isset($websitedetail->awards_page_description))
@section('description')
   {{$websitedetail->awards_page_description}}
@stop
@endif
@if(isset($websitedetail->awards_page_url))
@section('url')
   {{url($websitedetail->awards_page_url)}}
@stop
@endif
@if(isset($websitedetail->awards_page_image))
@section('image')
   {{URL::asset($websitedetail->awards_page_image)}}
@stop
@endif
@section('content')
<section class="section padding_bottom_50" id="awards-inner">
		<div class="container-fluid">			
			<div class="col-md-3 awards-title" data-aos-delay="500" data-aos="fade-right">
				<h1 class="title">Awards</h1>
				<span class="divider"></span>
			</div>							
		</div>

		<div class="container-fluid awards-listing">			
			<div class="col-md-12">
				<div class="col-md-12 list" data-aos-delay="500" data-aos="fade-right">
					<ul class="nav nav-tabs">
						@foreach($awardyears as $key=>$awardyear)
						<li @if($key==0)class="active"@endif><a href="#{{$awardyear->year}}">{{$awardyear->year}}</a></li>
						@endforeach
					  </ul>


					  <div class="tab-content">
					  	@foreach($awardyears as $key1=>$awardyr)
					  	<div id="{{$awardyr->year}}" @if($key1==0)class="tab-pane fade in active" @else class="tab-pane fade" @endif>
					      
					    	<div class="col-md-12 main">
					    		@foreach($awardyr->awards as $kay2=>$award)
					    		@php
					    		$in=$kay2;
					    		@endphp
                            <div class="col-md-4 block">
								<div class="col-md-2">
									<img src="{{URL::asset('/assets/images/awards/awards-inner/award-icon.png')}}" alt="award icon">
								</div>
						      	<div class="col-md-8 text">
						      		{!!$award->award_details!!}
						      	</div>
					      	</div>

					      	@if((++$in)%3 == 0)
					      		<div class="clearfix"></div>
					      	@endif
					      		@endforeach
					      </div>

					    </div>
					    @endforeach
					  </div>
				</div>
			</div>					
		</div>
	</section>
@endsection
@section('scripts')
<script type="text/javascript">
		AOS.init({
		  easing: 'ease-in-out-sine'
		});
		$('.bxslider').bxSlider({
		    minSlides: 1,
		    // maxSlides: 4,
		    // slideWidth: 300,
		    maxSlides: 6,
		    slideWidth: 200,
		    slideMargin: 0,
		    ticker: true,
		    speed: 50000
		});
	</script>

	<script>
		$(document).ready(function(){
		    $(".nav-tabs a").click(function(){
		        $(this).tab('show');
		    });
		});
	</script>
	@endsection