@extends('layouts.app')
@if(isset($websitedetail->contact_page_title))
@section('title')
   {{$websitedetail->contact_page_title}}
@stop
@endif
@if(isset($websitedetail->contact_page_keyword))
@section('keywords')
   {{$websitedetail->contact_page_keyword}}
@stop
@endif
@if(isset($websitedetail->contact_page_description))
@section('description')
   {{$websitedetail->contact_page_description}}
@stop
@endif
@if(isset($websitedetail->contact_page_url))
@section('url')
   {{url($websitedetail->contact_page_url)}}
@stop
@endif
@if(isset($websitedetail->contact_page_image))
@section('image')
   {{URL::asset($websitedetail->contact_page_image)}}
@stop
@endif
@section('content')
<section class="section" id="contact_section">
		<div class="container text-center title_desc">
            <div class="col-md-12 msg-carr">
                <?php if(isset($_SESSION['error_msg'])){ echo $_SESSION['error_msg']; unset($_SESSION['error_msg']); } ?>
            </div>
			<h3 class="title">Contact Us</h3>
			<span class="divider"></span>
		</div>
		@if(isset($reachus->map))
		<div class="container map_block">
				{!!$reachus->map!!}
		</div>
		@endif
		<div class="container contact_details">
			<div class="col-md-6 address">
				@if(isset($reachus->registered_office))
				<h4>Registered Office</h4>
				{!!$reachus->registered_office!!}
				@endif
				@if(isset($reachus->office_address))
				<h4>Office Address : </h4>
					{!!$reachus->office_address!!}
				@endif
				@if(isset($reachus->for_business_related_queries))
				<h4>FOR BUSINESS RELATED QUERIES : <span>{!!$reachus->for_business_related_queries!!}</span></h4>
				@endif
				@if(isset($reachus->for_media_related_queries))
				<h4>FOR MEDIA RELATED QUERIES : <span>{!!$reachus->for_media_related_queries!!}</span></h4>
				@endif
				@if(isset($reachus->health_and_support))
				<h4>HELP & SUPPORT : <span>i{!!$reachus->health_and_support!!}</span></h4>
				@endif
				@if(isset($reachus->work_with_us))
				<h4>WORK WITH US : <span>{!!$reachus->work_with_us!!}</span></h4>
				@endif
			</div>
			<div class="col-md-6">
				<div class="contact_form">
					<form action="javascript:void(0)" accept-charset="utf-8">
						<div class="col-md-6">
							<input type="text" placeholder="Name" name="name" id="name">
						</div>
						<div class="col-md-6">
							<input type="email" placeholder="Email" name="email" id="email">
						</div>
						<div class="col-md-6">
							<input type="text" placeholder="Mobile Number" name="contact" id="contact">
						</div>
						<div class="col-md-6">
							<input type="text" id="state" placeholder="Enter your State">
						</div>
						<div class="col-md-6">
							<input type="text" id="city" placeholder="Enter your City" >
						</div>
						<div class="col-md-6">
							<select id="interest">
								 <option value="">Interested In</option>
						    @foreach($products as $product)
						   <option value="{{$product->id}}">{{$product->title}}</option>
						   @endforeach
							</select>
						</div>
						<div class="col-md-12">
							<textarea id="message" placeholder="Write a message"></textarea>
						</div>
						<input type="hidden" id="contact_form_type" name="contact_form_type" value="contact-form">
						<div class="col-md-12 text-center">
							<input id="contact-enquiry-submit" type="submit" value="submit">
						</div>
						<div class="col-md-12 text-center">
							<span id="enq_msg"></span>
						</div>
					</form>
				</div>
			</div>
		</div>


	</section>


	<section class="section" id="contact_section">
		<div class="container text-center title_desc">
			<h3 class="title">Work With Us</h3>
			<span class="divider"></span>
			<p>
				If you are passionate about digital payments and enjoy the company of amazing colleagues, we will be more than happy to have you in our team of booming enthusiasts.<br>Just mail us your updated CV and we will get back to you!
			</p>
		</div>
		<div class="container contact_details">

			<div class="col-md-6 col-md-offset-3">
				<div class="contact_form">
					<form action="javascript:void(0)" method="POST" enctype="multipart/form-data">
						<div class="col-md-6">
							<input type="text" placeholder="Name" name="carr_name" id="carr_name">
						</div>
						<div class="col-md-6">
							<input type="email" placeholder="Email" name="carr_email" id="carr_email">
						</div>
						<div class="col-md-6">
							<input type="text" placeholder="Mobile Number" name="carr_mobile" id="carr_mobile">
						</div>
						<div class="col-md-12">
							<p>Upload Resume</p>
							<input type="file" name="resume_upload" id="resume_upload">
						</div>
						<div class="col-md-12">
							<textarea placeholder="Write a message" name="message" id="carr_message"></textarea>
						</div>
						<input type="hidden" id="career_form_type" name="career_form_type" value="career-contact-form">
						<div class="col-md-12 text-center">
							<input type="submit" value="submit" id="career_submit">
						</div>
						<div class="col-md-12 text-center">
							<span id="enq_msg_career"></span>
						</div>
					</form>
				</div>
			</div>
		</div>


	</section>
@endsection
@section('scripts')
<script>
	$('#career_submit').click(function(){
	//alert('test');
	var name = $('#carr_name').val();
	var email = $('#carr_email').val();
	var contact = $('#carr_mobile').val();
	var form_type = $('#career_form_type').val();
	var message = $('#carr_message').val();
	var file_name = $('#resume_upload').prop('files')[0];
	var flag = 0;
	var EmailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
    var phonePattern = /^[0-9]*$/;

    console.log(file_name);

	if(name != '' && name!= null)
	{
		$('#carr_name').removeClass('error');
	}
	else
	{
		$('#carr_name').addClass('error');
		flag++;
	}

	if(EmailPattern.test(email) && email != '' && email!= null)
	{
		$('#carr_email').removeClass('error');
	}
	else
	{
		$('#carr_email').addClass('error');
		flag++;
	}

	if(phonePattern.test(contact) && contact != '' && contact!= null)
	{
		$('#carr_mobile').removeClass('error');
	}
	else
	{
		$('#carr_mobile').addClass('error');
		flag++;
	}



	if(message != '' && message!= null)
	{
		$('#carr_message').removeClass('error');
	}
	else
	{
		$('#carr_message').addClass('error');
		flag++;
	}


    if(file_name != '' && file_name!= null)
	{
		$('#resume_upload').removeClass('error');
	}
	else
	{
		$('#resume_upload').addClass('error');
		flag++;
	}

	if(flag==0)
    {
       $('#career_submit').attr('disabled',true);
              $('#enq_msg_career').html('Please wait..');
              var form_data = new FormData();
        	  form_data.append('file', $('#resume_upload')[0].files[0]);
        	  form_data.append('name', name);
        	  form_data.append('email', email);
        	  form_data.append('mobile', contact);
      		  form_data.append('message', message);
        	  form_data.append('form_type', form_type);
    //     var ajax_data = {
    //             name : name,
    //             email : email,
    //             mobile : contact,
				// message:message,
				// form_type:form_type,
				// file_name:file_name,
    //         }
            
            //var formData = new FormData(this);
            //console.log(form_data);return false;
            $.ajax({
                type: "POST",
                url: '/careerform',
                contentType: false,
                cache: false,
                processData:false,
                data: form_data ,
                success: function(result) {
                    if(result == 1)
					{
						$('#career_submit').attr('disabled',false);

                        $('#enq_msg_career').html('Enquiry Placed Successfully');
						setTimeout(location.reload(), 30000);
					}
					else
					{
						$('#career_submit').attr('disabled',false);
                        $('#enq_msg_career').html('Error Occured');
					}

                },
                error: function() {
					$('#career_submit').attr('disabled',false);
                    $('#enq_msg_career').html('Error Occured');
                }
            });
    }
    
});

</script>
@endsection