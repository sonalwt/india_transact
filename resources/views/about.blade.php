@extends('layouts.app')
@if(isset($websitedetail->about_page_title))
@section('title')
   {{$websitedetail->about_page_title}}
@stop
@endif
@if(isset($websitedetail->about_page_keyword))
@section('keywords')
   {{$websitedetail->about_page_keyword}}
@stop
@endif
@if(isset($websitedetail->about_page_description))
@section('description')
   {{$websitedetail->about_page_description}}
@stop
@endif
@if(isset($websitedetail->about_page_url))
@section('url')
   {{url($websitedetail->about_page_url)}}
@stop
@endif
@if(isset($websitedetail->about_page_image))
@section('image')
   {{URL::asset($websitedetail->about_page_image)}}
@stop
@endif
@section('content')

@if(isset($aboutusdetail))
<section class="section abt-top-section" id="about">
		<div class="container-fluid">
			<div class="col-md-6 text-center img_block" data-aos="fade-right">
				<img src="{{URL::asset($aboutusdetail->image)}}" class="about1" alt="about" style="background: none;">
			</div>		
			<div class="col-md-6" data-aos="fade-left" data-aos-delay="500">
				<h1 class="title">{{$aboutusdetail->heading}}</h1>
				<span class="divider"></span>
				{!!$aboutusdetail->description!!}
			</div>				
		</div>
	</section>
	@endif
	<div class="click-button">
      <i class="fa fa-arrow-circle-o-down"></i>
    </div>

    @if(isset($aboutusvision))
	<section class="section scroll-section" id="vision">
		<div class="container-fluid">			
			<div class="col-md-6 about_content" data-aos-delay="500" data-aos="fade-right">
				<h1 class="title">{{$aboutusvision->title1}}</h1>
				<span class="divider" id="mission"></span>
				{!!$aboutusvision->description1!!}
				<h1 class="title">{{$aboutusvision->title2}}</h1>
				<span class="divider"></span>
				{!!$aboutusvision->description2!!}

			</div>
			<div class="col-md-6 text-center img_block" data-aos-delay="300" data-aos="fade-left">
				<img src="{{URL::asset($aboutusvision->image)}}" class="about2" alt="vision" style="background: none;">
			</div>					
		</div>
	</section>
	@endif
	


	<section class="section" id="bod">
		<div class="container-fluid">			
			<div class="col-md-6 about_content" data-aos-delay="500" data-aos="fade-right">
				<h1 class="title">Board Of Directors</h1>
				<span class="divider"></span>						
			</div>					
		</div>
	</section>


	@foreach($boardofdirectors as $k=>$boardofdirector)
	@php
	$in=$k+1;
	@endphp
	@if($in%2==0)
		<section class="section" id="bod{{$in}}">
		<div class="container-fluid">			
			<div class="col-md-6 about_content" data-aos-delay="500" data-aos="fade-right">
				<h2 class="title">{{$boardofdirector->name}}</h2>
				<h3 class="title"> {{$boardofdirector->designation}}</h3>
				<span class="divider"></span>
				{!!$boardofdirector->description!!}
				
			</div>
			<div class="col-md-6 text-center img_block" data-aos-delay="300" data-aos="fade-left">
				<img src="{{$boardofdirector->image}}" class="about2" alt="{{$boardofdirector->name}}" style="background: none;">
			</div>					
		</div>
	</section>
	@else
		<section class="section" id="bod{{$in}}">
		<div class="container-fluid">	
			<div class="col-md-6 text-center img_block" data-aos-delay="300" data-aos="fade-right">
				<img src="{{$boardofdirector->image}}" class="about3" alt="{{$boardofdirector->name}}" style="background: none;">
			</div>		
			<div class="col-md-6 about_content" data-aos-delay="500" data-aos="fade-left">
				<h2 class="title">{{$boardofdirector->name}}</h2>
				<h3 class="title">{{$boardofdirector->designation}}</h3>
				<span class="divider"></span>
				<p class="text-overflow">
					{!!$boardofdirector->description!!}
				</p>
			</div>					
		</div>
	</section>	
	@endif
	

	
	@endforeach
	

	<section class="section" id="team_about">
		<div class="container-fluid">
			<div class="col-md-6 about_content" data-aos-delay="500" data-aos="fade-right">
				<h1 class="title">Management Team</h1>
				<span class="divider"></span>						
			</div>
		</div>
		<div class="container-fluid">
			
			@foreach($members as $key=>$member)
			<div class="col-md-4 team_block" data-aos="flip-right" data-aos-delay="{{5+$key}}00">
				<img src="{{URL::asset($member->image)}}" alt="{{$member->name}}">
				<h3 class="team_title">{{$member->name}}</h3>
				<span class="team_caption">{{$member->designation}}</span>
				<p class="team_text">
					{!!$member->description!!}
				</p>
			</div>
			@php 
			$a=$key+1;
			@endphp
			@if($a%3==0)
			<div class="clearfix"></div>
			@endif
			@endforeach
			
		</div>
	</section>
	@endsection