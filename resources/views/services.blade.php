@extends('layouts.app')
@if(isset($websitedetail->service_page_title))
@section('title')
   {{$websitedetail->service_page_title}}
@stop
@endif
@if(isset($websitedetail->service_page_keyword))
@section('keywords')
   {{$websitedetail->service_page_keyword}}
@stop
@endif
@if(isset($websitedetail->service_page_description))
@section('description')
   {{$websitedetail->service_page_description}}
@stop
@endif
@if(isset($websitedetail->service_page_url))
@section('url')
   {{url($websitedetail->service_page_url)}}
@stop
@endif
@if(isset($websitedetail->service_page_image))
@section('image')
   {{URL::asset($websitedetail->service_page_image)}}
@stop
@endif
@section('content')
	@foreach($services as $key => $service)
  @php
      $count=$key;
      @endphp
  @if((++$count)%2 != 0)
  <section id="{{$service->service_title_as_id}}" class="section">
    <div class="container-fluid">
      <div class="col-md-6 pos_content" data-aos="fade-right">
        <h2 class="title">
          {{$service->service_title}}
        </h2>
        <span class="divider"></span>
        {!! $service->service_description !!}
      </div>
      <div class="col-md-6 pos_img text-center" data-aos="fade-left">
        <img src="{{URL::asset($service->service_image)}}" alt="{{$service->service_title}}" class="payment-n-pos" style="background: none;">
      </div>
    </div>
  </section>
  @else
  <section id="{{$service->service_title_as_id}}" class="section">
    <div class="container-fluid">
      <div class="col-md-6 pos_img text-center" data-aos="fade-left">
        <img src="{{URL::asset($service->service_image)}}" class="payment-n-pos" style="background: none;" alt="{{$service->service_title}}">
      </div>
      <div class="col-md-6 pos_content" data-aos="fade-right">
        <h2 class="title">
          {{$service->service_title}}
        </h2>
        <span class="divider"></span>       
          {!! $service->service_description !!}
      </div>
    </div>
  </section>
  @endif
  @endforeach
@endsection