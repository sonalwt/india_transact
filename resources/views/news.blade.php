@extends('layouts.app')
@if(isset($websitedetail->news_page_title))
@section('title')
   {{$websitedetail->news_page_title}}
@stop
@endif
@if(isset($websitedetail->news_page_keyword))
@section('keywords')
   {{$websitedetail->news_page_keyword}}
@stop
@endif
@if(isset($websitedetail->news_page_description))
@section('description')
   {{$websitedetail->news_page_description}}
@stop
@endif
@if(isset($websitedetail->news_page_url))
@section('url')
   {{url($websitedetail->news_page_url)}}
@stop
@endif
@if(isset($websitedetail->news_page_image))
@section('image')
   {{URL::asset($websitedetail->news_page_image)}}
@stop
@endif
@section('content')
<section class="section padding_bottom_50" id="awards-inner">
		<div class="container-fluid">			
			<div class="col-md-3 awards-title" data-aos-delay="500" data-aos="fade-right">
				<h1 class="title">News & Media</h1>
				<span class="divider" style="width: 85%;"></span>
			</div>							
		</div>
		<div class="container news_media">
			<table class="table">
				<!-- <caption>table title and/or explanatory text</caption> -->
				<thead>
					<tr>
						<th>Title</th>
						<th>Date</th>
						<th>Download</th>
					</tr>
				</thead>
				<tbody>

					@foreach($news as $news)
					
					<tr>
						<td>
							<a href="{{url($news->news_file)}}" target="_blank">
								{{$news->news_title}}
							</a>
						</td>
						<td>@if(isset($news->news_date))
							@php
							$date=date("d-M-Y", strtotime($news->news_date));
							@endphp
							 {{$date}}
							@else {{'-'}} 
							 @endif</td>
						<td>
							<a href="{{url($news->news_file)}}" target="_blank">
								<i class="fa fa-download" aria-hidden="true"></i>
							</a>
						</td>
					</tr>
					@endforeach
					
				</tbody>
			</table>
		</div>
	</section>
	@endsection