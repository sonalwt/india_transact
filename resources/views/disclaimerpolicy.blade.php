@extends('layouts.app')
@if(isset($websitedetail->disclaimer_policy_page_title))
@section('title')
   {{$websitedetail->disclaimer_policy_page_title}}
@stop
@endif
@if(isset($websitedetail->disclaimer_policy_page_keyword))
@section('keywords')
   {{$websitedetail->disclaimer_policy_page_keyword}}
@stop
@endif
@if(isset($websitedetail->disclaimer_policy_page_description))
@section('description')
   {{$websitedetail->disclaimer_policy_page_description}}
@stop
@endif
@if(isset($websitedetail->disclaimer_policy_page_url))
@section('url')
   {{url($websitedetail->disclaimer_policy_page_url)}}
@stop
@endif
@if(isset($websitedetail->disclaimer_policy_page_image))
@section('image')
   {{URL::asset($websitedetail->disclaimer_policy_page_image)}}
@stop
@endif
@section('content')
<section class="section">
		<div class="container-fluid">
			<div class="container disclaimer-wrapper">
				@if(isset($disclaimerpolicy))
					{!!$disclaimerpolicy->description!!}
				@endif
			</div>
		</div>
		
	</section>
@endsection