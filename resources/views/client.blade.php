@extends('layouts.app')
@if(isset($websitedetail->client_page_title))
@section('title')
   {{$websitedetail->client_page_title}}
@stop
@endif
@if(isset($websitedetail->client_page_keyword))
@section('keywords')
   {{$websitedetail->client_page_keyword}}
@stop
@endif
@if(isset($websitedetail->client_page_description))
@section('description')
   {{$websitedetail->client_page_description}}
@stop
@endif
@if(isset($websitedetail->client_page_url))
@section('url')
   {{url($websitedetail->client_page_url)}}
@stop
@endif
@if(isset($websitedetail->client_page_image))
@section('image')
   {{URL::asset($websitedetail->client_page_image)}}
@stop
@endif
@section('content')
<section class="section" id="business">
		<div class="container-fluid">
			<div class="why_title">
				<div class="col-md-6">
					<h1 class="title">Our Clients</h1>				
					<span class="divider"></span>
				</div>
				<span class="clearfix"></span>
				<div class="col-md-10">
					<span class="main-details">We believe in making our clients happy for which we always put in our sincere efforts. These are some of our pleased clients.</span>
				</div>
			</div>
		</div>
		<div class="container-fluid" id="testimonial_bg">
			<div class="container">
				<h3 class="title" style="text-align: center;color: #3e3e3e;">Client Testimonials</h3>	
				<div class="owl-carousel owl-theme" id="testimonial-slider">
					@foreach($clienttestomonials as $clienttestomonial)
					
	              <div class="item col-md-12">
	              	<div class="testimonial_inner">
<!--             			<img src="images/clients/16.png" alt="client" class="text-center">-->
                        
             				{!! $clienttestomonial->client_message!!}
             			
                        <h3>{{$clienttestomonial->client_name}}</h3>
             		</div>
	              </div>
	              @endforeach
	            </div>
			</div>
		</div>
		<div class="container-fluid">			
			<!-- <h2 class="business_title" data-aos="zoom-in" data-aos-delay="500">Security & Certifications</h2> -->
			<div class="container">
				@foreach($clients as $client)
				<div class="col-md-3 certificate_team" data-aos-delay="100" data-aos="flip-right">
					<span class="">
						<img src="{{$client->client_logo}}" alt="{{$client->client_name}}">
					</span>
				</div>
				@endforeach
			</div>
		</div>
		
	</section>
	@endsection

	@section('scripts')
	<script>
		AOS.init({
		  easing: 'ease-in-out-sine'
		});


		$('#testimonial-slider').owlCarousel({
	       autoplay: true,
	       autoplayHoverPause: true,
	       loop: true,
	       // margin: 20,
	       responsiveClass: true,
	       nav: true,
	       dots: false,
	       navText: ["<i class='fa fa-caret-left' aria-hidden='true'></i>","<i class='fa fa-caret-right'></i>"],
	       loop: true,
	       items: 1,
	       responsive: {
	         0: {
	           items: 1
	         },
	         568: {
	           items: 1
	         },
	         600: {
	           items: 1
	         },
	         1000: {
	           items: 1
	         }
	       }
	      });
	</script>
	
	@endsection