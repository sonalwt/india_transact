<!DOCTYPE html>
<html>
<head>
    <title>  @yield('title')</title>
   
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="keywords" content="@yield('keywords')"/>
    <meta name="description" content="@yield('description')"/>
    <meta property="og:url" content="@yield('url')">
    <meta property="og:image" content="@yield('image')">
    <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-170183189-1"></script>
        <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-170183189-1');
        </script>
        <!-- Global site tag end -->
    <link rel="shortcut icon" href="{{URL::asset($websitedetail->favicon)}}" type="image/png" sizes="32x32">
    <!-- Global site tag end -->
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
    <!-- stylesheet -->
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL::asset('assets/css/responsive.css')}}">
    <!-- <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/assets/owl.carousel.css'> -->
    <link rel='stylesheet prefetch' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css'>
    <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.min.css" integrity="sha256-UhQQ4fxEeABh4JrcmAJ1+16id/1dnlOEVCFOxDef9Lw=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css" integrity="sha256-nXBV7Gr2lU0t+AwKsnS05DYtzZ81oYTXS6kj7LBQHfM=" crossorigin="anonymous" />
    <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="{{URL::asset('/assets/css/ps_slider.css')}}">
  <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
  <script src="{{URL::asset('assets/js/app.js')}}"></script>
    
    <!-- <link href="https://snatchbot.me/sdk/webchat.css" rel="stylesheet" type="text/css">

        <script src="https://snatchbot.me/sdk/webchat.min.js"></script>

        <script> Init('?botID=41134&appID=webchat', 600, 600, 'https://dvgpba5hywmpo.cloudfront.net/media/image/1cqvw5Z9Q2BgxWOw4CqMcqf6m', 'rounded', '#89C141', 90, 90, 62.99999999999999, '', '1', '#FFFFFF', '#FFFFFF', 0);

        </script> -->
    
     <!-- Global site tag (gtag.js) - Google Ads: 725284602 -->
   <!--  <script async src="https://www.googletagmanager.com/gtag/js?id=AW-725284602"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());

      gtag('config', 'AW-725284602');
    </script>
     -->
  <!--   <script>
      gtag('event', 'page_view', {
        'send_to': 'AW-725284602',
        'value': 'replace with value',
        'items': [{
          'id': 'replace with value',
          'google_business_vertical': 'retail'
        }, {
          'id': 'replace with value',
          'location_id': 'replace with value',
          'google_business_vertical': 'custom'
        }]
      });
    </script> -->
    
    
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


</head>
<body id="transcroller-body" class="aos-all">

    <!-- Header -->
     @include('layouts.header')

    
        <main class="py-4">
            @yield('content')
        </main>


    <!-- Footer -->
     @include('layouts.footer')




    <!-- scripts -->
    <script src='https://code.jquery.com/jquery-1.11.1.min.js'></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- <script src='https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.0.0-beta.3/owl.carousel.js'></script> -->
    <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js" integrity="sha256-pTxD+DSzIwmwhOqTFN+DB+nHjO4iAsbgfyFq5K5bcE0=" crossorigin="anonymous"></script>
    <script src="{{URL::asset('assets/js/jquery.exitintent.js')}}"></script>
    <script src="{{URL::asset('assets/js/main.js')}}"></script>

    <!-- <script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script> -->
    <script type="text/javascript">
       
    </script>
      @yield('scripts')
</body>
</html>
