<div class="btm-frm-wrapper">
        <div class="container desktop">
            <div class="row footer-form">

               <div class="col-md-12">
                   <form  autocomplete="off" class="form-sticky lead_form" autocomplete="off">
                      <div class="row">
                          <div class="co-md-2 col">
                              <input autocomplete="off" type="text" data-req="y" data-type="text" class="capture form-control enqu-form" placeholder="Name" name="name" id="footer_name">
                          </div>

                          <div class="co-md-2 col">
                              <input autocomplete="off" type="email" data-req="y" data-type="email" class="capture form-control enqu-form" placeholder="Email" id="footer_email" name="email">
                          </div>

                          <div class="co-md-2 col">
                              <input autocomplete="off" type="tel" data-req="y" data-type="mobile" class="capture form-control enqu-form" placeholder="Mobile" id="footer_mobile" name="mobile">
                          </div>

                          <div class="co-md-2 col">
                              <input autocomplete="off" type="text" data-req="y" data-type="state" class="capture form-control enqu-form" placeholder="State" id="footer_state" name="state">
                          </div>
                          <input type="hidden" id="footer_form_type" name="footer_form_type" value="footer-form">
                          <input type="hidden" id="footer_utm_source" name="footer_utm_source" value="{{ isset($utm_source) ? $utm_source : '' }}">
                          <input type="hidden" id="footer_utm_medium" name="footer_utm_medium" value="{{ isset($utm_medium) ? $utm_medium : '' }}">
                          <input type="hidden" id="footer_utm_campaign" name="footer_utm_campaign" value="{{ isset($utm_campaign) ? $utm_campaign : '' }}">
                          <div class="co-md-3 col">
                              <button type="submit" id="frm_submit" class="btn btn-primary btn-block">Submit</button>
                          </div>
                      </div>
                           <div id="frm_msg"></div>
                    </form>
               </div>
           </div>
        </div>
    </div>


 <footer>
     <div id="stop" class="scrollTop">
        <span><a href=""><i class="fa fa-chevron-up"></i></a></span>
     </div>
    <div class="footer_links_block">
        <div class="container text-left">
            <h4>Quick Links</h4>
            <hr>
        </div>
        <div class="container">
            <div class="footer_widget col-md-2" style="padding-left: 0px;">
                <h4><a href="{{url('/')}}">Home</a></h4>
                <h4><a href="{{url('/about')}}">About</a></h4>
                <ul class="footer_links">
                    <li><a href="{{url('/about#mission')}}">Mission</a></li>
                    <li><a href="{{url('/about#vision')}}">Vision</a></li>
                    <li><a href="{{url('/about#bod')}}">Board of directors</a></li>
                    <li><a href="{{url('/about#team_about')}}">Management Team</a></li>
                </ul>
            </div>
            <div class="footer_widget col-md-2">
                <h4><a href="{{url('/products')}}">Products & Solutions</a></h4>
                <ul class="footer_links">
                    <li><a href="{{url('/products')}}">Payments & POS</a></li>
                    <li><a href="{{url('/products#loyalty')}}">Loyalty</a></li>
                    <li><a href="{{url('/products#ongo')}}">Ongo Billing++</a></li>
                    <li><a href="{{url('/products#ongo-pos')}}">Ongo Business POS</a></li>
                    <li><a href="{{url('/products#online-presence')}}">Merchant Services</a></li>
                </ul>
            </div>
            <div class="footer_widget col-md-2">
                <h4><a href="{{url('/service')}}">Services</a></h4>
                <ul class="footer_links">
                    <li><a href="{{url('/service#merchant-acquiring-services')}}">Merchant Acquiring</a></li>
                    <li><a href="{{url('/service#payment-business-solutions')}}">Payment & Business Solutions</a></li>
                    <li><a href="{{url('/service#issuance-processing')}}">Issuance Processing</a></li>
                    <li><a href="{{url('/service#switching-solutions')}}">Switching Solutions</a></li>

                </ul>
            </div>
            <div class="footer_widget col-md-2">
                <h4><a href="{{url('/businessvertical')}}">Business Verticals</a></h4>
                <h4><a href="{{url('/client')}}">Clientele</a></h4>
                <h4><a href="{{url('/award')}}">Awards & Recognition</a></h4>
                <h4><a href="{{url('/news')}}">Media & News Archieve</a></h4>
                <h4><a href="{{url('/contactus')}}">Reach Us</a></h4>
            </div>
            <div class="footer_widget col-md-3" style="width: 21%;">
                <h4>To Download our apps</h4>
                <ul class="footer_links footer_app_links">
                    <li>
                        <a href="{{$footerlink->ongomerchant}}" target="_blank">Ongo Merchant++</a>
                        <a href="{{$footerlink->ongomerchant}}" target="_blank"><img src="{{URL::asset('assets/images/playstore.png')}}" alt="Ongo Merchant++ playstore"></a>
<!--                        <a href="https://itunes.apple.com/in/app/ongo-app/id1079422557?mt=8" target="_blank"><img src="images/apple_store.png" alt="Ongo Merchant++ App store"></a>
 -->                    </li>
                    <li>
                        <a href="{{$footerlink->ongopaytrack}}" target="_blank">Ongo PayTrack</a>
                        <a href="{{$footerlink->ongopaytrack}}" target="_blank"><img src="{{URL::asset('assets/images/playstore.png')}}" alt="Ongo Merchant++ playstore"></a>
                        <!-- <a href="https://play.google.com/store/apps/details?id=com.agsindia.paytrack&hl=en_IN" target="_blank" style="color: #fff;font-size: 10px;">*Coming Soon</a> -->
                        <!-- <a href="#" target="_blank"><img src="images/apple_store.png" alt=""></a> -->
                    </li>
                    <li>
                        <a href="{{$footerlink->ongobilling}}" target="_blank">Ongo Billing++</a>
                        <a href="{{$footerlink->ongobilling}}" target="_blank"><img src="{{URL::asset('assets/images/playstore.png')}}" alt="Ongo Merchant++ playstore"></a>
                        <!-- <a href="http://www.ongobilling.com/" target="_blank" style="color: #fff;font-size: 10px;">>*Coming Soon</a> -->
                        <!-- <a href="#" target="_blank"><img src="images/apple_store.png" alt=""></a> -->
                    </li>
                    <li>
                        <a href="{{$footerlink->ongoqr}}" target="_blank">Ongo QR++</a>
                        <a href="{{$footerlink->ongoqr}}" target="_blank"><img src="{{URL::asset('assets/images/playstore.png')}}" alt="Ongo Merchant++ playstore"></a>
                        <!-- <a href="#" target="_blank" style="color: #fff;font-size: 10px;">*Coming Soon</a> -->
                        <!-- <a href="#" target="_blank"><img src="images/apple_store.png" alt=""></a> -->
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container footer_white_block">
        <div class="col-md-5 text-left links_block_1">
            <ul class="copyright_links">
                <li><a href="{{url('/privacypolicy')}}">Privacy Policy </a></li>
                <li><a href="{{url('/disclaimerpolicy')}}">Disclaimer Policy </a></li>
                <!-- <li><a href="#">Cancellation, Refund & Return Policy </a></li> -->
                <li><a href="{{URL::asset('/assets/docs/ITSL_Vigil Mechanism-Whistle Blower Policy.pdf')}}" target="_blank">Whistle Blower Policy</a></li>

                <!-- <li><a href="javascript:void(0);">Cancellation, Refund & Return Policy </a></li> -->
                <li><a href="{{URL::asset('/assets/docs/ITSL_Corporate_Social_Responsibility_Policy.pdf')}}" target="_blank">ITSL CSR Policy</a></li>
                <li><a href="{{$investorrelation->doc_file}}" download>Investor Relations</a></li>
                <li><a href="{{url('/ewastepolicy')}}">E-waste Management Policy</a></li>
                @if(isset($financials))
                @foreach($financials as $financial)
                <li><a href="{{URL::asset($financial->file)}}" target="_blank">{{$financial->title}}</a></li>
                <br>
                @endforeach
                @endif
            </ul>

        </div>
        
        <div class="col-md-6">
            <div class="ags" style="display: flex;justify-content: flex-end;align-items: center;">
               
                    <img src="{{URL::asset('assets/images/ags_logo.png')}}" alt="" style="width: 20%;">
              
                <p>
                    <a href="http://sites.d-designstudio.com/ags-aspx" target="_blank" style="color:#505050;">AGS Transact Technologies Ltd.</a>
                </p>
            </div>

            <div class="flex_social">
                <h4 class="footer_social">Follow Us</h4>
                <ul class="follow_social">
                    <li id="facebook"><a href="{{$footerlink->facebook}}" class="fa fa-facebook bg-primary" target="_blank"></a></li>
                    <li id="twitter"><a href="{{$footerlink->twitter}}" class="fa fa-twitter bg-primary" target="_blank"></a></li>
                    <li id="linkedin"><a href="{{$footerlink->linkedin}}" class="fa fa-linkedin bg-primary"  target="_blank"></a></li>
                    <li id="instagram"><a href="{{$footerlink->instagram}}" class="fa fa-instagram bg-primary" target="_blank"></a></li>
                    <li id="youtube"><a href="{{$footerlink->youtube}}" class="fa fa-youtube bg-primary" target="_blank"></a></li>
                </ul>
            </div>
            <p style="text-align: right;">
                &copy; Copyright 2018 | All Rights Reserved
            </p>
            
        </div>
    </div>

</footer>

@section('scripts')
    <script>
    $(document).ready(function(){

 });
</script>
@endsection