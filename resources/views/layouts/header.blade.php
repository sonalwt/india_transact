
	<header>
		<div class="logo">
			<a href="{{url('/')}}"><img src="{{URL::asset($websitedetail->logo)}}"></a>
		</div>		
		<div class="side_nav">	
			<div class="right_menu">		
				<div class="header_tr_text">
					<h1>Transactions Made Simple</h1>
				</div>
				<div class="home_icon">
					<a href="{{url('/')}}" style="color:#595959"><i class="fa fa-home" aria-hidden="true"></i></a>
				</div>			
				<div class="nav_btn">
					<span class="icon_bar"></span>
					<span class="icon_bar"></span>
					<span class="icon_bar"></span>
					<span class="icon_bar"></span>
				</div>
			</div>
		<div class="nav_menu">
			<div class="close_btn">
				<i class="fa fa-times" aria-hidden="true"></i>
			</div>
			<nav>
				<ul>
					
					<li class="active"><a href="{{url('/')}}"> Home</a></li>
					<li><a href="{{url('/about')}}"> About</a></li>
					<li><span id="link1" class="navLinkAnchors" href="#1"><a href="#">Products & Solutions&nbsp;<i class="fa fa-plus"></i>&nbsp;<i class="fa fa-plus"></i></span></a>
						<ul id="1" class="navLinks"> 
							<li><a href="{{url('/products')}}">Payments & POS</a></li>
							<li><a href="{{url('/products#loyalty')}}">Loyalty</a></li>
							<li><a href="{{url('/products#ongo')}}">Ongo Billing++</a></li>
							<li><a href="{{url('/products#ongo-pos')}}"> Ongo Business POS </a></li>
							<li><a href="{{url('/products#online-presence')}}">Merchant Services</a></li> </ul>
						</li>
					<li><span id="link2" class="navLinkAnchors2" href="#2"><a href="#">Services&nbsp;<i class="fa fa-plus"></i>&nbsp;<i class="fa fa-plus"></i></span></a>
						<ul id="2" class="navLinks2"> 
							<li><a href="{{url('/service#merchant-acquiring-services')}}">Merchant Acquiring</a></li>
							<li><a href="{{url('/service#payment-business-solutions')}}">Payment & Business Solutions</a></li>
							<li><a href="{{url('/service#issuance-processing')}}">Issuance Processing</a></li>
							<li><a href="{{url('/service#switching-solutions')}}">Switching Solutions</a></li>
						</ul>

					</li>
					
					<li><a href="{{url('/businessvertical')}}">Business Verticals</a></li>
					<li><a href="{{url('/client')}}">Clientele</a></li>
					<li><a href="{{url('/award')}}">Awards & Recognition</a></li>
					<li><a href="{{url('/news')}}"> Media & News Archive</a></li>
					<li><a href="{{url('/contactus')}}"> Reach Us</a></li>
					
				</ul>

				<ul class="list-inline footer-social m-b0 nav-social">
					<li id="facebook"><a href="{{$footerlink->facebook}}" class="fa fa-facebook bg-primary" target="_blank"></a></li>
					<li id="twitter"><a href="{{$footerlink->twitter}}" class="fa fa-twitter bg-primary" target="_blank"></a></li>
					<li id="linkedin"><a href="{{$footerlink->linkedin}}" class="fa fa-linkedin bg-primary"  target="_blank"></a></li>
					<li id="instagram"><a href="{{$footerlink->instagram}}" class="fa fa-instagram bg-primary" target="_blank"></a></li>
					<li id="youtube"><a href="{{$footerlink->youtube}}" class="fa fa-youtube bg-primary" target="_blank"></a></li>
				</ul>
			</nav>
		</div>
		</div>
	</header>
	<!-- End of Header -->