<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusinessverticalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('businessverticals', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('business_title')->nullable();
            $table->string('business_heading_id')->nullable();
            $table->text('business_description')->nullable();
            $table->string('business_icon')->nullable();
            $table->string('business_icon_selected')->nullable();
            $table->string('business_image')->nullable();
            $table->string('business_sort_order')->nullable();
            $table->string('business_status');
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('businessverticals');
    }
}
