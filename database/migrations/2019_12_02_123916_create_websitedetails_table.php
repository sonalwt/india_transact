<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWebsitedetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('websitedetails', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('logo')->nullable();
            $table->string('favicon')->nullable();
            $table->string('index_page_title')->nullable();
            $table->string('index_page_keyword')->nullable();
            $table->string('index_page_description')->nullable();
            $table->string('index_page_url')->nullable();
            $table->string('index_page_image')->nullable();
            $table->string('about_page_title')->nullable();
            $table->string('about_page_keyword')->nullable();
            $table->string('about_page_description')->nullable();
            $table->string('about_page_url')->nullable();
            $table->string('about_page_image')->nullable();
            $table->string('product_page_title')->nullable();
            $table->string('product_page_keyword')->nullable();
            $table->string('product_page_description')->nullable();
            $table->string('product_page_url')->nullable();
            $table->string('product_page_image')->nullable();
            $table->string('service_page_title')->nullable();
            $table->string('service_page_keyword')->nullable();
            $table->string('service_page_description')->nullable();
            $table->string('service_page_url')->nullable();
            $table->string('service_page_image')->nullable();
            $table->string('business_vertical_page_title')->nullable();
            $table->string('business_vertical_page_keyword')->nullable();
            $table->string('business_vertical_page_description')->nullable();
            $table->string('business_vertical_page_url')->nullable();
            $table->string('business_vertical_page_image')->nullable();
            $table->string('client_page_title')->nullable();
            $table->string('client_page_keyword')->nullable();
            $table->string('client_page_description')->nullable();
            $table->string('client_page_url')->nullable();
            $table->string('client_page_image')->nullable();
            $table->string('awards_page_title')->nullable();
            $table->string('awards_page_keyword')->nullable();
            $table->string('awards_page_description')->nullable();
            $table->string('awards_page_url')->nullable();
            $table->string('awards_page_image')->nullable();
            $table->string('news_page_title')->nullable();
            $table->string('news_page_keyword')->nullable();
            $table->string('news_page_description')->nullable();
            $table->string('news_page_url')->nullable();
            $table->string('news_page_image')->nullable();
            $table->string('contact_page_title')->nullable();
            $table->string('contact_page_keyword')->nullable();
            $table->string('contact_page_description')->nullable();
            $table->string('contact_page_url')->nullable();
            $table->string('contact_page_image')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('websitedetails');
    }
}
