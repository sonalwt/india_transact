<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateReachusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reachuses', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('map')->nullable();
            $table->text('registered_office')->nullable();
            $table->text('office_address')->nullable();
            $table->string('for_business_related_queries')->nullable();
            $table->string('for_media_related_queries')->nullable();
            $table->string('health_and_support')->nullable();
            $table->string('work_with_us')->nullable();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reachuses');
    }
}
