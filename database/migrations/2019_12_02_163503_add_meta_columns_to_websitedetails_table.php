<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMetaColumnsToWebsitedetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('websitedetails', function (Blueprint $table) {
            //
            $table->string('privacy_policy_page_title')->nullable();
            $table->string('privacy_policy_page_keyword')->nullable();
            $table->string('privacy_policy_page_description')->nullable();
            $table->string('privacy_policy_page_url')->nullable();
            $table->string('privacy_policy_page_image')->nullable();
            $table->string('disclaimer_policy_page_title')->nullable();
            $table->string('disclaimer_policy_page_keyword')->nullable();
            $table->string('disclaimer_policy_page_description')->nullable();
            $table->string('disclaimer_policy_page_url')->nullable();
            $table->string('disclaimer_policy_page_image')->nullable();
            $table->string('ewaste_policy_page_title')->nullable();
            $table->string('ewaste_policy_page_keyword')->nullable();
            $table->string('ewaste_policy_page_description')->nullable();
            $table->string('ewaste_policy_page_url')->nullable();
            $table->string('ewaste_policy_page_image')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('websitedetails', function (Blueprint $table) {
              $table->drop('privacy_policy_page_title');
            $table->drop('privacy_policy_page_keyword');
            $table->drop('privacy_policy_page_description');
            $table->drop('privacy_policy_page_url');
            $table->drop('privacy_policy_page_image');
            $table->drop('disclaimer_policy_page_title');
            $table->drop('disclaimer_policy_page_keyword');
            $table->drop('disclaimer_policy_page_description');
            $table->drop('disclaimer_policy_page_url');
            $table->drop('disclaimer_policy_page_image');
            $table->drop('ewaste_policy_page_title');
            $table->drop('ewaste_policy_page_keyword');
            $table->drop('ewaste_policy_page_description');
            $table->drop('ewaste_policy_page_url');
            $table->drop('ewaste_policy_page_image');
        });
    }
}
