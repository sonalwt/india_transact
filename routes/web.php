<?php


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

 Route::get('/', 'IndexController@index');
 Route::get('/service', 'IndexController@services');
 Route::get('/businessvertical', 'IndexController@businessverticals');
 Route::get('/client', 'IndexController@clients');
 Route::get('/award', 'IndexController@awards');
 Route::get('/news', 'IndexController@news');
 Route::get('/about', 'IndexController@about');
 Route::get('/products', 'IndexController@products');
 Route::get('/contactus', 'IndexController@contactus');
 Route::get('/privacypolicy', 'IndexController@privacypolicy');
 Route::get('/disclaimerpolicy', 'IndexController@disclaimerpolicy');
 Route::get('/ewastepolicy', 'IndexController@ewastepolicy');
 Route::post('/modalenquiryform', 'IndexController@modalenquiryform');
 Route::post('/careerform', 'IndexController@careerform');
 Route::get('/thankyou', 'IndexController@thankyou');
 
Route::group(['prefix' => 'admin','namespace' => 'Admin\Auth','middleware' => 'admin_guest'], function () {
    Route::get('login', 'LoginController@showLoginForm');
    Route::post('login', 'LoginController@login')->name('admin.login');
    Route::get('register', 'RegisterController@showRegistrationForm');
    Route::post('register', 'RegisterController@register')->name('admin.register');
    Route::get('password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    Route::post('password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    Route::get('password/reset/{token}', 'ResetPasswordController@showResetForm')->name('admin.password.reset');
    Route::post('password/reset', 'ResetPasswordController@reset')->name('admin.password.update');
});
Route::group(['prefix' => 'admin','namespace' => 'Admin\Auth','middleware' => 'admin_auth'], function () {
    Route::get('/logout', 'LoginController@logout')->name('admin.logout');
});

Route::group(['prefix' => 'admin','namespace' => 'Admin\Auth','middleware' => ['admin_auth','emailNot_verified']], function () {
    Route::get('email/verify', 'VerificationController@show')->name('admin.verification.notice');
    Route::get('email/verify/{id}', 'VerificationController@verify')->name('admin.verification.verify');
    Route::get('email/resend', 'VerificationController@resend')->name('admin.verification.resend');
});

Route::group(['prefix' => 'admin','namespace' => 'Admin','middleware' => ['admin_auth','email_verified']], function () {
    Route::get('/home', 'HomeController@index');
    Route::get('/profile', 'HomeController@profile');
    Route::post('/profile', 'HomeController@saveprofile');
    Route::resource('/users', 'UserController');
    Route::resource('/roles', 'RoleController');
    Route::resource('submenus', 'SubmenusController');
    Route::resource('menus', 'MenusController');
    Route::resource('services', 'ServicesController');
    Route::resource('businessheadings', 'BusinessheadingsController');
    Route::resource('businessverticals', 'BusinessverticalsController');
    Route::resource('clienttestomonials', 'ClienttestomonialsController');
    Route::resource('clients', 'ClientsController');
    Route::resource('awardyears', 'AwardyearsController');
    Route::resource('awards', 'AwardsController');
    Route::resource('news', 'NewsController');
    Route::resource('footprints', 'FootprintsController');
    Route::resource('keystrengths', 'KeystrengthsController');
    Route::resource('homeoverviews', 'HomeoverviewsController');
    Route::resource('hometransactionheadings', 'HometransactionheadingsController');
    Route::resource('hometransactions', 'HometransactionsController');
    Route::resource('homenews', 'HomenewsController');
    Route::resource('homeawards', 'HomeawardsController');
    Route::resource('homevideos', 'HomevideosController');
    Route::resource('homeassociates', 'HomeassociatesController');
    Route::resource('aboutusdetails', 'AboutusdetailsController');
    Route::resource('aboutusvisions', 'AboutusvisionsController');
    Route::resource('boardofdirectors', 'BoardofdirectorsController');
    Route::resource('managementteams', 'ManagementteamsController');
    Route::resource('pospayments', 'PospaymentsController');
    Route::resource('paymentsolutions', 'paymentsolutionsController');
    Route::resource('loyaltyservices', 'LoyaltyservicesController');
    Route::resource('ongobusinesses', 'OngobusinessesController');
    Route::resource('ongobillings', 'OngobillingsController');
    Route::resource('merchantservices', 'MerchantservicesController');
    Route::resource('reachuses', 'ReachusesController');
    Route::resource('privacypolicies', 'PrivacypoliciesController');
    Route::resource('disclaimerpolicies', 'DisclaimerpoliciesController');
    Route::resource('ewastepolicies', 'EwastepoliciesController');
    Route::resource('enquiryformproducts', 'EnquiryformproductsController');
    Route::resource('collectioncentreaddresses', 'CollectioncentreaddressesController');
    Route::resource('footerlinks', 'FooterlinksController');
    Route::resource('investorrelations', 'InvestorrelationsController');
    Route::get('enquiryformleads', 'EnquiryformproductsController@enquiryformleads');
    Route::get('careerformleads', 'EnquiryformproductsController@careerformleads');
    Route::resource('websitedetails', 'websitedetailsController');
    Route::resource('financials', 'FinancialsController');
});

