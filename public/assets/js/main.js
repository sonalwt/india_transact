// main js
$(window).scroll(function(){
    if ($(window).scrollTop() >= 10) {
      $('header').addClass('bg-color');
      $('.tr_text').addClass('tr_text_remove');
      $('.header_tr_text').addClass('header_tr_active');
     }
     else {
      $('header').removeClass('bg-color');
      $('.tr_text').removeClass('tr_text_remove');
      $('.header_tr_text').removeClass('header_tr_active');
     }
  });

$('.nav_btn').on("click", function(){
  $('.nav_menu').addClass('open_nav');
});
$('.close_btn').on("click", function(){
  $('.nav_menu').removeClass('open_nav');
});

  $('.slide_icons').on('click', function(){
    $('.slide_icons').removeClass("active_icon");
    $(this).addClass('active_icon');
    var sub_id = $(this).attr('id');
    $(".sc_content").css("display","none");
    $(".slider_center_block").css("display","none");

    $("#sub_"+sub_id).fadeIn( 1000 );
    $("#sub_"+sub_id).animate({ opacity: 1 }, 1);
    $("#s_c_"+sub_id).fadeIn( 1000 );
    $("#s_c_"+sub_id).animate({ opacity: 1 }, 1);
  });

  //product js start here
   $('.product_slide_icons').on('click', function(){
    $('.product_slide_icons').removeClass("active_icon");
    $(this).addClass('active_icon');
    var sub_id = $(this).attr('id');

    $(".sc_content").css("display","none");

    $("#sub_"+sub_id).fadeIn( 1000 );
    $("#sub_"+sub_id).animate({ opacity: 1 }, 1);
  });
  //product js end here

  // $(window).scroll(function(){
  //    if ($(window).scrollTop() >= 10) {
  //      $('header').addClass('bg-color');
  //     }
  //     else {
  //      $('header').removeClass('bg-color');
  //     }
  //  });

// $('#client-slider').owlCarousel({
//  autoplay: true,
//  autoplayHoverPause: true,
//  loop: true,
//  // margin: 20,
//  responsiveClass: true,
//  nav: true,
//  navText: ["<i class='fa fa-chevron-left' aria-hidden='true'></i>","<i class='fa fa-chevron-right' aria-hidden='true'></i>"],
//  loop: true,
//  items: 4,
//  responsive: {
//    0: {
//      items: 1
//    },
//    568: {
//      items: 2
//    },
//    600: {
//      items: 3
//    },
//    1000: {
//      items: 4
//    }
//  }
// });

$(document).ready(function(){
    

    $('[data-toggle="tooltip"]').tooltip();
});



// script for navigation//

// $(document).ready(function(){
//     $(".navLinkAnchors").click(function(){
//     	var id=$(this).attr('href');
//     	//alert(id);
//         $(id).toggle(500);
//     });
// });
$(document).ready(function(){
    $(".navLinkAnchors").click(function(){
        $(".navLinks").toggle(500);
    });
});
$(document).ready(function(){
    $(".navLinkAnchors2").click(function(){
        $(".navLinks2").toggle(500);
    });
});

$(document).ready(function(){
    $(".navLinkAnchors3").click(function(){
        $(".navLinks3").toggle(500);
    });
});

// Scroll to top
$(document).ready(function(){
  /******************************
      BOTTOM SCROLL TOP BUTTON
   ******************************/

  // declare variable
  var scrollTop = $(".scrollTop");

  $(window).scroll(function() {
    // declare variable
    var topPos = $(this).scrollTop();

    // if user scrolls down - show scroll to top button
    if (topPos > 100) {
      $(scrollTop).css("opacity", "1");

    } else {
      $(scrollTop).css("opacity", "0");
    }

  }); // scroll END

  //Click event to scroll to top
  $(scrollTop).click(function() {
    $('html, body').animate({
      scrollTop: 0
    }, 800);
    return false;

  }); // click() scroll top EMD
});


$(document).ready(function(){

$('html').attr("lang","en");

if($('body').hasClass('home_grid_slider')){
  setInterval(function(){
    if($('#sub_4_content').is(':visible')){
      $('.slide_icons').removeClass('active_icon');
      $('.slider_center_block').hide();
      $('.sc_content').hide();
      $("#sub_3_content").fadeIn();
      $("#s_c_3_content").fadeIn();
      $("#3_content").addClass("active_icon");
    } else if($('#sub_3_content').is(':visible')){
      $('.slide_icons').removeClass('active_icon');
      $('.slider_center_block').hide();
      $('.sc_content').hide();
      $("#sub_2_content").fadeIn();
      $("#s_c_2_content").fadeIn();
      $("#2_content").addClass("active_icon");
    } else if($('#sub_2_content').is(':visible')){
      $('.slide_icons').removeClass('active_icon');
      $('.slider_center_block').hide();
      $('.sc_content').hide();
      $("#sub_1_content").fadeIn();
      $("#s_c_1_content").fadeIn();
      $("#1_content").addClass("active_icon");
    } else if($('#sub_1_content').is(':visible')){
      $('.slide_icons').removeClass('active_icon');
      $('.slider_center_block').hide();
      $('.sc_content').hide();
      $("#sub_4_content").fadeIn();
      $("#s_c_4_content").fadeIn();
      $("#4_content").addClass("active_icon");
    }
  },5000);
}
// News items click

//onclick news items

$('.news_item').on('click', function(){
  var sub_id = $(this).attr('id');
  $(".news_title_content").css("display","none");
  $(".news_img").css("display","none");

  $("#news_"+sub_id).fadeIn( 1000 );
  $("#news_"+sub_id).animate({ opacity: 1 }, 1);
  $("#news_img_"+sub_id).fadeIn( 1000 );
  $("#news_img_"+sub_id).animate({ opacity: 1 }, 1);
});


var host_url = location.protocol + "//" + location.host;
$('#enquiry-submit').click(function(){
	var name = $('#index_name').val();
	var email = $('#index_email').val();
	var contact = $('#index_contact').val();
	var state = $('#index_state').val();
	var city = $('#index_city').val();
	var interest = $('#index_interest').val();
	var message = $('#index_message').val();
	var form_type=$('#index_form_type').val();
	var flag = 0;
	var EmailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
    var phonePattern = /^[0-9]*$/;

	if(name != '' && name!= null)
	{
		$('#index_name').removeClass('error');
	}
	else
	{
		$('#index_name').addClass('error');
		flag++;
	}

	if(EmailPattern.test(email) && email != '' && email!= null)
	{
		$('#index_email').removeClass('error');
	}
	else
	{
		$('#index_email').addClass('error');
		flag++;
	}

	if(phonePattern.test(contact) && contact != '' && contact!= null)
	{
		$('#index_contact').removeClass('error');
	}
	else
	{
		$('#index_contact').addClass('error');
		flag++;
	}

	if(state != '' && state!= null)
	{
		$('#index_state').removeClass('error');
	}
	else
	{
		$('#index_state').addClass('error');
		flag++;
	}

	if(city != '' && city!= null)
	{
		$('#index_city').removeClass('error');
	}
	else
	{
		$('#index_city').addClass('error');
		flag++;
	}

	if(interest != '' && interest!= null)
	{
		$('#index_interest').removeClass('error');
	}
	else
	{
		$('#index_interest').addClass('error');
		flag++;
	}

	if(message != '' && message!= null)
	{
		$('#index_message').removeClass('error');
	}
	else
	{
		$('#index_message').addClass('error');
		flag++;
	}

		if(flag==0)
        {
			$('#enquiry-submit').attr('disabled',true);
            $('#enq_msg').html('Please wait..');
        var ajax_data = {
                name : name,
                email : email,
                mobile : contact,
				state:state,
				city:city,
				intrested_in:interest,
				message:message,
				form_type:form_type,
            }
            $.ajax({
                type: "POST",
                url: '/modalenquiryform',
                data: ajax_data,
                success: function(result) {
                    if(result == 1)
					{
						$('#enquiry-submit').attr('disabled',false);

                        $('#enq_msg').html('Enquiry Placed Successfully');
                        setTimeout(location.reload(), 30000);
						
					}
					else
					{
						$('#enquiry-submit').attr('disabled',false);
                        $('#enq_msg').html('Error Occured');
					}

                },
                error: function() {
					$('#enquiry-submit').attr('disabled',false);
                    $('#enq_msg').html('Error Occured');
                }
            });
        }
});
$('#contact-enquiry-submit').click(function(){
	var name = $('#name').val();
	var email = $('#email').val();
	var contact = $('#contact').val();
	var state = $('#state').val();
	var city = $('#city').val();
	var interest = $('#interest').val();
	var message = $('#message').val();
	var form_type=$('#contact_form_type').val();
	var flag = 0;
	var EmailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
    var phonePattern = /^[0-9]*$/;

	if(name != '' && name!= null)
	{
		$('#name').removeClass('error');
	}
	else
	{
		$('#name').addClass('error');
		flag++;
	}

	if(EmailPattern.test(email) && email != '' && email!= null)
	{
		$('#email').removeClass('error');
	}
	else
	{
		$('#email').addClass('error');
		flag++;
	}

	if(phonePattern.test(contact) && contact != '' && contact!= null)
	{
		$('#contact').removeClass('error');
	}
	else
	{
		$('#contact').addClass('error');
		flag++;
	}

	if(state != '' && state!= null)
	{
		$('#state').removeClass('error');
	}
	else
	{
		$('#state').addClass('error');
		flag++;
	}

	if(city != '' && city!= null)
	{
		$('#city').removeClass('error');
	}
	else
	{
		$('#city').addClass('error');
		flag++;
	}

	if(interest != '' && interest!= null)
	{
		$('#interest').removeClass('error');
	}
	else
	{
		$('#interest').addClass('error');
		flag++;
	}

	if(message != '' && message!= null)
	{
		$('#message').removeClass('error');
	}
	else
	{
		$('#message').addClass('error');
		flag++;
	}

		if(flag==0)
        {
			$('#contact-enquiry-submit').attr('disabled',true);
            $('#enq_msg').html('Please wait..');
        var ajax_data = {
                name : name,
                email : email,
                mobile : contact,
				state:state,
				city:city,
				intrested_in:interest,
				message:message,
				form_type:form_type,
            }
            $.ajax({
                type: "POST",
                url: '/modalenquiryform',
                data: ajax_data,
                success: function(result) {
                    if(result == 1)
					{
						$('#contact-enquiry-submit').attr('disabled',false);

                        $('#enq_msg').html('Enquiry Placed Successfully');
						setTimeout(location.reload(), 30000);
					}
					else
					{
						$('#contact-enquiry-submit').attr('disabled',false);
                        $('#enq_msg').html('Error Occured');
					}

                },
                error: function() {
					$('#contact-enquiry-submit').attr('disabled',false);
                    $('#enq_msg').html('Error Occured');
                }
            });
        }
});


});

$(".click-button").click(function() {
        $('html,body').animate({
            scrollTop: $(".scroll-section").offset().top},
            'slow');

    });


$('.videos_item').on('click', function(){
  var sub_id = $(this).attr('id');
  $(".videos_title_content").css("display","none");
  $(".videos_img").css("display","none");

  $("#videos_"+sub_id).fadeIn( 1000 );
  $("#videos_"+sub_id).animate({ opacity: 1 }, 1);
  $("#videos_img_"+sub_id).fadeIn( 1000 );
  $("#videos_img_"+sub_id).animate({ opacity: 1 }, 1);
});

// favicn
$('head').append('<link rel="shortcut icon" href="images/favicon.png" type="image/png" sizes="32x32">');

// keywords
$('head').append('<meta name="keywords" content="Swipe machines, ONGO POS, mobile pos, GPRS machine, card swipe, accept card payment">');

// open graph object
$('head').append('<meta property="og:title" content=" India Transact | Transactions Made Simple">');
$('head').append('<meta property="og:url" content="https://www.indiatransact.com/">');
$('head').append('<meta property="og:description" content="India Transact Services Ltd. (ITSL), a digital payment arm of AGS Transact Technologies (AGSTTL), has introduced its first ‘SMART’ PoS solution. ">');
$('head').append('<meta property="og:image" content="https://www.indiatransact.com/images/logo.png">');



/**** form js ****/


$('#frm_submit').on('click', function(e){
	//alert('test');
	        e.preventDefault();

	        var name = $.trim($('#footer_name').val());
	        var email = $.trim($('#footer_email').val());
	        var mobile = $.trim($('#footer_mobile').val());
	        var state = $.trim($('#footer_state').val());
	        var utm_source = $.trim($('#footer_utm_source').val());
	   		var utm_campaign = $.trim($('#footer_utm_campaign').val());
	   		var utm_medium = $.trim($('#footer_utm_medium').val());
	   		var form_type = $.trim($('#footer_form_type').val());
	        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;
	        var mobilePattern = /^\d{10}$/;
	        var flag=0;

	        if(name=='' || name == null)
	        {
	            $('#footer_name').addClass('error');
	            flag++;
	        }
	        else
	        {
	            $('#footer_name').removeClass('error');
	        }


	        if(state=='' || state == null)
	        {
	            $('#footer_state').addClass('error');
	            flag++;
	        }
	        else
	        {
	            $('#footer_state').removeClass('error');
	        }

	        if (!emailPattern.test(email) || email == '' || email == null)
	        {
	            $('#footer_email').addClass('error');
	            flag++;
	        }
	        else
	        {
	            $('#footer_email').removeClass('error');
	        }

	        if (!mobilePattern.test(mobile) || mobile == '' || mobile == null)
	        {
	            $('#footer_mobile').addClass('error');
	            flag++;
	        }
	        else
	        {
	            $('#footer_mobile').removeClass('error');
	        }

	        if(flag==0)
	        {
	        	$('#frm_submit').attr('disabled',true);
	        	$('#frm_msg').val('Please wait...');
	            var data = {
	                      	email : email,
	                      	name:name,
	                      	mobile:mobile,
	                      	state:state,
	                      	utm_source:utm_source,
	             			utm_medium:utm_medium,
	             			utm_campaign:utm_campaign,
	             			form_type:form_type,
	                    }

	                    $.ajax({
	                    type : "POST",
	                    url : '/modalenquiryform',
	                    data : data,
	                    success : function(result) {
	                        if(result==1)
	                        {
	                        	$('#frm_submit').attr('disabled',false);
	                            $('#frm_msg').html('Thank you for contacting us');
	                            window.setTimeout(function(){window.location.href="/thankyou";}, 3000);
		                    }
	                        else if(result == 0)
	                        {
	                            $('#frm_submit').attr('disabled',false);
	                            $('#frm_msg').val('Something went wrong');
	                        }
	                    },
	                    error: function() {
	                        $('#frm_submit').attr('disabled',false);
	                        $('#frm_msg').val('Something went wrong');
	                    }
	                });
	        }

	    });
			


 AOS.init({
          easing: 'ease-in-out-sine'
        });
        $(document).ready(function(){
            $('.client_slider').owlCarousel({
                loop:true,
                margin:10,
                nav:false,
                autoplay:true,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:7
                    }
                }
            });


            $('.call_now').removeClass('open_box');
          $(".slide-toggle").click(function(){
            $('.call_now').toggleClass('open_box');
          });
        });
        $(window).scroll(function(){
          if ($(window).scrollTop() >= 10) {
            $('header').addClass('bg-color');
           }
           else {
            $('header').removeClass('bg-color');
           }
        });

//      setTimeout(function(){
//          //open enquiry form after 3 seconds
//          $('.call_now').addClass('open_box');
//      }, 3000);
//
//      setTimeout(function(){
//          //open enquiry form after 3 seconds
//          $('.call_now').removeClass('open_box');
//      }, 5000);


        // on particular div trigger counter

        var eventFired = false,
            objectPositionTop = $('#four-images').offset().top;

        $(window).on('scroll', function() {

         var currentPosition = $(document).scrollTop();
         if (currentPosition > objectPositionTop && eventFired === false) {
           eventFired = true;

               $('.count').each(function () {
                $(this).prop('Counter',0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 4000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });

         }

        });

//        /******* Form-popup *****/
//        $(window).on('load',function(){
//            setTimeout(function(){
//            $('#enq_form').modal('show');
//             }, 1000);
//        });
//
//        $(document).ready(function(){
//            $.exitIntent('enable');
//            $(document).bind('exitintent', function() {
//                $('#enq_form').modal('show');
//            });
//        });

        /******* Form-popup *****/
        // $(window).on('load',function(){
        //     setTimeout(function(){
        //     $('#enq_form').modal('show');
        //      }, 1000);
        // });

        // $(document).ready(function(){
        //     $.exitIntent('enable');
        //     $(document).bind('exitintent', function() {
        //         $('#enq_form').modal('show');
        //     });
        // });


// chatbox script that goes in head tag
// document.head.innerHTML += '<link href="https://snatchbot.me/sdk/webchat.css" rel="stylesheet" type="text/css">';
// document.head.innerHTML += '<script src="https://snatchbot.me/sdk/webchat.min.js">';
// document.head.innerHTML += "<script> Init('?botID=41134&appID=webchat', 600, 600, 'https://dvgpba5hywmpo.cloudfront.net/media/image/1cqvw5Z9Q2BgxWOw4CqMcqf6m', 'rounded', '#89C141', 90, 90, 62.99999999999999, '', '1', '#FFFFFF', '#FFFFFF', 0);";
// document.head.innerHTML += '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js">';
// document.head.innerHTML += '<style> div#sntchWebChat{top: auto;}</style>">';
