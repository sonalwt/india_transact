<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Careerform extends Model
{
    //
    //
     protected $table = 'careerforms';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'mobile','message','form_type','file'];
}
