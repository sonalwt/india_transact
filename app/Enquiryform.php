<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Enquiryform extends Model
{
    //
     protected $table = 'enquiry_form';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'mobile','state','city','intrested_in','message','utm_source','utm_medium','utm_campaign','form_type'];
}
