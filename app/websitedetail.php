<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class websitedetail extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */
        use  HasRoles;
       protected $table = 'websitedetails';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['logo', 'favicon', 'index_page_title', 'index_page_keyword', 'index_page_description', 'index_page_url', 'index_page_image', 'about_page_title', 'about_page_keyword', 'about_page_description', 'about_page_url', 'about_page_image', 'product_page_title', 'product_page_keyword', 'product_page_description', 'product_page_url', 'product_page_image', 'service_page_title', 'service_page_keyword', 'service_page_description', 'service_page_url', 'service_page_image', 'business_vertical_page_title', 'business_vertical_page_keyword', 'business_vertical_page_description', 'business_vertical_page_url', 'business_vertical_page_image', 'client_page_title', 'client_page_keyword', 'client_page_description', 'client_page_url', 'client_page_image', 'awards_page_title', 'awards_page_keyword', 'awards_page_description', 'awards_page_url', 'awards_page_image', 'news_page_title', 'news_page_keyword', 'news_page_description', 'news_page_url', 'news_page_image', 'contact_page_title', 'contact_page_keyword', 'contact_page_description', 'contact_page_url', 'contact_page_image','privacy_policy_page_title','privacy_policy_page_keyword','privacy_policy_page_description','privacy_policy_page_url','privacy_policy_page_image','disclaimer_policy_page_title','disclaimer_policy_page_keyword','disclaimer_policy_page_description','disclaimer_policy_page_url','disclaimer_policy_page_image','ewaste_policy_page_title','ewaste_policy_page_keyword','ewaste_policy_page_description','ewaste_policy_page_url','ewaste_policy_page_image'];

    
}
