<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Database\Eloquent\SoftDeletes;
class Businessvertical extends Model
{
        use SoftDeletes;
        /* The database table used by the model.
        *
        * @var string
        */
        use  HasRoles;
       protected $table = 'businessverticals';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['business_title', 'business_heading_id', 'business_description', 'business_icon', 'business_icon_selected', 'business_image', 'business_sort_order', 'business_status'];

    public function businessheadings()
    {
        return $this->belongsTo('App\Businessheading','business_heading_id','id');
    }
}
