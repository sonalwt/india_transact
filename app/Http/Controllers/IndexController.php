<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use App\Service;
use App\Businessheading;
use App\Clienttestomonial;
use App\Client;
use App\Awardyear;
use App\News;
use App\Homeoverview;
use App\Footprint;
use App\Keystrength;
use App\Homenews;
use App\Homeaward;
use App\Homevideo;
use App\Homeassociate;
use App\Hometransactionheading;
use App\Aboutusdetail;
use App\Aboutusvision;
use App\Boardofdirector;
use App\Managementteam;
use App\Pospayment;
use App\paymentsolution;
use App\Ongobusiness;
use App\Ongobilling;
use App\Loyaltyservice;
use App\Merchantservice;
use App\Reachus;
use App\Privacypolicy;
use App\Disclaimerpolicy;
use App\Ewastepolicy;
use App\Enquiryform;
use App\Enquiryformproduct;
use App\Collectioncentreaddress;
use App\Careerform;
use View;
use Session;
class IndexController extends Controller
{
    public function index(){
      $utm_source = Input::get('utm_source');
      Session::put('utm_source', $utm_source);
      $utm_medium = Input::get('utm_medium');
      Session::put('utm_medium', $utm_medium);
      $utm_campaign = Input::get('utm_campaign');
      Session::put('utm_campaign', $utm_campaign);
       $homeoverview=Homeoverview::first();
       $footprints=Footprint::where('counter_status',1)->get();
       $keystrengths=Keystrength::where('status',1)->get();
       $homenews=Homenews::where('status',1)->get();
       $homeawards=Homeaward::where('status',1)->get();
       $clients=Client::where('client_status',1)->orderBy('client_sort_order','ASC')->get();
       $homevideos=Homevideo::where('status',1)->get();
       $homeassociates=Homeassociate::where('status',1)->get();
       $hometransactionheading=Hometransactionheading::where('status',1)->first();
       $products=Enquiryformproduct::where('status',1)->get();
       return view('index',compact('homeoverview','footprints','keystrengths','homenews','homeawards','clients','homevideos','homeassociates','hometransactionheading','utm_source','utm_medium','utm_campaign','products'));
    }

    
    public function about(){
      $aboutusdetail=Aboutusdetail::first();
      $aboutusvision=Aboutusvision::first();
      $boardofdirectors=Boardofdirector::where('status',1)->get();
      $members=Managementteam::where('status',1)->get();
      return view('about',compact('aboutusdetail','aboutusvision','boardofdirectors','members'));
    }

    public function products(){
      $pospayment=Pospayment::first();
      $ongobusiness=Ongobusiness::first();
      $ongobilling=Ongobilling::first();
      $solutions=paymentsolution::where('status',1)->get();
      $loyaltyservices=Loyaltyservice::first();
      $merchantservices=Merchantservice::where('status',1)->get();
      return view('products',compact('pospayment','ongobusiness','solutions','loyaltyservices','ongobilling','merchantservices'));
    }

    public function contactus(){
       $products=Enquiryformproduct::where('status',1)->get();
      $reachus=Reachus::first();
      return view('contact',compact('reachus','products'));
    }
    public function services(){
        $services=Service::where('service_status',1)->get();
        return view('services',compact('services'));
    }

    public function businessverticals(){
        $businessheadings=Businessheading::where('business_heading_status','1')->get();
        
        return view('business_verticals',compact('businessheadings'));
    }

    public function clients(){
        $clienttestomonials=Clienttestomonial::where('clienttestomonials_status',1)->orderBy('clienttestomonials_sort_order','ASC')->get();
        $clients=Client::where('client_status',1)->orderBy('client_sort_order','ASC')->get();
        return view('client',compact('clienttestomonials','clients'));
    }

    public function awards(){
           $awardyears=Awardyear::where('year_status',1)->orderBy('id','desc')->get();
           return view('awards',compact('awardyears'));
    }

    public function news(){
            $news=News::where('news_status',1)->orderBy('news_date','desc')->get();
            return view('news',compact('news'));
    }

    public function privacypolicy(){
            $privacypolicy=Privacypolicy::first();
             return view('privacypolicy',compact('privacypolicy'));
    }

    public function disclaimerpolicy(){
            $disclaimerpolicy=Disclaimerpolicy::first();
            return view('disclaimerpolicy',compact('disclaimerpolicy'));
    }
    public function ewastepolicy(){
            $ewastepolicy=Ewastepolicy::first();
            $collectioncentreaddresses=Collectioncentreaddress::where('status',1)->get();
            return view('ewastepolicy',compact('ewastepolicy','collectioncentreaddresses'));
    }

    public function modalenquiryform(Request $request){
           $requestData = $request->all();
           $check=Enquiryform::create($requestData);

      if($check){
        if($requestData['form_type']=='index-form'){
          $products=Enquiryformproduct::where('id',$requestData['intrested_in'])->first();
         $data=['name'=>$requestData['name'],'email'=>$requestData['email'],'mobile'=>$requestData['mobile'],'message'=>$requestData['message'],'state'=>$requestData['state'],'city'=>$requestData['city'],'intrested_in'=>$products->title,'form_type'=>$requestData['form_type']];            
        
        View::share('data', $data);         
        try {
            Mail::send('mails.enquiry_email', $data, function ($message) use ($data) {
               $message->from('ongodigitalpayments@gmail.com', 'India Transact');
                $message->to(['business@indiatransact.com'])->subject('Enquiry Form');
            });
            echo 1;
        } catch (Exception $e) {
            dd($e);
        }
      }
      if($requestData['form_type']=='contact-form'){
          $products=Enquiryformproduct::where('id',$requestData['intrested_in'])->first();
         $data=['name'=>$requestData['name'],'email'=>$requestData['email'],'mobile'=>$requestData['mobile'],'message'=>$requestData['message'],'state'=>$requestData['state'],'city'=>$requestData['city'],'intrested_in'=>$products->title,'form_type'=>$requestData['form_type']];            
        
        View::share('data', $data);         
        try {
            Mail::send('mails.enquiry_email', $data, function ($message) use ($data) {
               $message->from('ongodigitalpayments@gmail.com', 'India Transact');
                $message->to(['business@indiatransact.com'])->subject('Enquiry Form');
            });
            echo 1;
        } catch (Exception $e) {
            dd($e);
        }
      }
      if($requestData['form_type']=='pop-up-form'){
         $data=['name'=>$requestData['name'],'email'=>$requestData['email'],'mobile'=>$requestData['mobile'],'state'=>$requestData['state'],'utm_source'=>$requestData['utm_source'],'utm_medium'=>$requestData['utm_medium'],'form_type'=>$requestData['form_type'],'utm_campaign'=>$requestData['utm_campaign']];            
        
        View::share('data', $data);         
        try {
            Mail::send('mails.enquiry_email', $data, function ($message) use ($data) {
                $message->from('ongodigitalpayments@gmail.com', 'India Transact');
                $message->to(['business@indiatransact.com'])->subject('Enquiry Form');
            });
            echo 1;
        } catch (Exception $e) {
           dd($e);
        }
      }
      if($requestData['form_type']=='footer-form'){
         $data=['name'=>$requestData['name'],'email'=>$requestData['email'],'mobile'=>$requestData['mobile'],'state'=>$requestData['state'],'utm_source'=>$requestData['utm_source'],'utm_medium'=>$requestData['utm_medium'],'form_type'=>$requestData['form_type'],'utm_campaign'=>$requestData['utm_campaign']];            
        
        View::share('data', $data);         
        try {
            Mail::send('mails.enquiry_email', $data, function ($message) use ($data) {
                $message->from('ongodigitalpayments@gmail.com', 'India Transact');
                $message->to(['business@indiatransact.com'])->subject('Enquiry Form');
            });
            echo 1;
        } catch (Exception $e) {
           dd($e);
        }
      }
       
      } else{
        echo 0;
      }

    }
    public function careerform(Request $request){
      $document_path="";
      $requestData = $request->all();
        if ($request->hasFile('file')) {
                    $icon=mt_rand();
                    $files = $request->file('file');
                    $path = 'assets/docs/resume';
                    $file = $request->file('file');
                    $files->move($path,$file->getClientOriginalName());

                    $requestData['file'] ='/assets/docs/resume/'.$file->getClientOriginalName();   

                    $document_path = $path.'/'. $file->getClientOriginalName();    
        }
        
           $check=Careerform::create($requestData);

      if($check){
         $data=['file'=>$requestData['file'],'doc_path'=>$document_path ,'name'=>$requestData['name'],'email'=>$requestData['email'],'mobile'=>$requestData['mobile'],'message'=>$requestData['message'],'form_type'=>$requestData['form_type']];            
        
        View::share('data', $data);         
        try {
            Mail::send('mails.career_mail', $data, function ($message) use ($data) {
                 $message->from('ongodigitalpayments@gmail.com', 'India Transact');
                $message->to(['careers@agsindia.com'])->subject('Enquiry Form')->attach( $data['doc_path'] );
            });
            echo 1;
        } catch (Exception $e) {
           dd($e);
        }
        
      } else{
        echo 0;
      }
    }

    public function thankyou(){
      return view('thankyou');
    }
}
