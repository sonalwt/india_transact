<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Submenu;
use App\Menu;
use Illuminate\Http\Request;
use App\Authorizable;

class SubmenusController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $submenus = Submenu::where('sub_menu', 'LIKE', "%$keyword%")
                ->orWhere('menu_id', 'LIKE', "%$keyword%")
                ->orWhere('submenu_link', 'LIKE', "%$keyword%")
                ->orWhere('submenu_sort_order', 'LIKE', "%$keyword%")
                ->orWhere('submenu_status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $submenus = Submenu::latest()->paginate($perPage);
        }

        return view('admin.submenus.index', compact('submenus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $menus=Menu::where('menu_status','1')->get();
        return view('admin.submenus.create',compact('menus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'sub_menu' => 'required'
		]);
        $requestData = $request->all();
        
        Submenu::create($requestData);

        return redirect('admin/submenus')->with('flash_message', 'Submenu added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        
        $submenu = Submenu::findOrFail($id);

        return view('admin.submenus.show', compact('submenu'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $menus=Menu::where('menu_status','1')->get();
        $submenu = Submenu::findOrFail($id);

        return view('admin.submenus.edit', compact('submenu','menus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'sub_menu' => 'required'
		]);
        $requestData = $request->all();
        
        $submenu = Submenu::findOrFail($id);
        $submenu->update($requestData);

        return redirect('admin/submenus')->with('flash_message', 'Submenu updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Submenu::destroy($id);

        return redirect('admin/submenus')->with('flash_message', 'Submenu deleted!');
    }
}
