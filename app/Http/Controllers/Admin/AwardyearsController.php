<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Awardyear;
use Illuminate\Http\Request;
use App\Authorizable;

class AwardyearsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $awardyears = Awardyear::where('year', 'LIKE', "%$keyword%")
                ->orWhere('year_status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $awardyears = Awardyear::latest()->paginate($perPage);
        }

        return view('admin.awardyears.index', compact('awardyears'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.awardyears.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'year' => 'required',
			'year_status' => 'required'
		]);
        $requestData = $request->all();
        
        Awardyear::create($requestData);

        return redirect('admin/awardyears')->with('flash_message', 'Award year added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $awardyear = Awardyear::findOrFail($id);

        return view('admin.awardyears.show', compact('awardyear'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $awardyear = Awardyear::findOrFail($id);

        return view('admin.awardyears.edit', compact('awardyear'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'year' => 'required',
			'year_status' => 'required'
		]);
        $requestData = $request->all();
        
        $awardyear = Awardyear::findOrFail($id);
        $awardyear->update($requestData);

        return redirect('admin/awardyears')->with('flash_message', 'Award year updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Awardyear::destroy($id);

        return redirect('admin/awardyears')->with('flash_message', 'Award year deleted!');
    }
}
