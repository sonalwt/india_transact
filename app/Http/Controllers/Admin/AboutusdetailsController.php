<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Aboutusdetail;
use Illuminate\Http\Request;
use App\Authorizable;

class AboutusdetailsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $aboutusdetails = Aboutusdetail::where('heading', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $aboutusdetails = Aboutusdetail::latest()->paginate($perPage);
        }

        return view('admin.aboutusdetails.index', compact('aboutusdetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.aboutusdetails.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'heading' => 'required',
			'description' => 'required',
			'image' => 'required'
		]);
        $requestData = $request->all();
         if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/about'), $filename);
                    $requestData['image'] ='/assets/images/about/'.$filename;

        }

        Aboutusdetail::create($requestData);

        return redirect('admin/aboutusdetails')->with('flash_message', 'About us detail added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $aboutusdetail = Aboutusdetail::findOrFail($id);

        return view('admin.aboutusdetails.show', compact('aboutusdetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $aboutusdetail = Aboutusdetail::findOrFail($id);

        return view('admin.aboutusdetails.edit', compact('aboutusdetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'heading' => 'required',
			'description' => 'required'
		]);
        $requestData = $request->all();
       if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/about'), $filename);
                    $requestData['image'] ='/assets/images/about/'.$filename;

        }

        $aboutusdetail = Aboutusdetail::findOrFail($id);
        $aboutusdetail->update($requestData);

        return redirect('admin/aboutusdetails/1/edit')->with('flash_message', 'About us detail updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Aboutusdetail::destroy($id);

        return redirect('admin/aboutusdetails')->with('flash_message', 'About us detail deleted!');
    }
}
