<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\News;
use Illuminate\Http\Request;
use App\Authorizable;

class NewsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $news = News::where('news_title', 'LIKE', "%$keyword%")
                ->orWhere('news_date', 'LIKE', "%$keyword%")
                ->orWhere('news_file', 'LIKE', "%$keyword%")
                ->orWhere('news_status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $news = News::latest()->paginate($perPage);
        }

        return view('admin.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'news_title' => 'required',
            'news_file' => 'required',
			'news_status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('news_file')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('news_file')->getClientOriginalName();
                    $request->news_file->move(base_path('public/assets/pr-docs'), $filename);
                    $requestData['news_file'] ='/assets/pr-docs/'.$filename;

        }
        News::create($requestData);

        return redirect('admin/news')->with('flash_message', 'News added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $news = News::findOrFail($id);

        return view('admin.news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $news = News::findOrFail($id);

        return view('admin.news.edit', compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'news_title' => 'required',
			'news_status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('news_file')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('news_file')->getClientOriginalName();
                    $request->news_file->move(base_path('public/assets/pr-docs'), $filename);
                    $requestData['news_file'] ='/assets/pr-docs/'.$filename;

        }
        $news = News::findOrFail($id);
        $news->update($requestData);

        return redirect('admin/news')->with('flash_message', 'News updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        News::destroy($id);

        return redirect('admin/news')->with('flash_message', 'News deleted!');
    }
}
