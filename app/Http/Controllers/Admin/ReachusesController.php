<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Reachus;
use Illuminate\Http\Request;
use App\Authorizable;

class ReachusesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $reachuses = Reachus::where('map', 'LIKE', "%$keyword%")
                ->orWhere('registered_office', 'LIKE', "%$keyword%")
                ->orWhere('office_address', 'LIKE', "%$keyword%")
                ->orWhere('for_business_related_queries', 'LIKE', "%$keyword%")
                ->orWhere('for_media_related_queries', 'LIKE', "%$keyword%")
                ->orWhere('health_and_support', 'LIKE', "%$keyword%")
                ->orWhere('work_with_us', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $reachuses = Reachus::latest()->paginate($perPage);
        }

        return view('admin.reachuses.index', compact('reachuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.reachuses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'map' => 'required',
			'registered_office' => 'required',
			'office_address' => 'required',
			'for_business_related_queries' => 'required',
			'for_media_related_queries' => 'required',
			'health_and_support' => 'required',
			'work_with_us' => 'required'
		]);
        $requestData = $request->all();
        
        Reachus::create($requestData);

        return redirect('admin/reachuses')->with('flash_message', 'Reachus added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $reachus = Reachus::findOrFail($id);

        return view('admin.reachuses.show', compact('reachus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $reachus = Reachus::findOrFail($id);

        return view('admin.reachuses.edit', compact('reachus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'map' => 'required',
			'registered_office' => 'required',
			'office_address' => 'required',
			'for_business_related_queries' => 'required',
			'for_media_related_queries' => 'required',
			'health_and_support' => 'required',
			'work_with_us' => 'required'
		]);
        $requestData = $request->all();
        
        $reachus = Reachus::findOrFail($id);
        $reachus->update($requestData);

        return redirect('admin/reachuses/1/edit')->with('flash_message', 'Reach us updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Reachus::destroy($id);

        return redirect('admin/reachuses')->with('flash_message', 'Reachus deleted!');
    }
}
