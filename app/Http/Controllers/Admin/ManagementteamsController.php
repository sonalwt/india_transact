<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Managementteam;
use Illuminate\Http\Request;
use App\Authorizable;

class ManagementteamsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $managementteams = Managementteam::where('name', 'LIKE', "%$keyword%")
                ->orWhere('designation', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $managementteams = Managementteam::latest()->paginate($perPage);
        }

        return view('admin.managementteams.index', compact('managementteams'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.managementteams.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required',
			'designation' => 'required',
			'image' => 'required',
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
         if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/about'), $filename);
                    $requestData['image'] ='/assets/images/about/'.$filename;

        }

        Managementteam::create($requestData);

        return redirect('admin/managementteams')->with('flash_message', 'Management team added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $managementteam = Managementteam::findOrFail($id);

        return view('admin.managementteams.show', compact('managementteam'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $managementteam = Managementteam::findOrFail($id);

        return view('admin.managementteams.edit', compact('managementteam'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'designation' => 'required',
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
         if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/about'), $filename);
                    $requestData['image'] ='/assets/images/about/'.$filename;

        }

        $managementteam = Managementteam::findOrFail($id);
        $managementteam->update($requestData);

        return redirect('admin/managementteams')->with('flash_message', 'Management team updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Managementteam::destroy($id);

        return redirect('admin/managementteams')->with('flash_message', 'Management team member deleted!');
    }
}
