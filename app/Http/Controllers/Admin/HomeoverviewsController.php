<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Homeoverview;
use Illuminate\Http\Request;
use App\Authorizable;

class HomeoverviewsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $homeoverviews = Homeoverview::where('headings', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $homeoverviews = Homeoverview::latest()->paginate($perPage);
        }

        return view('admin.homeoverviews.index', compact('homeoverviews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.homeoverviews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'headings' => 'required',
			'description' => 'required',
			'image' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images'), $filename);
                    $requestData['image'] ='/assets/images/'.$filename;

        }

        Homeoverview::create($requestData);

        return redirect('admin/homeoverviews')->with('flash_message', 'Home overview added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $homeoverview = Homeoverview::findOrFail($id);

        return view('admin.homeoverviews.show', compact('homeoverview'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $homeoverview = Homeoverview::findOrFail($id);

        return view('admin.homeoverviews.edit', compact('homeoverview'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'headings' => 'required',
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
       if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/'), $filename);
                    $requestData['image'] ='/assets/images/'.$filename;

        }

        $homeoverview = Homeoverview::findOrFail($id);
        $homeoverview->update($requestData);

        return redirect('admin/homeoverviews/1/edit')->with('flash_message', 'Home Overview Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Homeoverview::destroy($id);

        return redirect('admin/homeoverviews')->with('flash_message', 'Home overview deleted!');
    }
}
