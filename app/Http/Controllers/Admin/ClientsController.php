<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Client;
use Illuminate\Http\Request;
use App\Authorizable;

class ClientsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $clients = Client::where('client_name', 'LIKE', "%$keyword%")
                ->orWhere('client_logo', 'LIKE', "%$keyword%")
                ->orWhere('client_sort_order', 'LIKE', "%$keyword%")
                ->orWhere('client_status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $clients = Client::latest()->paginate($perPage);
        }

        return view('admin.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.clients.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'client_name' => 'required',
			'client_logo' => 'required',
			'client_sort_order' => 'required',
			'client_status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('client_logo')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('client_logo')->getClientOriginalName();
                    $request->client_logo->move(base_path('public/assets/images/clients'), $filename);
                    $requestData['client_logo'] ='/assets/images/clients/'.$filename;

        }

        Client::create($requestData);

        return redirect('admin/clients')->with('flash_message', 'Client added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $client = Client::findOrFail($id);

        return view('admin.clients.show', compact('client'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $client = Client::findOrFail($id);

        return view('admin.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'client_name' => 'required',
			
			'client_sort_order' => 'required',
			'client_status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('client_logo')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('client_logo')->getClientOriginalName();
                    $request->client_logo->move(base_path('public/assets/images/clients'), $filename);
                    $requestData['client_logo'] ='/assets/images/clients/'.$filename;

        }

        $client = Client::findOrFail($id);
        $client->update($requestData);

        return redirect('admin/clients')->with('flash_message', 'Client updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Client::destroy($id);

        return redirect('admin/clients')->with('flash_message', 'Client deleted!');
    }
}
