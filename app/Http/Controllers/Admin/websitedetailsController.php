<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\websitedetail;
use Illuminate\Http\Request;
use App\Authorizable;

class websitedetailsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $websitedetails = websitedetail::where('logo', 'LIKE', "%$keyword%")
                ->orWhere('favicon', 'LIKE', "%$keyword%")
                ->orWhere('index_page_title', 'LIKE', "%$keyword%")
                ->orWhere('index_page_keyword', 'LIKE', "%$keyword%")
                ->orWhere('index_page_description', 'LIKE', "%$keyword%")
                ->orWhere('index_page_url', 'LIKE', "%$keyword%")
                ->orWhere('index_page_image', 'LIKE', "%$keyword%")
                ->orWhere('about_page_title', 'LIKE', "%$keyword%")
                ->orWhere('about_page_keyword', 'LIKE', "%$keyword%")
                ->orWhere('about_page_description', 'LIKE', "%$keyword%")
                ->orWhere('about_page_url', 'LIKE', "%$keyword%")
                ->orWhere('about_page_image', 'LIKE', "%$keyword%")
                ->orWhere('product_page_title', 'LIKE', "%$keyword%")
                ->orWhere('product_page_keyword', 'LIKE', "%$keyword%")
                ->orWhere('product_page_description', 'LIKE', "%$keyword%")
                ->orWhere('product_page_url', 'LIKE', "%$keyword%")
                ->orWhere('product_page_image', 'LIKE', "%$keyword%")
                ->orWhere('service_page_title', 'LIKE', "%$keyword%")
                ->orWhere('service_page_keyword', 'LIKE', "%$keyword%")
                ->orWhere('service_page_description', 'LIKE', "%$keyword%")
                ->orWhere('service_page_url', 'LIKE', "%$keyword%")
                ->orWhere('service_page_image', 'LIKE', "%$keyword%")
                ->orWhere('business_vertical_page_title', 'LIKE', "%$keyword%")
                ->orWhere('business_vertical_page_keyword', 'LIKE', "%$keyword%")
                ->orWhere('business_vertical_page_description', 'LIKE', "%$keyword%")
                ->orWhere('business_vertical_page_url', 'LIKE', "%$keyword%")
                ->orWhere('business_vertical_page_image', 'LIKE', "%$keyword%")
                ->orWhere('client_page_title', 'LIKE', "%$keyword%")
                ->orWhere('client_page_keyword', 'LIKE', "%$keyword%")
                ->orWhere('client_page_description', 'LIKE', "%$keyword%")
                ->orWhere('client_page_url', 'LIKE', "%$keyword%")
                ->orWhere('client_page_image', 'LIKE', "%$keyword%")
                ->orWhere('awards_page_title', 'LIKE', "%$keyword%")
                ->orWhere('awards_page_keyword', 'LIKE', "%$keyword%")
                ->orWhere('awards_page_description', 'LIKE', "%$keyword%")
                ->orWhere('awards_page_url', 'LIKE', "%$keyword%")
                ->orWhere('awards_page_image', 'LIKE', "%$keyword%")
                ->orWhere('news_page_title', 'LIKE', "%$keyword%")
                ->orWhere('news_page_keyword', 'LIKE', "%$keyword%")
                ->orWhere('news_page_description', 'LIKE', "%$keyword%")
                ->orWhere('news_page_url', 'LIKE', "%$keyword%")
                ->orWhere('news_page_image', 'LIKE', "%$keyword%")
                ->orWhere('contact_page_title', 'LIKE', "%$keyword%")
                ->orWhere('contact_page_keyword', 'LIKE', "%$keyword%")
                ->orWhere('contact_page_description', 'LIKE', "%$keyword%")
                ->orWhere('contact_page_url', 'LIKE', "%$keyword%")
                ->orWhere('contact_page_image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $websitedetails = websitedetail::latest()->paginate($perPage);
        }

        return view('admin.websitedetails.index', compact('websitedetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.websitedetails.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'index_page_title' => 'required',
			'index_page_keyword' => 'required',
			'index_page_description' => 'required'
		]);
        $requestData = $request->all();
         if ($request->hasFile('logo')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('logo')->getClientOriginalName();
                    $request->logo->move(base_path('public/assets/images'), $filename);
                    $requestData['logo'] ='/assets/images/'.$filename;
        }
         if ($request->hasFile('favicon')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('favicon')->getClientOriginalName();
                    $request->favicon->move(base_path('public/assets/images'), $filename);
                    $requestData['favicon'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('index_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('index_page_image')->getClientOriginalName();
                    $request->index_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['index_page_image'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('about_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('about_page_image')->getClientOriginalName();
                    $request->about_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['about_page_image'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('product_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('product_page_image')->getClientOriginalName();
                    $request->product_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['product_page_image'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('service_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('service_page_image')->getClientOriginalName();
                    $request->service_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['service_page_image'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('business_vertical_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('business_vertical_page_image')->getClientOriginalName();
                    $request->business_vertical_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['business_vertical_page_image'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('client_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('client_page_image')->getClientOriginalName();
                    $request->client_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['client_page_image'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('awards_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('awards_page_image')->getClientOriginalName();
                    $request->awards_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['awards_page_image'] ='/assets/images/'.$filename;
        }
        
        if ($request->hasFile('news_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('news_page_image')->getClientOriginalName();
                    $request->news_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['news_page_image'] ='/assets/images/'.$filename;
        }
        
        if ($request->hasFile('contact_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('contact_page_image')->getClientOriginalName();
                    $request->contact_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['contact_page_image'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('privacy_policy_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('privacy_policy_page_image')->getClientOriginalName();
                    $request->privacy_policy_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['privacy_policy_page_image'] ='/assets/images/'.$filename;
        }
        
        if ($request->hasFile('disclaimer_policy_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('disclaimer_policy_page_image')->getClientOriginalName();
                    $request->disclaimer_policy_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['disclaimer_policy_page_image'] ='/assets/images/'.$filename;
        }
        
        if ($request->hasFile('ewaste_policy_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('ewaste_policy_page_image')->getClientOriginalName();
                    $request->ewaste_policy_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['ewaste_policy_page_image'] ='/assets/images/'.$filename;
        }        


        websitedetail::create($requestData);

        return redirect('admin/websitedetails/1/edit')->with('flash_message', 'Website Detail added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $websitedetail = websitedetail::findOrFail($id);

        return view('admin.websitedetails.show', compact('websitedetail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $websitedetail = websitedetail::findOrFail($id);

        return view('admin.websitedetails.edit', compact('websitedetail'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'index_page_title' => 'required',
			'index_page_keyword' => 'required',
			'index_page_description' => 'required'
		]);
        $requestData = $request->all();
       if ($request->hasFile('logo')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('logo')->getClientOriginalName();
                    $request->logo->move(base_path('public/assets/images'), $filename);
                    $requestData['logo'] ='/assets/images/'.$filename;
        }
         if ($request->hasFile('favicon')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('favicon')->getClientOriginalName();
                    $request->favicon->move(base_path('public/assets/images'), $filename);
                    $requestData['favicon'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('index_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('index_page_image')->getClientOriginalName();
                    $request->index_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['index_page_image'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('about_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('about_page_image')->getClientOriginalName();
                    $request->about_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['about_page_image'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('product_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('product_page_image')->getClientOriginalName();
                    $request->product_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['product_page_image'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('service_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('service_page_image')->getClientOriginalName();
                    $request->service_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['service_page_image'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('business_vertical_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('business_vertical_page_image')->getClientOriginalName();
                    $request->business_vertical_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['business_vertical_page_image'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('client_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('client_page_image')->getClientOriginalName();
                    $request->client_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['client_page_image'] ='/assets/images/'.$filename;
        }
        if ($request->hasFile('awards_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('awards_page_image')->getClientOriginalName();
                    $request->awards_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['awards_page_image'] ='/assets/images/'.$filename;
        }
        
        if ($request->hasFile('news_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('news_page_image')->getClientOriginalName();
                    $request->news_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['news_page_image'] ='/assets/images/'.$filename;
        }
        
        if ($request->hasFile('contact_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('contact_page_image')->getClientOriginalName();
                    $request->contact_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['contact_page_image'] ='/assets/images/'.$filename;
        }
        
         if ($request->hasFile('privacy_policy_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('privacy_policy_page_image')->getClientOriginalName();
                    $request->privacy_policy_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['privacy_policy_page_image'] ='/assets/images/'.$filename;
        }
        
        if ($request->hasFile('disclaimer_policy_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('disclaimer_policy_page_image')->getClientOriginalName();
                    $request->disclaimer_policy_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['disclaimer_policy_page_image'] ='/assets/images/'.$filename;
        }
        
        if ($request->hasFile('ewaste_policy_page_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('ewaste_policy_page_image')->getClientOriginalName();
                    $request->ewaste_policy_page_image->move(base_path('public/assets/images'), $filename);
                    $requestData['ewaste_policy_page_image'] ='/assets/images/'.$filename;
        }   
        $websitedetail = websitedetail::findOrFail($id);
        $websitedetail->update($requestData);

        return redirect('admin/websitedetails/1/edit')->with('flash_message', 'Website Detail updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        websitedetail::destroy($id);

        return redirect('admin/websitedetails')->with('flash_message', 'websitedetail deleted!');
    }
}
