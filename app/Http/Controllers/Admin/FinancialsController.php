<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Financial;
use Illuminate\Http\Request;
use App\Authorizable;

class FinancialsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $financials = Financial::where('title', 'LIKE', "%$keyword%")
                ->orWhere('file', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $financials = Financial::latest()->paginate($perPage);
        }

        return view('admin.financials.index', compact('financials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.financials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
         if ($request->hasFile('file')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('file')->getClientOriginalName();
                    $request->file->move(base_path('public/assets/docs'), $filename);
                    $requestData['file'] ='/assets/docs/'.$filename;

        }

        Financial::create($requestData);

        return redirect('admin/financials')->with('flash_message', 'Financial added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $financial = Financial::findOrFail($id);

        return view('admin.financials.show', compact('financial'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $financial = Financial::findOrFail($id);

        return view('admin.financials.edit', compact('financial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'file' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('file')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('file')->getClientOriginalName();
                    $request->file->move(base_path('public/assets/docs'), $filename);
                    $requestData['file'] ='/assets/docs/'.$filename;

        }

        $financial = Financial::findOrFail($id);
        $financial->update($requestData);

        return redirect('admin/financials')->with('flash_message', 'Financial updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Financial::destroy($id);

        return redirect('admin/financials')->with('flash_message', 'Financial deleted!');
    }
}
