<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Footerlink;
use Illuminate\Http\Request;
use App\Authorizable;

class FooterlinksController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $footerlinks = Footerlink::where('facebook', 'LIKE', "%$keyword%")
                ->orWhere('twitter', 'LIKE', "%$keyword%")
                ->orWhere('instagram', 'LIKE', "%$keyword%")
                ->orWhere('linkedin', 'LIKE', "%$keyword%")
                ->orWhere('youtube', 'LIKE', "%$keyword%")
                ->orWhere('ongomerchant', 'LIKE', "%$keyword%")
                ->orWhere('ongopaytrack', 'LIKE', "%$keyword%")
                ->orWhere('ongobilling', 'LIKE', "%$keyword%")
                ->orWhere('ongoqr', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $footerlinks = Footerlink::latest()->paginate($perPage);
        }

        return view('admin.footerlinks.index', compact('footerlinks'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.footerlinks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'facebook' => 'required',
			'twitter' => 'required',
			'instagram' => 'required',
			'linkedin' => 'required',
			'youtube' => 'required',
			'ongomerchant' => 'required',
			'ongopaytrack' => 'required',
			'ongobilling' => 'required',
			'ongoqr' => 'required'
		]);
        $requestData = $request->all();
        
        Footerlink::create($requestData);

        return redirect('admin/footerlinks')->with('flash_message', 'Footerlink added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $footerlink = Footerlink::findOrFail($id);

        return view('admin.footerlinks.show', compact('footerlink'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $footerlink = Footerlink::findOrFail($id);

        return view('admin.footerlinks.edit', compact('footerlink'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'facebook' => 'required',
			'twitter' => 'required',
			'instagram' => 'required',
			'linkedin' => 'required',
			'youtube' => 'required',
			'ongomerchant' => 'required',
			'ongopaytrack' => 'required',
			'ongobilling' => 'required',
			'ongoqr' => 'required'
		]);
        $requestData = $request->all();
        
        $footerlink = Footerlink::findOrFail($id);
        $footerlink->update($requestData);

        return redirect('admin/footerlinks/1/edit')->with('flash_message', 'Footerlink updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Footerlink::destroy($id);

        return redirect('admin/footerlinks')->with('flash_message', 'Footerlink deleted!');
    }
}
