<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Ewastepolicy;
use Illuminate\Http\Request;
use App\Authorizable;

class EwastepoliciesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $ewastepolicies = Ewastepolicy::where('title', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $ewastepolicies = Ewastepolicy::latest()->paginate($perPage);
        }

        return view('admin.ewastepolicies.index', compact('ewastepolicies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.ewastepolicies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'description' => 'required'
		]);
        $requestData = $request->all();
        
        Ewastepolicy::create($requestData);

        return redirect('admin/ewastepolicies')->with('flash_message', 'E-waste policy added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $ewastepolicy = Ewastepolicy::findOrFail($id);

        return view('admin.ewastepolicies.show', compact('ewastepolicy'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $ewastepolicy = Ewastepolicy::findOrFail($id);

        return view('admin.ewastepolicies.edit', compact('ewastepolicy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'description' => 'required'
		]);
        $requestData = $request->all();
        
        $ewastepolicy = Ewastepolicy::findOrFail($id);
        $ewastepolicy->update($requestData);

        return redirect('admin/ewastepolicies/1/edit')->with('flash_message', 'E-waste Policy updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Ewastepolicy::destroy($id);

        return redirect('admin/ewastepolicies')->with('flash_message', 'E-waste policy deleted!');
    }
}
