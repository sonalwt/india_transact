<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Disclaimerpolicy;
use Illuminate\Http\Request;
use App\Authorizable;

class DisclaimerpoliciesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $disclaimerpolicies = Disclaimerpolicy::where('title', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $disclaimerpolicies = Disclaimerpolicy::latest()->paginate($perPage);
        }

        return view('admin.disclaimerpolicies.index', compact('disclaimerpolicies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.disclaimerpolicies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'description' => 'required'
		]);
        $requestData = $request->all();
        
        Disclaimerpolicy::create($requestData);

        return redirect('admin/disclaimerpolicies')->with('flash_message', 'Disclaimer policy added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $disclaimerpolicy = Disclaimerpolicy::findOrFail($id);

        return view('admin.disclaimerpolicies.show', compact('disclaimerpolicy'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $disclaimerpolicy = Disclaimerpolicy::findOrFail($id);

        return view('admin.disclaimerpolicies.edit', compact('disclaimerpolicy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'description' => 'required'
		]);
        $requestData = $request->all();
        
        $disclaimerpolicy = Disclaimerpolicy::findOrFail($id);
        $disclaimerpolicy->update($requestData);

        return redirect('admin/disclaimerpolicies/1/edit')->with('flash_message', 'Disclaimer Policy updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Disclaimerpolicy::destroy($id);

        return redirect('admin/disclaimerpolicies')->with('flash_message', 'Disclaimer policy deleted!');
    }
}
