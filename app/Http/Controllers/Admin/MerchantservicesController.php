<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Merchantservice;
use Illuminate\Http\Request;
use App\Authorizable;

class MerchantservicesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $merchantservices = Merchantservice::where('title1', 'LIKE', "%$keyword%")
                ->orWhere('title2', 'LIKE', "%$keyword%")
                ->orWhere('main_image', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $merchantservices = Merchantservice::latest()->paginate($perPage);
        }

        return view('admin.merchantservices.index', compact('merchantservices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.merchantservices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title1' => 'required',
			'title2' => 'required',
			'main_image' => 'required',
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
         if ($request->hasFile('main_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('main_image')->getClientOriginalName();
                    $request->main_image->move(base_path('public/assets/images/products'), $filename);
                    $requestData['main_image'] ='/assets/images/products/'.$filename;

        }

        Merchantservice::create($requestData);

        return redirect('admin/merchantservices')->with('flash_message', 'Merchant service added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $merchantservice = Merchantservice::findOrFail($id);

        return view('admin.merchantservices.show', compact('merchantservice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $merchantservice = Merchantservice::findOrFail($id);

        return view('admin.merchantservices.edit', compact('merchantservice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title1' => 'required',
			'title2' => 'required',
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
         if ($request->hasFile('main_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('main_image')->getClientOriginalName();
                    $request->main_image->move(base_path('public/assets/images/products'), $filename);
                    $requestData['main_image'] ='/assets/images/products/'.$filename;

        }

        $merchantservice = Merchantservice::findOrFail($id);
        $merchantservice->update($requestData);

        return redirect('admin/merchantservices')->with('flash_message', 'Merchant service updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Merchantservice::destroy($id);

        return redirect('admin/merchantservices')->with('flash_message', 'Merchant service deleted!');
    }
}
