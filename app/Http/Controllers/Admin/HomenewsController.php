<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Homenews;
use Illuminate\Http\Request;
use App\Authorizable;

class HomenewsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $homenews = Homenews::where('title', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('main_image', 'LIKE', "%$keyword%")
                ->orWhere('icon_image', 'LIKE', "%$keyword%")
                ->orWhere('location', 'LIKE', "%$keyword%")
                ->orWhere('date', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $homenews = Homenews::latest()->paginate($perPage);
        }

        return view('admin.homenews.index', compact('homenews'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.homenews.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'description' => 'required',
			'main_image' => 'required',
			'icon_image' => 'required',
            'news_file' => 'required',
			'location' => 'required',
			'date' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();

        if ($request->hasFile('main_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('main_image')->getClientOriginalName();
                    $request->main_image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['main_image'] ='/assets/images/home/'.$filename;

        }
        if ($request->hasFile('icon_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('icon_image')->getClientOriginalName();
                    $request->icon_image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['icon_image'] ='/assets/images/home/'.$filename;

        }
        if ($request->hasFile('news_file')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('news_file')->getClientOriginalName();
                    $request->news_file->move(base_path('public/assets/images/home'), $filename);
                    $requestData['news_file'] ='/assets/images/home/'.$filename;

        }

        $h=Homenews::create($requestData);
         dd($h);

        return redirect('admin/homenews')->with('flash_message', 'Home news added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $homenews = Homenews::findOrFail($id);

        return view('admin.homenews.show', compact('homenews'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $homenews = Homenews::findOrFail($id);

        return view('admin.homenews.edit', compact('homenews'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'description' => 'required',
			'location' => 'required',
			'date' => 'required',
			'status' => 'required'
		]);
         $requestData = $request->all();
        if ($request->hasFile('main_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('main_image')->getClientOriginalName();
                    $request->main_image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['main_image'] ='/assets/images/home/'.$filename;

        }
        if ($request->hasFile('icon_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('icon_image')->getClientOriginalName();
                    $request->icon_image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['icon_image'] ='/assets/images/home/'.$filename;

        }
        if ($request->hasFile('news_file')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('news_file')->getClientOriginalName();
                    $request->news_file->move(base_path('public/assets/images/home'), $filename);
                    $requestData['news_file'] ='/assets/images/home/'.$filename;

        }

        $homenews = Homenews::findOrFail($id);
        $homenews->update($requestData);

        return redirect('admin/homenews')->with('flash_message', 'Home news updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Homenews::destroy($id);

        return redirect('admin/homenews')->with('flash_message', 'Home  news deleted!');
    }
}
