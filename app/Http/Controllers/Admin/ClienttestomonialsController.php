<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Clienttestomonial;
use Illuminate\Http\Request;
use App\Authorizable;

class ClienttestomonialsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $clienttestomonials = Clienttestomonial::where('client_message', 'LIKE', "%$keyword%")
                ->orWhere('client_name', 'LIKE', "%$keyword%")
                ->orWhere('clienttestomonials_sort_order', 'LIKE', "%$keyword%")
                ->orWhere('clienttestomonials_status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $clienttestomonials = Clienttestomonial::latest()->paginate($perPage);
        }

        return view('admin.clienttestomonials.index', compact('clienttestomonials'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.clienttestomonials.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'client_message' => 'required',
			'client_name' => 'required',
			'clienttestomonials_sort_order' => 'required',
			'clienttestomonials_status' => 'required'
		]);
        $requestData = $request->all();
        
        Clienttestomonial::create($requestData);

        return redirect('admin/clienttestomonials')->with('flash_message', 'Client testomonial added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $clienttestomonial = Clienttestomonial::findOrFail($id);

        return view('admin.clienttestomonials.show', compact('clienttestomonial'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $clienttestomonial = Clienttestomonial::findOrFail($id);

        return view('admin.clienttestomonials.edit', compact('clienttestomonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'client_message' => 'required',
			'client_name' => 'required',
			'clienttestomonials_sort_order' => 'required',
			'clienttestomonials_status' => 'required'
		]);
        $requestData = $request->all();
        
        $clienttestomonial = Clienttestomonial::findOrFail($id);
        $clienttestomonial->update($requestData);

        return redirect('admin/clienttestomonials')->with('flash_message', 'Client testomonial updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Clienttestomonial::destroy($id);

        return redirect('admin/clienttestomonials')->with('flash_message', 'Client testomonial deleted!');
    }
}
