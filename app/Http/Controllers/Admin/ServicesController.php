<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Service;
use Illuminate\Http\Request;
use App\Authorizable;

class ServicesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $services = Service::where('service_title', 'LIKE', "%$keyword%")
                ->orWhere('service_description', 'LIKE', "%$keyword%")
                ->orWhere('service_image', 'LIKE', "%$keyword%")
                ->orWhere('service_sort_order', 'LIKE', "%$keyword%")
                ->orWhere('service_status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $services = Service::latest()->paginate($perPage);
        }

        return view('admin.services.index', compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.services.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'service_title' => 'required',
			'service_description' => 'required',
			'service_image' => 'required',
			'service_sort_order' => 'required',
			'service_status' => 'required'
		]);
        $requestData = $request->all();
        
         $slug=str_slug($request->service_title);
        $requestData['service_title_as_id']=$slug;
        if ($request->hasFile('service_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('service_image')->getClientOriginalName();
                    $request->service_image->move(base_path('public/assets/images/services'), $filename);
                    $requestData['service_image'] ='/assets/images/services/'.$filename;

        }
                
        Service::create($requestData);

        return redirect('admin/services')->with('flash_message', 'Service added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $service = Service::findOrFail($id);

        return view('admin.services.show', compact('service'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $service = Service::findOrFail($id);

        return view('admin.services.edit', compact('service'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'service_title' => 'required',
			'service_description' => 'required',
			'service_sort_order' => 'required',
			'service_status' => 'required'
		]);
        $requestData = $request->all();
        $slug=str_slug($request->service_title);
        //dd($slug);
        $requestData['service_title_as_id']=$slug;
        //dd($requestData['service_title_as_id']);
        if ($request->hasFile('service_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('service_image')->getClientOriginalName();
                    $request->service_image->move(base_path('public/assets/images/services'), $filename);
                    $requestData['service_image'] ='/assets/images/services/'.$filename;

        }

        $service = Service::findOrFail($id);
        $service->update($requestData);

        return redirect('admin/services')->with('flash_message', 'Service updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Service::destroy($id);

        return redirect('admin/services')->with('flash_message', 'Service deleted!');
    }
}
