<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Homeassociate;
use Illuminate\Http\Request;
use App\Authorizable;

class HomeassociatesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $homeassociates = Homeassociate::where('title', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $homeassociates = Homeassociate::latest()->paginate($perPage);
        }

        return view('admin.homeassociates.index', compact('homeassociates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.homeassociates.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'image' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['image'] ='/assets/images/home/'.$filename;

        }

        Homeassociate::create($requestData);

        return redirect('admin/homeassociates')->with('flash_message', 'Home page associate added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $homeassociate = Homeassociate::findOrFail($id);

        return view('admin.homeassociates.show', compact('homeassociate'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $homeassociate = Homeassociate::findOrFail($id);

        return view('admin.homeassociates.edit', compact('homeassociate'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'image' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['image'] ='/assets/images/home/'.$filename;

        }

        $homeassociate = Homeassociate::findOrFail($id);
        $homeassociate->update($requestData);

        return redirect('admin/homeassociates')->with('flash_message', 'Home page associate updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Homeassociate::destroy($id);

        return redirect('admin/homeassociates')->with('flash_message', 'Home page associate deleted!');
    }
}
