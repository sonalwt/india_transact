<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Investorrelation;
use Illuminate\Http\Request;
use App\Authorizable;

class InvestorrelationsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $investorrelations = Investorrelation::where('doc_file', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $investorrelations = Investorrelation::latest()->paginate($perPage);
        }

        return view('admin.investorrelations.index', compact('investorrelations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.investorrelations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'doc_file' => 'file'
		]);
        $requestData = $request->all();
           if ($request->hasFile('doc_file')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('doc_file')->getClientOriginalName();
                    $request->doc_file->move(base_path('public/assets/docs/download'), $filename);
                    $requestData['doc_file'] ='/assets/docs/download/'.$filename;

        }

        Investorrelation::create($requestData);

        return redirect('admin/investorrelations')->with('flash_message', 'Investor relation added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $investorrelation = Investorrelation::findOrFail($id);

        return view('admin.investorrelations.show', compact('investorrelation'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $investorrelation = Investorrelation::findOrFail($id);

        return view('admin.investorrelations.edit', compact('investorrelation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'doc_file' => 'file'
		]);
        $requestData = $request->all();
         if ($request->hasFile('doc_file')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('doc_file')->getClientOriginalName();
                    $request->doc_file->move(base_path('public/assets/docs'), $filename);
                    $requestData['doc_file'] ='/assets/docs'.$filename;

        }

        $investorrelation = Investorrelation::findOrFail($id);
        $investorrelation->update($requestData);

        return redirect('admin/investorrelations')->with('flash_message', 'Investor relation updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Investorrelation::destroy($id);

        return redirect('admin/investorrelations')->with('flash_message', 'Investor relation deleted!');
    }
}
