<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Keystrength;
use Illuminate\Http\Request;
use App\Authorizable;

class KeystrengthsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $keystrengths = Keystrength::where('caption', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('after_hover_image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $keystrengths = Keystrength::latest()->paginate($perPage);
        }

        return view('admin.keystrengths.index', compact('keystrengths'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.keystrengths.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'caption' => 'required',
			'image' => 'required',
			'after_hover_image' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/key_strength'), $filename);
                    $requestData['image'] ='/assets/images/key_strength/'.$filename;

        }
        if ($request->hasFile('after_hover_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('after_hover_image')->getClientOriginalName();
                    $request->after_hover_image->move(base_path('public/assets/images/key_strength'), $filename);
                    $requestData['after_hover_image'] ='/assets/images/key_strength/'.$filename;

        }

        Keystrength::create($requestData);

        return redirect('admin/keystrengths')->with('flash_message', 'Key strength added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $keystrength = Keystrength::findOrFail($id);

        return view('admin.keystrengths.show', compact('keystrength'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $keystrength = Keystrength::findOrFail($id);

        return view('admin.keystrengths.edit', compact('keystrength'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'caption' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/key_strength'), $filename);
                    $requestData['image'] ='/assets/images/key_strength/'.$filename;

        }
        if ($request->hasFile('after_hover_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('after_hover_image')->getClientOriginalName();
                    $request->after_hover_image->move(base_path('public/assets/images/key_strength'), $filename);
                    $requestData['after_hover_image'] ='/assets/images/key_strength/'.$filename;

        }
        $keystrength = Keystrength::findOrFail($id);
        $keystrength->update($requestData);

        return redirect('admin/keystrengths')->with('flash_message', 'Key strength updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Keystrength::destroy($id);

        return redirect('admin/keystrengths')->with('flash_message', 'Key strength deleted!');
    }
}
