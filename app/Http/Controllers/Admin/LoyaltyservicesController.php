<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Loyaltyservice;
use Illuminate\Http\Request;
use App\Authorizable;

class LoyaltyservicesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $loyaltyservices = Loyaltyservice::where('title', 'LIKE', "%$keyword%")
                ->orWhere('image1', 'LIKE', "%$keyword%")
                ->orWhere('image2', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $loyaltyservices = Loyaltyservice::latest()->paginate($perPage);
        }

        return view('admin.loyaltyservices.index', compact('loyaltyservices'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.loyaltyservices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'image1' => 'required',
			'image2' => 'required',
			'description' => 'required'
		]);
        $requestData = $request->all();
         if ($request->hasFile('image1')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image1')->getClientOriginalName();
                    $request->image1->move(base_path('public/assets/images/products'), $filename);
                    $requestData['image1'] ='/assets/images/products/'.$filename;

        }
         if ($request->hasFile('image2')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image2')->getClientOriginalName();
                    $request->image2->move(base_path('public/assets/images/products'), $filename);
                    $requestData['image2'] ='/assets/images/products/'.$filename;

        }

        Loyaltyservice::create($requestData);

        return redirect('admin/loyaltyservices')->with('flash_message', 'Loyalty service added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $loyaltyservice = Loyaltyservice::findOrFail($id);

        return view('admin.loyaltyservices.show', compact('loyaltyservice'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $loyaltyservice = Loyaltyservice::findOrFail($id);

        return view('admin.loyaltyservices.edit', compact('loyaltyservice'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'description' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image1')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image1')->getClientOriginalName();
                    $request->image1->move(base_path('public/assets/images/products'), $filename);
                    $requestData['image1'] ='/assets/images/products/'.$filename;

        }
         if ($request->hasFile('image2')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image2')->getClientOriginalName();
                    $request->image2->move(base_path('public/assets/images/products'), $filename);
                    $requestData['image2'] ='/assets/images/products/'.$filename;

        }

        $loyaltyservice = Loyaltyservice::findOrFail($id);
        $loyaltyservice->update($requestData);

        return redirect('admin/loyaltyservices/1/edit')->with('flash_message', 'Loyalty Service updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Loyaltyservice::destroy($id);

        return redirect('admin/loyaltyservices')->with('flash_message', 'Loyalty service deleted!');
    }
}
