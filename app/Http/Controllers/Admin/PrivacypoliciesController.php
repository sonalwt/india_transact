<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Privacypolicy;
use Illuminate\Http\Request;
use App\Authorizable;

class PrivacypoliciesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $privacypolicies = Privacypolicy::where('title', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $privacypolicies = Privacypolicy::latest()->paginate($perPage);
        }

        return view('admin.privacypolicies.index', compact('privacypolicies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.privacypolicies.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'description' => 'required',
            'image'=>'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/docs'), $filename);
                    $requestData['image'] ='/assets/docs/'.$filename;

        }    
        Privacypolicy::create($requestData);

        return redirect('admin/privacypolicies')->with('flash_message', 'Privacypolicy added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $privacypolicy = Privacypolicy::findOrFail($id);

        return view('admin.privacypolicies.show', compact('privacypolicy'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $privacypolicy = Privacypolicy::findOrFail($id);

        return view('admin.privacypolicies.edit', compact('privacypolicy'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'description' => 'required'
		]);
        $requestData = $request->all();
         if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/docs'), $filename);
                    $requestData['image'] ='/assets/docs/'.$filename;

        }  
        $privacypolicy = Privacypolicy::findOrFail($id);
        $privacypolicy->update($requestData);

        return redirect('admin/privacypolicies/1/edit')->with('flash_message', 'Privacy Policy updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Privacypolicy::destroy($id);

        return redirect('admin/privacypolicies')->with('flash_message', 'Privacypolicy deleted!');
    }
}
