<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Footprint;
use Illuminate\Http\Request;
use App\Authorizable;

class FootprintsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $footprints = Footprint::where('counter_caption', 'LIKE', "%$keyword%")
                ->orWhere('counter_number', 'LIKE', "%$keyword%")
                ->orWhere('counter_unit', 'LIKE', "%$keyword%")
                ->orWhere('counter_image', 'LIKE', "%$keyword%")
                ->orWhere('counter_status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $footprints = Footprint::latest()->paginate($perPage);
        }

        return view('admin.footprints.index', compact('footprints'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.footprints.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'counter_caption' => 'required',
			'counter_number' => 'required',
			'counter_image' => 'required',
			'counter_status' => 'required'
		]);
        $requestData = $request->all();
        

        if ($request->hasFile('counter_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('counter_image')->getClientOriginalName();
                    $request->counter_image->move(base_path('public/assets/images/footprints'), $filename);
                    $requestData['counter_image'] ='/assets/images/footprints/'.$filename;

        }
        Footprint::create($requestData);

        return redirect('admin/footprints')->with('flash_message', 'Footprint added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $footprint = Footprint::findOrFail($id);

        return view('admin.footprints.show', compact('footprint'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $footprint = Footprint::findOrFail($id);

        return view('admin.footprints.edit', compact('footprint'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'counter_caption' => 'required',
			'counter_number' => 'required',
			'counter_status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('counter_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('counter_image')->getClientOriginalName();
                    $request->counter_image->move(base_path('public/assets/images/footprints'), $filename);
                    $requestData['counter_image'] ='/assets/images/footprints/'.$filename;
        }

        $footprint = Footprint::findOrFail($id);
        $footprint->update($requestData);

        return redirect('admin/footprints')->with('flash_message', 'Footprint updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Footprint::destroy($id);

        return redirect('admin/footprints')->with('flash_message', 'Footprint deleted!');
    }
}
