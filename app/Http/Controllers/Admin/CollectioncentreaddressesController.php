<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Collectioncentreaddress;
use Illuminate\Http\Request;
use App\Authorizable;

class CollectioncentreaddressesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $collectioncentreaddresses = Collectioncentreaddress::where('state', 'LIKE', "%$keyword%")
                ->orWhere('collection_centre_address', 'LIKE', "%$keyword%")
                ->orWhere('toll_free_no', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $collectioncentreaddresses = Collectioncentreaddress::latest()->paginate($perPage);
        }

        return view('admin.collectioncentreaddresses.index', compact('collectioncentreaddresses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.collectioncentreaddresses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'state' => 'required',
			'collection_centre_address' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        Collectioncentreaddress::create($requestData);

        return redirect('admin/collectioncentreaddresses')->with('flash_message', 'Collection centre address added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $collectioncentreaddress = Collectioncentreaddress::findOrFail($id);

        return view('admin.collectioncentreaddresses.show', compact('collectioncentreaddress'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $collectioncentreaddress = Collectioncentreaddress::findOrFail($id);

        return view('admin.collectioncentreaddresses.edit', compact('collectioncentreaddress'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'state' => 'required',
			'collection_centre_address' => 'required',
			
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        $collectioncentreaddress = Collectioncentreaddress::findOrFail($id);
        $collectioncentreaddress->update($requestData);

        return redirect('admin/collectioncentreaddresses')->with('flash_message', 'Collection centre address updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Collectioncentreaddress::destroy($id);

        return redirect('admin/collectioncentreaddresses')->with('flash_message', 'Collection centre address deleted!');
    }
}
