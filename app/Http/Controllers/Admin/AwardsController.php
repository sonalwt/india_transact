<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Award;
use App\Awardyear;
use Illuminate\Http\Request;
use App\Authorizable;

class AwardsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $awards = Award::where('award_details', 'LIKE', "%$keyword%")
                ->orWhere('award_year', 'LIKE', "%$keyword%")
                ->orWhere('award_sort_order', 'LIKE', "%$keyword%")
                ->orWhere('year_status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $awards = Award::latest()->paginate($perPage);
        }

        return view('admin.awards.index', compact('awards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $awardyears=Awardyear::where('year_status',1)->get();
        return view('admin.awards.create',compact('awardyears'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'award_details' => 'required',
			'award_year' => 'required',
			'award_sort_order' => 'required',
			'year_status' => 'required'
		]);
        $requestData = $request->all();
        
        Award::create($requestData);

        return redirect('admin/awards')->with('flash_message', 'Award added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $award = Award::findOrFail($id);

        return view('admin.awards.show', compact('award'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $awardyears=Awardyear::where('year_status',1)->get();
        $award = Award::findOrFail($id);

        return view('admin.awards.edit', compact('award','awardyears'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'award_details' => 'required',
			'award_year' => 'required',
			'award_sort_order' => 'required',
			'year_status' => 'required'
		]);
        $requestData = $request->all();
        
        $award = Award::findOrFail($id);
        $award->update($requestData);

        return redirect('admin/awards')->with('flash_message', 'Award updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Award::destroy($id);

        return redirect('admin/awards')->with('flash_message', 'Award deleted!');
    }
}
