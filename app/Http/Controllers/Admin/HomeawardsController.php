<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Homeaward;
use Illuminate\Http\Request;
use App\Authorizable;

class HomeawardsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $homeawards = Homeaward::where('title', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $homeawards = Homeaward::latest()->paginate($perPage);
        }

        return view('admin.homeawards.index', compact('homeawards'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.homeawards.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'image' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['image'] ='/assets/images/home/'.$filename;

        }

        Homeaward::create($requestData);

        return redirect('admin/homeawards')->with('flash_message', 'Home page award added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $homeaward = Homeaward::findOrFail($id);

        return view('admin.homeawards.show', compact('homeaward'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $homeaward = Homeaward::findOrFail($id);

        return view('admin.homeawards.edit', compact('homeaward'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'image' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
         if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['image'] ='/assets/images/home/'.$filename;

        }

        $homeaward = Homeaward::findOrFail($id);
        $homeaward->update($requestData);

        return redirect('admin/homeawards')->with('flash_message', 'Home page award updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Homeaward::destroy($id);

        return redirect('admin/homeawards')->with('flash_message', 'Home page award deleted!');
    }
}
