<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\paymentsolution;
use Illuminate\Http\Request;
use App\Authorizable;

class paymentsolutionsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $paymentsolutions = paymentsolution::where('title1', 'LIKE', "%$keyword%")
                ->orWhere('title2', 'LIKE', "%$keyword%")
                ->orWhere('icon_image', 'LIKE', "%$keyword%")
                ->orWhere('main_image', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $paymentsolutions = paymentsolution::latest()->paginate($perPage);
        }

        return view('admin.paymentsolutions.index', compact('paymentsolutions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.paymentsolutions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title1' => 'required',
			'title2' => 'required',
			'icon_image' => 'required',
			'main_image' => 'required',
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('icon_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('icon_image')->getClientOriginalName();
                    $request->icon_image->move(base_path('public/assets/images/products'), $filename);
                    $requestData['icon_image'] ='/assets/images/products/'.$filename;

        }
        if ($request->hasFile('main_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('main_image')->getClientOriginalName();
                    $request->main_image->move(base_path('public/assets/images/products'), $filename);
                    $requestData['main_image'] ='/assets/images/products/'.$filename;

        }

        paymentsolution::create($requestData);

        return redirect('admin/paymentsolutions')->with('flash_message', 'payment solution added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $paymentsolution = paymentsolution::findOrFail($id);

        return view('admin.paymentsolutions.show', compact('paymentsolution'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $paymentsolution = paymentsolution::findOrFail($id);

        return view('admin.paymentsolutions.edit', compact('paymentsolution'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title1' => 'required',
			'title2' => 'required',
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('icon_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('icon_image')->getClientOriginalName();
                    $request->icon_image->move(base_path('public/assets/images/products'), $filename);
                    $requestData['icon_image'] ='/assets/images/products/'.$filename;

        }
        if ($request->hasFile('main_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('main_image')->getClientOriginalName();
                    $request->main_image->move(base_path('public/assets/images/products'), $filename);
                    $requestData['main_image'] ='/assets/images/products/'.$filename;

        }

        $paymentsolution = paymentsolution::findOrFail($id);
        $paymentsolution->update($requestData);

        return redirect('admin/paymentsolutions')->with('flash_message', 'payment solution updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        paymentsolution::destroy($id);

        return redirect('admin/paymentsolutions')->with('flash_message', 'payment solution deleted!');
    }
}
