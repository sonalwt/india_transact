<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Ongobusiness;
use Illuminate\Http\Request;
use App\Authorizable;

class OngobusinessesController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $ongobusinesses = Ongobusiness::where('title', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $ongobusinesses = Ongobusiness::latest()->paginate($perPage);
        }

        return view('admin.ongobusinesses.index', compact('ongobusinesses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.ongobusinesses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'image' => 'required',
			'description' => 'required'
		]);
        $requestData = $request->all();
         if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/products'), $filename);
                    $requestData['image'] ='/assets/images/products/'.$filename;

        }


        Ongobusiness::create($requestData);

        return redirect('admin/ongobusinesses')->with('flash_message', 'Ongobusiness added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $ongobusiness = Ongobusiness::findOrFail($id);

        return view('admin.ongobusinesses.show', compact('ongobusiness'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $ongobusiness = Ongobusiness::findOrFail($id);

        return view('admin.ongobusinesses.edit', compact('ongobusiness'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'description' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/products'), $filename);
                    $requestData['image'] ='/assets/images/products/'.$filename;

        }


        $ongobusiness = Ongobusiness::findOrFail($id);
        $ongobusiness->update($requestData);

        return redirect('admin/ongobusinesses/1/edit')->with('flash_message', 'Ongo business updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Ongobusiness::destroy($id);

        return redirect('admin/ongobusinesses')->with('flash_message', 'Ongobusiness deleted!');
    }
}
