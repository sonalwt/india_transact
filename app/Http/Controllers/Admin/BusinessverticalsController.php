<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Businessvertical;
use App\Businessheading;
use Illuminate\Http\Request;
use App\Authorizable;

class BusinessverticalsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $businessverticals = Businessvertical::where('business_title', 'LIKE', "%$keyword%")
                ->orWhere('business_heading_id', 'LIKE', "%$keyword%")
                ->orWhere('business_description', 'LIKE', "%$keyword%")
                ->orWhere('business_icon', 'LIKE', "%$keyword%")
                ->orWhere('business_icon_selected', 'LIKE', "%$keyword%")
                ->orWhere('business_image', 'LIKE', "%$keyword%")
                ->orWhere('business_sort_order', 'LIKE', "%$keyword%")
                ->orWhere('business_status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $businessverticals = Businessvertical::latest()->paginate($perPage);
        }

        return view('admin.businessverticals.index', compact('businessverticals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $businessheadings=Businessheading::where('business_heading_status',1)->get();
        return view('admin.businessverticals.create',compact('businessheadings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'business_title' => 'required',
			'business_heading_id' => 'required',
			'business_sort_order' => 'required',
			'business_status' => 'required',
			'business_description' => 'required',
			'business_icon' => 'required',
			'business_icon_selected' => 'required',
			'business_image' => 'required'
		]);
        $requestData = $request->all();
         if ($request->hasFile('business_icon')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('business_icon')->getClientOriginalName();
                    $request->business_icon->move(base_path('public/assets/images/business/icons'), $filename);
                    $requestData['business_icon'] ='/assets/images/business/icons/'.$filename;

        }
        if ($request->hasFile('business_icon_selected')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('business_icon_selected')->getClientOriginalName();
                    $request->business_icon_selected->move(base_path('public/assets/images/business/icons'), $filename);
                    $requestData['business_icon_selected'] ='/assets/images/business/icons/'.$filename;

        }
        
        if ($request->hasFile('business_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('business_image')->getClientOriginalName();
                    $request->business_image->move(base_path('public/assets/images/business'), $filename);
                    $requestData['business_image'] ='/assets/images/business/'.$filename;

        }       
        

        Businessvertical::create($requestData);

        return redirect('admin/businessverticals')->with('flash_message', 'Business vertical added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $businessvertical = Businessvertical::findOrFail($id);

        return view('admin.businessverticals.show', compact('businessvertical'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $businessheadings=Businessheading::where('business_heading_status',1)->get();
        $businessvertical = Businessvertical::findOrFail($id);

        return view('admin.businessverticals.edit', compact('businessvertical','businessheadings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'business_title' => 'required',
			'business_heading_id' => 'required',
			'business_sort_order' => 'required',
			'business_status' => 'required',
			'business_description' => 'required',
		]);
        $requestData = $request->all();
         if ($request->hasFile('business_icon')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('business_icon')->getClientOriginalName();
                    $request->business_icon->move(base_path('public/assets/images/business/icons'), $filename);
                    $requestData['business_icon'] ='/assets/images/business/icons/'.$filename;

        }
        if ($request->hasFile('business_icon_selected')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('business_icon_selected')->getClientOriginalName();
                    $request->business_icon_selected->move(base_path('public/assets/images/business/icons'), $filename);
                    $requestData['business_icon_selected'] ='/assets/images/business/icons/'.$filename;

        }
        
        if ($request->hasFile('business_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('business_image')->getClientOriginalName();
                    $request->business_image->move(base_path('public/assets/images/business'), $filename);
                    $requestData['business_image'] ='/assets/images/business/'.$filename;

        }       
        
        $businessvertical = Businessvertical::findOrFail($id);
        $businessvertical->update($requestData);

        return redirect('admin/businessverticals/'.$id.'/edit')->with('flash_message', 'Business Vertical Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Businessvertical::destroy($id);

        return redirect('admin/businessverticals')->with('flash_message', 'Business vertical deleted!');
    }
}
