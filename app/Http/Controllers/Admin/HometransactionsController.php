<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Hometransaction;
use Illuminate\Http\Request;
use App\Authorizable;
use App\Hometransactionheading;
class HometransactionsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $hometransactions = Hometransaction::where('title', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('main_image', 'LIKE', "%$keyword%")
                ->orWhere('icon_image', 'LIKE', "%$keyword%")
                ->orWhere('selected_icon_image', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $hometransactions = Hometransaction::latest()->paginate($perPage);
        }

        return view('admin.hometransactions.index', compact('hometransactions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $headings=Hometransactionheading::where('status',1)->first();
        return view('admin.hometransactions.create',compact('headings'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
            'heading_id'=>'required',
			'description' => 'required',
			'main_image' => 'required',
			'icon_image' => 'required',
			'selected_icon_image' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('main_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('main_image')->getClientOriginalName();
                    $request->main_image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['main_image'] ='/assets/images/home/'.$filename;

        }
       if ($request->hasFile('icon_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('icon_image')->getClientOriginalName();
                    $request->icon_image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['icon_image'] ='/assets/images/home/'.$filename;

        }
        if ($request->hasFile('selected_icon_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('selected_icon_image')->getClientOriginalName();
                    $request->selected_icon_image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['selected_icon_image'] ='/assets/images/home/'.$filename;

        }

        Hometransaction::create($requestData);

        return redirect('admin/hometransactions')->with('flash_message', 'Home transaction added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $hometransaction = Hometransaction::findOrFail($id);

        return view('admin.hometransactions.show', compact('hometransaction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $headings=Hometransactionheading::where('status',1)->first();
        $hometransaction = Hometransaction::findOrFail($id);

        return view('admin.hometransactions.edit', compact('hometransaction','headings'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
            'heading_id'=>'required',
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('main_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('main_image')->getClientOriginalName();
                    $request->main_image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['main_image'] ='/assets/images/home/'.$filename;

        }
       if ($request->hasFile('icon_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('icon_image')->getClientOriginalName();
                    $request->icon_image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['icon_image'] ='/assets/images/home/'.$filename;

        }
        if ($request->hasFile('selected_icon_image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('selected_icon_image')->getClientOriginalName();
                    $request->selected_icon_image->move(base_path('public/assets/images/home'), $filename);
                    $requestData['selected_icon_image'] ='/assets/images/home/'.$filename;

        }

        $hometransaction = Hometransaction::findOrFail($id);
        $hometransaction->update($requestData);

        return redirect('admin/hometransactions/'.$id.'/edit')->with('flash_message', 'Home Transaction  Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Hometransaction::destroy($id);

        return redirect('admin/hometransactions')->with('flash_message', 'Home transaction deleted!');
    }
}
