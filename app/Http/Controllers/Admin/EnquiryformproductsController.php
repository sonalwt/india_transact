<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Enquiryformproduct;
use Illuminate\Http\Request;
use App\Authorizable;
use App\Enquiryform;
use App\Careerform;
class EnquiryformproductsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $enquiryformproducts = Enquiryformproduct::where('title', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $enquiryformproducts = Enquiryformproduct::latest()->paginate($perPage);
        }

        return view('admin.enquiryformproducts.index', compact('enquiryformproducts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.enquiryformproducts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        Enquiryformproduct::create($requestData);

        return redirect('admin/enquiryformproducts')->with('flash_message', 'Enquiry form product added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $enquiryformproduct = Enquiryformproduct::findOrFail($id);

        return view('admin.enquiryformproducts.show', compact('enquiryformproduct'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $enquiryformproduct = Enquiryformproduct::findOrFail($id);

        return view('admin.enquiryformproducts.edit', compact('enquiryformproduct'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        $enquiryformproduct = Enquiryformproduct::findOrFail($id);
        $enquiryformproduct->update($requestData);

        return redirect('admin/enquiryformproducts')->with('flash_message', 'Enquiry form product updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Enquiryformproduct::destroy($id);

        return redirect('admin/enquiryformproducts')->with('flash_message', 'Enquiry form product deleted!');
    }

    public function enquiryformleads(Request $request){

        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $enquiryforms = Enquiryform::where('name', 'LIKE', "%$keyword%")->orWhere('email', 'LIKE', "%$keyword%")->orWhere('mobile', 'LIKE', "%$keyword%")->orWhere('state', 'LIKE', "%$keyword%")->orWhere('city', 'LIKE', "%$keyword%")
                ->orWhere('message', 'LIKE', "%$keyword%")->orWhere('utm_source', 'LIKE', "%$keyword%")->orWhere('form_type', 'LIKE', "%$keyword%")->orWhere('utm_campaign', 'LIKE', "%$keyword%")->orWhere('utm_medium', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $enquiryforms = Enquiryform::latest()->paginate($perPage);
        }

        return view('admin.enquiryformproducts.enquiry_leads', compact('enquiryforms'));
    }

    public function careerformleads(Request $request){
         $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $enquiryforms = Careerform::where('name', 'LIKE', "%$keyword%")->orWhere('email', 'LIKE', "%$keyword%")->orWhere('mobile', 'LIKE', "%$keyword%")->orWhere('message', 'LIKE', "%$keyword%")->latest()->paginate($perPage);
        } else {
            $enquiryforms = Careerform::latest()->paginate($perPage);
        }

        return view('admin.enquiryformproducts.career_leads', compact('enquiryforms'));
    }
}
