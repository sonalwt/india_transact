<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Hometransactionheading;
use Illuminate\Http\Request;
use App\Authorizable;

class HometransactionheadingsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $hometransactionheadings = Hometransactionheading::where('headings', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $hometransactionheadings = Hometransactionheading::latest()->paginate($perPage);
        }

        return view('admin.hometransactionheadings.index', compact('hometransactionheadings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.hometransactionheadings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'headings' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        Hometransactionheading::create($requestData);

        return redirect('admin/hometransactionheadings')->with('flash_message', 'Home transaction heading added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $hometransactionheading = Hometransactionheading::findOrFail($id);

        return view('admin.hometransactionheadings.show', compact('hometransactionheading'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $hometransactionheading = Hometransactionheading::findOrFail($id);

        return view('admin.hometransactionheadings.edit', compact('hometransactionheading'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'headings' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        
        $hometransactionheading = Hometransactionheading::findOrFail($id);
        $hometransactionheading->update($requestData);

        return redirect('admin/hometransactionheadings')->with('flash_message', 'Home transaction heading updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Hometransactionheading::destroy($id);

        return redirect('admin/hometransactionheadings')->with('flash_message', 'Home transaction heading deleted!');
    }
}
