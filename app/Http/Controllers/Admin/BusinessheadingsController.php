<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Businessheading;
use Illuminate\Http\Request;
use App\Authorizable;

class BusinessheadingsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $businessheadings = Businessheading::where('business_heading', 'LIKE', "%$keyword%")
                ->orWhere('business_heading_sort_order', 'LIKE', "%$keyword%")
                ->orWhere('business_heading_status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $businessheadings = Businessheading::latest()->paginate($perPage);
        }

        return view('admin.businessheadings.index', compact('businessheadings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.businessheadings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'business_heading' => 'required',
			'business_heading_sort_order' => 'required',
			'business_heading_status' => 'required'
		]);
        $requestData = $request->all();
        
        Businessheading::create($requestData);

        return redirect('admin/businessheadings')->with('flash_message', 'Business heading added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $businessheading = Businessheading::findOrFail($id);

        return view('admin.businessheadings.show', compact('businessheading'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $businessheading = Businessheading::findOrFail($id);

        return view('admin.businessheadings.edit', compact('businessheading'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'business_heading' => 'required',
			'business_heading_sort_order' => 'required',
			'business_heading_status' => 'required'
		]);
        $requestData = $request->all();
        
        $businessheading = Businessheading::findOrFail($id);
        $businessheading->update($requestData);

        return redirect('admin/businessheadings')->with('flash_message', 'Business heading updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Businessheading::destroy($id);

        return redirect('admin/businessheadings')->with('flash_message', 'Business heading deleted!');
    }
}
