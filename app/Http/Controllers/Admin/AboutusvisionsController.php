<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Aboutusvision;
use Illuminate\Http\Request;
use App\Authorizable;

class AboutusvisionsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $aboutusvisions = Aboutusvision::where('title1', 'LIKE', "%$keyword%")
                ->orWhere('description1', 'LIKE', "%$keyword%")
                ->orWhere('title2', 'LIKE', "%$keyword%")
                ->orWhere('description2', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $aboutusvisions = Aboutusvision::latest()->paginate($perPage);
        }

        return view('admin.aboutusvisions.index', compact('aboutusvisions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.aboutusvisions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'title1' => 'required',
			'description1' => 'required',
			'title2' => 'required',
			'description2' => 'required',
			'image' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/about'), $filename);
                    $requestData['image'] ='/assets/images/about/'.$filename;

        }

        Aboutusvision::create($requestData);

        return redirect('admin/aboutusvisions')->with('flash_message', 'About us vision added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $aboutusvision = Aboutusvision::findOrFail($id);

        return view('admin.aboutusvisions.show', compact('aboutusvision'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $aboutusvision = Aboutusvision::findOrFail($id);

        return view('admin.aboutusvisions.edit', compact('aboutusvision'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'title1' => 'required',
			'description1' => 'required',
			'title2' => 'required',
			'description2' => 'required',
			
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/about'), $filename);
                    $requestData['image'] ='/assets/images/about/'.$filename;

        }

        $aboutusvision = Aboutusvision::findOrFail($id);
        $aboutusvision->update($requestData);

        return redirect('admin/aboutusvisions/1/edit')->with('flash_message', 'About us vision updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Aboutusvision::destroy($id);

        return redirect('admin/aboutusvisions')->with('flash_message', 'About us vision deleted!');
    }
}
