<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Boardofdirector;
use Illuminate\Http\Request;
use App\Authorizable;

class BoardofdirectorsController extends Controller
{
    use Authorizable;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $boardofdirectors = Boardofdirector::where('name', 'LIKE', "%$keyword%")
                ->orWhere('designation', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('description', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $boardofdirectors = Boardofdirector::latest()->paginate($perPage);
        }

        return view('admin.boardofdirectors.index', compact('boardofdirectors'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.boardofdirectors.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
			'name' => 'required',
			'designation' => 'required',
			'image' => 'required',
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/about'), $filename);
                    $requestData['image'] ='/assets/images/about/'.$filename;

        }

        Boardofdirector::create($requestData);

        return redirect('admin/boardofdirectors')->with('flash_message', 'Board of director added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $boardofdirector = Boardofdirector::findOrFail($id);

        return view('admin.boardofdirectors.show', compact('boardofdirector'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $boardofdirector = Boardofdirector::findOrFail($id);

        return view('admin.boardofdirectors.edit', compact('boardofdirector'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
			'name' => 'required',
			'designation' => 'required',
			'description' => 'required',
			'status' => 'required'
		]);
        $requestData = $request->all();
        if ($request->hasFile('image')) {
                    $icon=mt_rand();
                    $filename=$icon.$request->file('image')->getClientOriginalName();
                    $request->image->move(base_path('public/assets/images/about'), $filename);
                    $requestData['image'] ='/assets/images/about/'.$filename;

        }

        $boardofdirector = Boardofdirector::findOrFail($id);
        $boardofdirector->update($requestData);

        return redirect('admin/boardofdirectors')->with('flash_message', 'Board of director updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Boardofdirector::destroy($id);

        return redirect('admin/boardofdirectors')->with('flash_message', 'Board of director deleted!');
    }
}
